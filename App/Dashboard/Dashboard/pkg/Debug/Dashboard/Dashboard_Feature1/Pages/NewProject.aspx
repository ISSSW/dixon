﻿<%@ Page Language="C#" MasterPageFile="~masterurl/default.master" Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <SharePoint:ScriptLink ID="ScriptLink1" Name="sp.js" runat="server" OnDemand="true" LoadAfterUI="true" Localizable="false" />
    <WebPartPages:AllowFraming ID="AllowFraming1" runat="server" />

    <script type="text/javascript" src="../Scripts/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.runtime.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.js"></script>
    <script type="text/javascript" src="../Scripts/App.js"></script>
    <script type="text/javascript" src="../Scripts/q.js"></script>
    <!--Start Dashboard references -->

    <!-- Bootstrap Core CSS -->
    <link href="../Content/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Sidebar CSS -->
    <link href="../Content/css/sidebar.css" rel="stylesheet" />

    <!-- Custom CSS -->
    <link href="../Content/css/style.css" rel="stylesheet" />

    <!-- Slider CSS -->
    <link rel="stylesheet" href="../Content/css/slider-style.css" type="text/css" media="all" />
    <link rel="stylesheet" href="../Content/css/jquery-ui.css" type="text/css" media="all" />

    <!-- jQuery -->
    <script type="text/javascript" src="../Scripts/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script type="text/javascript" src="../Scripts/bootstrap.min.js"></script>

    <!-- Slider JS files -->
    <script type="text/javascript" src="../Scripts/jquery-ui.js"></script>
    <script type="text/javascript" src="../Scripts/slider.js"></script>
    <!-- End Dashboard references-->


    <!---------------New References ---------------------------->
    <script type="text/javascript" src="../Scripts/dist/cpexcel.js"></script>
    <script type="text/javascript" src="../Scripts/shim.js"></script>
    <script type="text/javascript" src="../Scripts/jszip.js"></script>
    <script type="text/javascript" src="../Scripts/xlsx.js"></script>
    <script type="text/javascript" src="../Scripts/dist/ods.js"></script>
    <script type="text/javascript" src="../Scripts/jquery1.min.js"></script>
    <script type="text/javascript" src="../Scripts/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Scripts/popupWindow.js"></script>

    <!-- Kendo customize css -->
    <link href="../Content/kendo-customize.css" rel="stylesheet" />
    <link href="../Content/kendo.common.min.css" rel="stylesheet" />
    <link href="../Content/kendo.silver.min.css" rel="stylesheet" />
    <link href="../Content/kendo.dataviz.min.css" rel="stylesheet" />
    <link href="../Content/kendo.dataviz.silver.min.css" rel="stylesheet" />
    <!---------------end new references ------------------------>

    <!-- Loading CSS & JS -->
    <link href="../Content/css/font-awesome.css" rel="stylesheet" />
    <link href="../Content/css/loadingstyle.css" rel="stylesheet" />
    <script type="text/javascript" src="../Scripts/jquery.isloading.js"></script>
    <!---------------------------------------------------------->

    <style>
        input.form-control {
            text-align: left !important;
        }

        .import {
            margin-right: 8px;
            margin-left: -30px;
        }

        #s4-titlerow {
            display: none !important;
        }
    </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <WebPartPages:WebPartZone runat="server" FrameType="TitleBarOnly" ID="full" Title="loc:full" />


    <input type="hidden" id="AccessCode" />
    <input type="hidden" id="ProjectCode" />
    <div id="wrapper">
        <div class="col-lg-4 col-md-6 col-sm-12">
            <h1 class="project-label margin-bottom20 label label-primary" style="color: #F7F8F9">New Project<span id="projectId"></span></h1>
        </div>
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row margin-top5">
                    <div class="col-md-12 no-padding margin-top20">
                        <div class="col-lg-3 col-md-6 col-sm-12 import">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="col-lg-12 no-padding padding-for-large">
                                        <span class="project-label margin-bottom20">Import Estimate Spreadsheet
                                        <button id="import" class="btn btn-sm btn-default btn-export" style="cursor: pointer">Import</button>
                                            <input type="file" name="xlfile" id="xlf" style="display: none" /></span>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div id="import-data">
                                        <div class="col-lg-12 no-padding padding-for-large">
                                            <div class="col-lg-5 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <h4 class="project-label margin-bottom20">Project Number</h4>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <input disabled="disabled" type="text" value="  projectNumber  " id="ProjectNumber" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-5 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <h4 class="project-label margin-bottom20">Project Type</h4>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <input disabled="disabled" type="text" value="  projectType  " id="ProjectType" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="alert"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12 no-padding margin-top20">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /#wrapper -->

    <script type="text/javascript">

        //-----------------------------------------------------------------

        $(document).ready(function () {

            var code = getUrlParameter('code');
            $('#suiteBarDelta').css("display", "none");
            $('#s4-ribbonrow').css("display", "none");
            $('#titleAreaRow').css("display", "none");
            $('#s4-workspace').css("overflow-x", "hidden");
            $('#s4-workspace').css("overflow-y", "hidden");

            if (code != undefined) {
                $("#AccessCode").val(code);
            }

            var _hostweburl = $("#hostweburl", opener.document).val();
            var scriptbase1 = _hostweburl + "/_layouts/15/";
            $.getScript(scriptbase1 + "SP.RequestExecutor.js", getAccounts);


        });

        //-----------------------------------------------------------------

        function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1);
            var sURLVariables = sPageURL.split('&');
            for (var i = 0; i < sURLVariables.length; i++) {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] == sParam) {
                    return sParameterName[1];
                }
            }
        }

        $("#import").click(function (e) {
            e.preventDefault();
            $("#xlf").trigger('click');
        });

        function confirmUpload() {

            if (XML_OUTPUT['Import_1'][1].Value == "Commercial" || XML_OUTPUT['Import_1'][1].Value == "Residential") {

                var code = $("#AccessCode").val();
                var path = 'https://' + window.location.hostname + window.location.pathname;

                if (code !== "") {

                    $.isLoading({ text: "MYOB validation...", position: "overlay" });

                    var externalUrl = "http://dixonmyobdataservice.cloudapp.net/MYOBDataSvc.svc/ProjectCodeExist";
                    var jsonData = { AuthorizationCode: code, ProjectCode: $("#ProjectCode").val(), Path: path };

                    $.ajax({
                        url: "../_api/SP.WebProxy.invoke",
                        type: "POST",
                        data: JSON.stringify(
                            {
                                "requestInfo": {
                                    "__metadata": { "type": "SP.WebRequestInfo" },
                                    "Url": externalUrl, //this is the uri that calls the webservice api
                                    "Method": "POST",
                                    "Headers": {
                                        "results": [{
                                            "__metadata": { "type": "SP.KeyValue" },
                                            "Key": "Accept",
                                            "Value": "application/json;odata=verbose",
                                            "ValueType": "Edm.String"
                                        }, {
                                            __metadata: { type: "SP.KeyValue" },
                                            Key: "Content-Type",
                                            Value: "application/json",
                                            ValueType: "Edm.String"
                                        }]
                                    },
                                    "Body": JSON.stringify(jsonData), //this is the data to be send to the webservice
                                }
                            }),
                        headers: {
                            "Accept": "application/json;odata=verbose",
                            "Content-Type": "application/json;odata=verbose",
                            "X-RequestDigest": $("#__REQUESTDIGEST").val()
                        },
                        success: function (data) {

                            if (data.d.Invoke.StatusCode == 200) {
                                var body = JSON.parse(data.d.Invoke.Body);
                                var myobAccounts = body.Accounts;

                                if (body.Msg == "OK" && body.IsValidated == true) {

                                    var alert = '<div class="alert alert-success"> ' +
                                             //'<button type="button" class="close" data-dismiss="alert">×</button> ' +
                                             '<strong>MYOB </strong> validation success. ' +
                                           '</div>';

                                    $("#alert").empty();
                                    $("#alert").append(alert);

                                    $.isLoading("hide");
                                    $.isLoading({ text: "Checking project exist...", position: "overlay" });

                                    //Here doing update list
                                    //============================================================

                                    checkProjectExistFromNew(Q, XML_OUTPUT['Import_1'][0].Value).then(function (returnData) {

                                        if (returnData != undefined && returnData.length == 0) {

                                            var projectId = XML_OUTPUT['Import_1'][0].Value;

                                            var projectData = {
                                                project_number: XML_OUTPUT['Import_1'][0].Value,
                                                project_type: XML_OUTPUT['Import_1'][1].Value,//Commercial
                                                Title: XML_OUTPUT['Import_1'][2].Value,
                                                project_address: XML_OUTPUT['Import_1'][3].Value,
                                                project_principle_name: XML_OUTPUT['Import_1'][4].Value,
                                                project_principle_address: XML_OUTPUT['Import_1'][5].Value,
                                                project_principle_email: XML_OUTPUT['Import_1'][6].Value,
                                                project_principle_business_name: XML_OUTPUT['Import_1'][7].Value,
                                                project_principle_abn: XML_OUTPUT['Import_1'][8].Value,
                                                project_principle_ect: XML_OUTPUT['Import_1'][9].Value,
                                                project_rep_name: XML_OUTPUT['Import_1'][10].Value,
                                                project_rep_address: XML_OUTPUT['Import_1'][11].Value,
                                                project_rep_email: XML_OUTPUT['Import_1'][12].Value,
                                                project_rep_business_name: XML_OUTPUT['Import_1'][13].Value,
                                                project_rep_abn: XML_OUTPUT['Import_1'][14].Value,
                                                project_rep_ect: XML_OUTPUT['Import_1'][15].Value,
                                                project_building_margin: XML_OUTPUT['Import_1'][16].Value,
                                                project_contingency: XML_OUTPUT['Import_1'][17].Value,
                                                project_status: 'Open'
                                            };

                                            $.isLoading("hide");
                                            $.isLoading({ text: "Insert project data...", position: "overlay" });

                                            createItemFromNew(Q, projectData, "Project").then(function () {

                                                createFolders(XML_OUTPUT['Import_1'][1].Value, XML_OUTPUT['Import_1'][2].Value, projectId, ["Milestone Claims", "Variations"], true);

                                                $.isLoading("hide");
                                                $.isLoading({ text: "Insert costing data...", position: "overlay" });

                                                insertProjectAccountData(Q, projectId, GetCostings(XML_OUTPUT, myobAccounts)).then(function () {

                                                    $.isLoading("hide");
                                                    $.isLoading({ text: "Insert milestone data...", position: "overlay" });

                                                    var _MI = GetMilestone(XML_OUTPUT);

                                                    setTimeout(function () {

                                                        insertMilestoneData(Q, projectId, _MI['MilestoneData'], _MI['MilestoneAccountData']).then(function (data) {

                                                            var _r = data;

                                                            setTimeout(function () {

                                                                var alert2 = '<div class="alert alert-success"> ' +
                                                                //'<button type="button" class="close" data-dismiss="alert">×</button> ' +
                                                                '<strong>New Project </strong> successfully created. ' +
                                                              '</div>';

                                                                $("#alert").empty();
                                                                $("#alert").append(alert2);
                                                                $.isLoading("hide");

                                                                $("#imported_project_id", opener.document).val(projectId);

                                                                window.close();

                                                            }, 10000);//-- 10 sec

                                                        });

                                                    }, 4000);//-- 4 sec

                                                });

                                            });

                                        } else {

                                            var projectId = XML_OUTPUT['Import_1'][0].Value;

                                            var projectData = {
                                                //project_number: XML_OUTPUT['Import_1'][0].Value,
                                                project_type: XML_OUTPUT['Import_1'][1].Value,//Commercial
                                                Title: XML_OUTPUT['Import_1'][2].Value,
                                                project_address: XML_OUTPUT['Import_1'][3].Value,
                                                project_principle_name: XML_OUTPUT['Import_1'][4].Value,
                                                project_principle_address: XML_OUTPUT['Import_1'][5].Value,
                                                project_principle_email: XML_OUTPUT['Import_1'][6].Value,
                                                project_principle_business_name: XML_OUTPUT['Import_1'][7].Value,
                                                project_principle_abn: XML_OUTPUT['Import_1'][8].Value,
                                                project_principle_ect: XML_OUTPUT['Import_1'][9].Value,
                                                project_rep_name: XML_OUTPUT['Import_1'][10].Value,
                                                project_rep_address: XML_OUTPUT['Import_1'][11].Value,
                                                project_rep_email: XML_OUTPUT['Import_1'][12].Value,
                                                project_rep_business_name: XML_OUTPUT['Import_1'][13].Value,
                                                project_rep_abn: XML_OUTPUT['Import_1'][14].Value,
                                                project_rep_ect: XML_OUTPUT['Import_1'][15].Value,
                                                project_building_margin: XML_OUTPUT['Import_1'][16].Value,
                                                project_contingency: XML_OUTPUT['Import_1'][17].Value,
                                                project_status: 'Open'
                                            };

                                            $.isLoading("hide");
                                            $.isLoading({ text: "Update project data...", position: "overlay" });

                                            updateProjectData(Q, projectId, projectData).then(function () {

                                                $.isLoading("hide");
                                                $.isLoading({ text: "Clear existing data...", position: "overlay" });

                                                deleteProjectAccountData(Q, projectId, true).then(function () {

                                                    deleteMilestoneData(Q, projectId, true).then(function () {

                                                        $.isLoading("hide");
                                                        $.isLoading({ text: "Insert costing data...", position: "overlay" });

                                                        insertProjectAccountData(Q, projectId, GetCostings(XML_OUTPUT, myobAccounts)).then(function () {

                                                            $.isLoading("hide");
                                                            $.isLoading({ text: "Insert milestone data...", position: "overlay" });

                                                            var _MI = GetMilestone(XML_OUTPUT);

                                                            setTimeout(function () {

                                                                insertMilestoneData(Q, projectId, _MI['MilestoneData'], _MI['MilestoneAccountData']).then(function (data) {

                                                                    var _r = data;

                                                                    setTimeout(function () {

                                                                        var alert2 = '<div class="alert alert-success"> ' +
                                                                        //'<button type="button" class="close" data-dismiss="alert">×</button> ' +
                                                                        '<strong>New Project </strong> successfully created. ' +
                                                                      '</div>';

                                                                        $("#alert").empty();
                                                                        $("#alert").append(alert2);
                                                                        $.isLoading("hide");

                                                                        $("#imported_project_id", opener.document).val(projectId);

                                                                        window.close();

                                                                    }, 10000);//-- 10 sec

                                                                });

                                                            }, 4000);//-- 4 sec

                                                        });

                                                    });
                                                });

                                            });
                                        }

                                    });
                                    //==========================================================

                                } else {
                                    var alert = '<div class="alert alert-danger"> ' +
                                                  //'<button type="button" class="close" data-dismiss="alert">×</button> ' +
                                                  '<strong>MYOB </strong> validation failed. ' +
                                                '</div>';

                                    $("#alert").empty();
                                    $("#alert").append(alert);

                                    $.isLoading("hide");
                                }

                            } else {

                                var alert = '<div class="alert alert-danger"> ' +
                                                 //'<button type="button" class="close" data-dismiss="alert">×</button> ' +
                                                 '<strong>MYOB </strong> validation failed. ' +
                                               '</div>';

                                $("#alert").empty();
                                $("#alert").append(alert);

                                $.isLoading("hide");

                            }

                            return false;
                        },
                        error: function (data) {
                            return false;
                        }

                    });
                }

            } else {

                var alert = '<div class="alert alert-danger"> ' +
                                '<h4><strong>Invalid project type.</strong> Only accepted Commercial or Residential</h4>' +
                            '</div>';

                $("#alert").empty();
                $("#alert").append(alert);
            }

            return false;
        }

        var XML_OUTPUT = "";
        var X = XLSX;
        var XW = {
            /* worker message */
            msg: 'xlsx',
            /* worker scripts */
            rABS: './xlsxworker2.js',
            norABS: './xlsxworker1.js',
            noxfer: './xlsxworker.js'
        };

        var rABS = typeof FileReader !== "undefined" && typeof FileReader.prototype !== "undefined" && typeof FileReader.prototype.readAsBinaryString !== "undefined";
        var use_worker = typeof Worker !== 'undefined';
        var transferable = use_worker;
        var wtf_mode = false;

        function fixdata(data) {
            var o = "", l = 0, w = 10240;
            for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w, l * w + w)));
            o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w)));
            return o;
        }

        function ab2str(data) {
            var o = "", l = 0, w = 10240;
            for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint16Array(data.slice(l * w, l * w + w)));
            o += String.fromCharCode.apply(null, new Uint16Array(data.slice(l * w)));
            return o;
        }

        function s2ab(s) {
            var b = new ArrayBuffer(s.length * 2), v = new Uint16Array(b);
            for (var i = 0; i != s.length; ++i) v[i] = s.charCodeAt(i);
            return [v, b];
        }

        function xw_noxfer(data, cb) {
            var worker = new Worker(XW.noxfer);
            worker.onmessage = function (e) {
                switch (e.data.t) {
                    case 'ready': break;
                    case 'e': console.error(e.data.d); break;
                    case XW.msg: cb(JSON.parse(e.data.d)); break;
                }
            };
            var arr = rABS ? data : btoa(fixdata(data));
            worker.postMessage({ d: arr, b: rABS });
        }

        function xw_xfer(data, cb) {
            var worker = new Worker(rABS ? XW.rABS : XW.norABS);
            worker.onmessage = function (e) {
                switch (e.data.t) {
                    case 'ready': break;
                    case 'e': console.error(e.data.d); break;
                    default: xx = ab2str(e.data).replace(/\n/g, "\\n").replace(/\r/g, "\\r"); console.log("done"); cb(JSON.parse(xx)); break;
                }
            };
            if (rABS) {
                var val = s2ab(data);
                worker.postMessage(val[1], [val[1]]);
            } else {
                worker.postMessage(data, [data]);
            }
        }

        function xw(data, cb) {
            transferable = false;
            if (transferable) xw_xfer(data, cb);
            else xw_noxfer(data, cb);
        }

        function to_json(workbook) {
            var result = {};
            workbook.SheetNames.forEach(function (sheetName) {
                var roa = X.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                if (roa.length > 0) {
                    result[sheetName] = roa;
                }
            });
            return result;
        }

        function process_wb(wb) {

            XML_OUTPUT = to_json(wb);
            $('#xlf').val("");

            var Import_1 = XML_OUTPUT['Import_1'];
            SetImportData(Import_1[0].Value, Import_1[1].Value);

        }

        var xlf = document.getElementById('xlf');
        function handleFile(e) {
            rABS = false;
            use_worker = false;
            var files = e.target.files;
            var f = files[0];
            {
                var reader = new FileReader();
                var name = f.name;

                reader.onload = function (e) {
                    if (typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
                    var data = e.target.result;
                    if (use_worker) {
                        xw(data, process_wb);
                    } else {
                        var wb;
                        if (rABS) {
                            wb = X.read(data, { type: 'binary' });
                        } else {
                            var arr = fixdata(data);
                            wb = X.read(btoa(arr), { type: 'base64' });
                        }
                        process_wb(wb);
                    }
                };

                if (rABS) reader.readAsBinaryString(f);
                else reader.readAsArrayBuffer(f);
            }
        }

        if (xlf.addEventListener) xlf.addEventListener('change', handleFile, false);


        function SetImportData(projectNumber, projectType) {

            var content = "";
            var alert = "";

            if (projectType == "Commercial" || projectType == "Residential") {

                content = '<div class="col-lg-12 no-padding padding-for-large">' +
                                            '<div class="col-lg-5 col-md-6 col-sm-6 col-xs-12">' +
                                                //'<form class="form-horizontal">' +
                                                    '<div class="form-group">' +
                                                        '<div class="row">' +
                                                            '<div class="col-sm-6">' +
                                                                '<h4 class="project-label margin-bottom20">Project Number</h4>' +
                                                            '</div>' +
                                                            '<div class="col-sm-6">' +
                                                                '<input disabled="disabled" type="text" value="' + projectNumber + '" id="ProjectNumber" class="form-control">' +
                                                            '</div>' +
                                                        '</div>' +
                                                    '</div>' +
                                                //'</form>' +
                                            '</div>' +
                                            '<div class="col-lg-5 col-md-6 col-sm-6 col-xs-12">' +
                                                //'<form class="form-horizontal">' +
                                                    '<div class="form-group">' +
                                                        '<div class="row">' +
                                                            '<div class="col-sm-6">' +
                                                                '<h4 class="project-label margin-bottom20">Project Type</h4>' +
                                                             '</div>' +
                                                            '<div class="col-sm-6">' +
                                                                '<input disabled="disabled" type="text" value="' + projectType + '" id="ProjectType" class="form-control">' +
                                                            '</div>' +
                                                         '</div>' +
                                                    '</div>' +
                                                //'</form>' +
                                            '</div>' +
                                            '<div class="col-lg-5 col-md-6 col-sm-6 col-xs-12">' +
                                                '<div>' +
                                                    '<h4 class="project-label margin-bottom20">Is this correct ? &nbsp;' +
                                                        '<a id="confirmupload" onclick="confirmUpload()" class="btn btn-sm btn-default btn-export" >Confirm Upload</a>' +
                                                    '</h4>' +
                                                '</div>' +
                                            '</div>' +
                                        '</div>';


            } else {

                content = '<div class="col-lg-12 no-padding padding-for-large">' +
                                             '<div class="col-lg-5 col-md-6 col-sm-6 col-xs-12">' +
                                                 //'<form class="form-horizontal">' +
                                                     '<div class="form-group">' +
                                                         '<div class="row">' +
                                                             '<div class="col-sm-6">' +
                                                                 '<h4 class="project-label margin-bottom20">Project Number</h4>' +
                                                             '</div>' +
                                                             '<div class="col-sm-6">' +
                                                                 '<input disabled="disabled" type="text" value="' + projectNumber + '" id="ProjectNumber" class="form-control">' +
                                                             '</div>' +
                                                         '</div>' +
                                                     '</div>' +
                                                 //'</form>' +
                                             '</div>' +
                                             '<div class="col-lg-5 col-md-6 col-sm-6 col-xs-12">' +
                                                 //'<form class="form-horizontal">' +
                                                     '<div class="form-group">' +
                                                         '<div class="row">' +
                                                             '<div class="col-sm-6">' +
                                                                 '<h4 class="project-label margin-bottom20">Project Type</h4>' +
                                                              '</div>' +
                                                             '<div class="col-sm-6">' +
                                                                 '<input disabled="disabled" type="text" value="' + projectType + '" id="ProjectType" class="form-control">' +
                                                             '</div>' +
                                                          '</div>' +
                                                     '</div>' +
                                                 //'</form>' +
                                             '</div>' +
                                         '</div>';

                alert = '<div class="alert alert-danger"> ' +
                            '<h4><strong>Invalid project type.</strong> Only accepted Commercial or Residential</h4>' +
                        '</div>';
            }

            $("#alert").empty();
            $("#alert").append(alert);

            $("#import-data").empty();
            $("#import-data").append(content);
            $("#projectId").text(" " + projectNumber);
            $("#ProjectCode").val(projectNumber);

        }

        function IsCurrentParent(childAccount, parentAccount) {
            var res = false;

            Accounts.forEach(function (account) {
                if (account.Code == childAccount && account.ParentAccount == parentAccount) {
                    res = true;
                }
            });

            return res;
        }

        function SetProjectValues(Q, outputObj) {

            var deffered = Q.defer();

            var TotalContractValue = 0;
            var SupervisorAllocation = 0;
            var SupervisorCostIncurred = 0;
            var BuildingMargin = 0;
            var Insurances = 0;

            outputObj['Import_2'].forEach(function (imp) {
                var val = 0;
                var _cost = imp['Cost(ex GST)'].replace('$', '').replace(',', '').trim();
                if (_cost.indexOf("-") >= 0) {
                    val = parseFloat(_cost.replace('-', '').trim());
                } else
                    val = parseFloat(_cost);

                if (val > 0 || val < 0)
                    TotalContractValue += val;
            });

            BuildingMargin = outputObj['Import_1'][16].Value;

            var values = {

                Title: TotalContractValue.toString(),
                SupervisorAllocation: SupervisorAllocation.toString(),
                SupervisorCostIncurred: SupervisorCostIncurred.toString(),
                BuildingMargin: BuildingMargin.toString(),
                Insurances: Insurances.toString(),

            };

            deffered.resolve(insertDataToProjectValueList(Q, outputObj['Import_1'][0].Value, values).then(function () {

            }));

            return deffered.promise;
        }

        function SetCostings(Q, outputObj) {

            var deffered = Q.defer();
            var costings = [];

            Accounts.forEach(function (account) {

                if (account["ParentAccount"] === null) {

                    var Costed = 0;
                    var index = 0;

                    outputObj['Import_1'].forEach(function (imp) {
                        index++;

                        if (index > 18) {
                            if (IsCurrentParent(imp.Field, account["Code"])) {
                                Costed += parseInt(imp.Value);
                            }
                        }

                    });

                    if (Costed > 0 || Costed < 0) {
                        costings.push({ Title: account["Name"].toString().trim(), Costed: Costed.toString(), Incurred: parseInt(Costed * (70 / 100)).toString() });

                    } else {
                        costings.push({ Title: account["Name"].toString().trim(), Costed: "0", Incurred: "0" });
                    }
                }

            });

            deffered.resolve(insertDataToCostingsList(Q, outputObj['Import_1'][0].Value, costings).then(function () {

            }));

            return deffered.promise;
        }

        function GetCostings(outputObj, myobAccounts) {

            var costings = [];
            var index = 0;

            outputObj['Import_1'].forEach(function (imp) {
                index++;

                if (index > 18) {

                    var val = 0;
                    var _cost = imp.Value.toString().replace('$', '').replace(',', '').trim();

                    if (_cost.indexOf("-") >= 0) {
                        val = parseFloat(_cost.replace('-', '').trim()).toFixed(2);
                    } else
                        val = parseFloat(_cost).toFixed(2);

                    var project_account_incurred = 0;

                    if (myobAccounts != undefined) {
                        myobAccounts.forEach(function (acc) {
                            if (imp.Field == acc.DisplayID)
                                project_account_incurred = acc.Amount;
                        });

                    }

                    costings.push({ account_id: imp.Field, project_account_estimate: parseFloat(val), project_account_incurred: project_account_incurred });
                }

            });

            return costings;
        }

        function SetMilestone(Q, outputObj) {

            var deffered = Q.defer();

            var Milestones = [];

            outputObj['Import_2'].forEach(function (impMI) {

                var _milestomeForndInList = false;

                Milestones.forEach(function (MI) {
                    if (impMI.Milestone == MI.Title)
                        _milestomeForndInList = true;
                });

                if (_milestomeForndInList == false) {

                    var _milestone = impMI.Milestone;

                    var Dateforclaim = "";
                    var Progressclaimstage = "0";
                    var GSTincl = 0;
                    var Costallocated = 0;
                    var Costincurred = 0;

                    outputObj['Import_2'].forEach(function (imp) {

                        if (imp['Milestone'] == _milestone) {
                            var val = 0;
                            var _cost = imp['Cost(ex GST)'].replace('$', '').replace(',', '').trim();

                            if (_cost.indexOf("-") >= 0) {
                                val = parseFloat(_cost.replace('-', '').trim());
                            } else
                                val = parseFloat(_cost);

                            if (val > 0 || val < 0)
                                Costallocated += parseFloat(val);

                            Dateforclaim = imp['EndDate'];
                        }

                    });

                    GSTincl = Costallocated * 10 / 100;
                    Progressclaimstage = Costallocated + Costallocated * 20 / 100;

                    Milestones.push(
                        {
                            DateForClaim: Dateforclaim,
                            ProgressClaimStage: Progressclaimstage.toString(),
                            GST: GSTincl.toString(),
                            CostAllocated: Costallocated.toString(),
                            CostIncurred: Costincurred.toString(),
                            //Milestone: impMI.Milestone,
                            Title: impMI.Milestone
                        });

                }

            });

            deffered.resolve(insertMilestoneData(Q, outputObj['Import_1'][0].Value, Milestones).then(function () {

            }));

            return deffered.promise;
        }

        function GetMilestone(outputObj) {

            var MilestoneData = [];
            var MilestoneAccountData = [];

            outputObj['Import_2'].forEach(function (impMI) {

                var _milestomeForndInList = false;

                MilestoneData.forEach(function (MI) {
                    if (impMI.Milestone == MI.milestone_name)
                        _milestomeForndInList = true;
                });

                if (_milestomeForndInList == false) {

                    var _milestone = impMI.Milestone;

                    var _date_start = "";
                    var _date_end = "";
                    var _actual_date = "";
                    var _milestone_claim_estimate = 0;
                    var _milestone_claim_actual = 0;

                    outputObj['Import_2'].forEach(function (imp) {

                        if (imp['Milestone'] == _milestone) {
                            var val = 0;
                            var _cost = imp['Cost(ex GST)'].replace('$', '').replace(',', '').trim();

                            if (_cost.indexOf("-") >= 0) {
                                val = parseFloat(_cost.replace('-', '').trim()).toFixed(2);
                            } else
                                val = parseFloat(_cost).toFixed(2);

                            if (val > 0 || val < 0)
                                _milestone_claim_estimate += parseFloat(val);

                            _date_start = imp['StartDate'];
                            _date_end = imp['EndDate'];
                            _actual_date = new Date().toDateString();
                        }

                    });

                    MilestoneData.push(
                        {
                            milestone_name: _milestone,
                            milestone_date_start: _date_start,
                            milestone_date_end: _date_end,
                            milestone_claim_estimate: _milestone_claim_estimate,
                            milestone_claim_actual: _milestone_claim_actual,
                            milestone_claim_actual_date: _actual_date
                        });

                }

                var __val = 0;
                var __cost = impMI['Cost(ex GST)'].replace('$', '').replace(',', '').trim();

                if (__cost.indexOf("-") >= 0) {
                    __val = parseFloat(__cost.replace('-', '').trim()).toFixed(2);
                } else
                    __val = parseFloat(__cost).toFixed(2);

                MilestoneAccountData.push({
                    milestone_account_estimate: parseFloat(__val),
                    account_id: impMI['Account'].toString().trim(),
                    milestone_name: impMI['Milestone'].toString().trim(),
                });

            });

            return { MilestoneData: MilestoneData, MilestoneAccountData: MilestoneAccountData };

        }

    </script>

</asp:Content>
