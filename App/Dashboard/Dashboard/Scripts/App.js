﻿'use strict';

var context = SP.ClientContext.get_current();
var user = context.get_web().get_currentUser();
var Accounts = [];
var LinkDocuments = [];

var ProjectScheduleMonitoringEstimates = [];
var ProjectScheduleMonitoringActuals = [];
var CostBudgetVsMoneyReceivedDollars = [];
var CumulativeMilestoneClaims = [];
var CumulativeGrossMargin = [];

// This code runs when the DOM is ready and creates a context object which is needed to use the SharePoint object model
//get current Context
var context = SP.ClientContext.get_current();

//get current user
var user = context.get_web().get_currentUser();

var hostweburl;
var appweburl;
var documentLibraryName = 'Dashboard1';
var scriptbase;

$(document).ready(function () {

    //remove full screen mode page title and logo 
    //$("#s4-titlerow").remove();

    hostweburl = decodeURIComponent(getQueryStringParameter("SPHostUrl"));
    appweburl = decodeURIComponent(getQueryStringParameter("SPAppWebUrl"));

    if (appweburl.indexOf("#") >= 0)
        appweburl = appweburl.replace("#", "");

    if (hostweburl.indexOf("#") >= 0)
        hostweburl = appweburl.replace("#", "");

    scriptbase = hostweburl + "/_layouts/15/";

    $("#appweburl").val(appweburl);
    $("#hostweburl").val(hostweburl);

    if (hostweburl != "no" && appweburl != "no") {
        $.getScript(scriptbase + "SP.RequestExecutor.js", initialDataLoad);
    } 

});

// Function to retrieve a query string value.
function getQueryStringParameter(paramToRetrieve) {

    var l = document.URL.split("?").length;

    if (document.URL.split("?").length > 1) {

        var params =
            document.URL.split("?")[1].split("&");
        var strParams = "";
        for (var i = 0; i < params.length; i = i + 1) {
            var singleParam = params[i].split("=");
            if (singleParam[0] == paramToRetrieve)
                return singleParam[1];
        }
    }

    return "no";
}

function execCrossDomainRequest() {
    //var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/lists?@target='" + hostweburl + "'";
    //var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Dashboard1')/items?@target='" + hostweburl + "'&$select=Title";
    //var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/title?@target='" +hostweburl + "'";
    var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/GetFolderByServerRelativeUrl('" + documentLibraryName + "')/Files?@target='" + hostweburl + "'&$select=Name,TimeLastModified,ServerRelativeUrl&$orderby=TimeLastModified desc&$expand=Author";


    var executor = new SP.RequestExecutor(appweburl);
    executor.executeAsync({
        url: requestUrl,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: successHandler,
        error: errorHandler
    });
}

// Prints the data to the page.
function successHandler(data) {
    var jsonObject = JSON.parse(data.body);
    var blogsHTML = "";
    var recentDocuments = [];
    var tr;
    var results = jsonObject.d.results;
    //console.log(results);

    for (var i = 0; i < results.length; i++) {
        var documentDateIso = results[i].TimeLastModified;
        var documentDate = new Date(documentDateIso).toLocaleDateString();
        var fullDocumentName = results[i].Name;
        var splitDocumentName = fullDocumentName.split('.');
        var documentName = splitDocumentName[0];
        //var documentUrl = results[i].ServerRelativeUrl;      
        var documentAuthor = results[i].Author.Title;
        var documentUrl = hostweburl + "/" + documentLibraryName + "/" + fullDocumentName;

        var recentDocument = { DocumentDate: documentDate, DocumentName: documentName, DocumentUrl: documentUrl, DocumentAuthor: documentAuthor };
        recentDocuments.push(recentDocument);

        //blogsHTML = "<p>" + documentDate + "    " + results[i].Name +  " " + results[i].Author.Title + "</p></br>";
        //$('#message').append(blogsHTML);

        var tr;
        tr = $('<tr/>');
        tr.append("<td>" + documentDate + "</td>");
        tr.append("<td> <a href=" + documentUrl + ">" + documentName + "</a></td>");
        tr.append("<td>" + documentAuthor + "</td>");
        $('#recentDocument').children('tbody').append(tr);

    }
}

// Prints the error message to the page.
function errorHandler(data, errorCode, errorMessage) {
    $('#message').text("Could not complete cross-domain call: " + errorMessage);
    document.getElementById("listData").innerText = "Could not complete cross-domain call: " + errorMessage;
}

// This function prepares, loads, and then executes a SharePoint query to get the current users information
function getUserName() {
    context.load(user);
    context.executeQueryAsync(onGetUserNameSuccess, onGetUserNameFail);
}

// This function is executed if the above call is successful
// It replaces the contents of the 'message' element with the user name
function onGetUserNameSuccess() {

    $('#message').text('Hello ' + user.get_title());
}

// This function is executed if the above call fails
function onGetUserNameFail(sender, args) {
    alert('Failed to get user name. Error:' + args.get_message());
}

function initialDataLoad() {

    //var projectID = '1300';

    getProjectData(Q, "");
    getBusinessLevelData(Q);

    //GetDocumentsForFolder(Q, false);
}

function getItemIdByKey(Q, listName, columnName, key, ID) {

    var outPut = [];
    var results = [];
    var result;
    var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('" + listName + "')/items?@target='" + hostweburl + "'&$select=" + ID + "&$filter=" + columnName + " eq '" + key + "' ";

    var deffered = Q.defer();

    var executor = new SP.RequestExecutor(appweburl);
    executor.executeAsync({
        url: requestUrl,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;

            if (results.length > 0) {
                result = results[0][ID];
            }
            else
                result = null;

            deffered.resolve(result);
        },
        error: function (data) {
            result = "inside error function";
            deffered.reject = data;
        }
    });

    return deffered.promise;
}

function getItemIdByKeyForNew(Q, listName, columnName, key, ID) {

    var _appweburl = $("#appweburl", opener.document).val();
    var _hostweburl = $("#hostweburl", opener.document).val();

    var outPut = [];
    var results = [];
    var result;
    var requestUrl = _appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('" + listName + "')/items?@target='" + _hostweburl + "'&$select=" + ID + "&$filter=" + columnName + " eq '" + key + "' ";

    var deffered = Q.defer();

    var executor = new SP.RequestExecutor(_appweburl);

    executor.executeAsync({
        url: requestUrl,
        async: false,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;

            if (results.length > 0) {
                result = results[0][ID];
            }
            else
                result = null;

            //console.log("getItemIdByKeyForNew-Success~" + data);
            deffered.resolve(result);
        },
        error: function (data) {
            result = "inside error function";
            //console.log("getItemIdByKeyForNew-Error~" + data);
            deffered.reject = data;
        }
    });

    return deffered.promise;
}

function SortByDateForClaim(a, b) {
    var aDate = a.DateForClaim.toLowerCase();
    var bDate = b.DateForClaim.toLowerCase();
    return ((aDate < bDate) ? -1 : ((aDate > bDate) ? 1 : 0));
}

function SortByCostingsSummaryAccountMyobId(a, b) {
    var aDes = a.SummaryAccountMyobId;
    var bDes = b.SummaryAccountMyobId;
    return ((aDes < bDes) ? -1 : ((aDes > bDes) ? 1 : 0));
}

function SortByMilestone(a, b) {
    var aMi = a.Milestone;
    var bMi = b.Milestone;
    return ((aMi < bMi) ? -1 : ((aMi > bMi) ? 1 : 0));
}

//get projectData fro navigation-- Here projectId for AddNewProject Selection
function getProjectData(Q, projectId) {

    var deferred = Q.defer();

    var outPut = [];
    var results = [];
    var result;
    //var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Project')/items?@target='" + hostweburl + "'&$select=Title,project_number,project_status";
    var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Project')/items?@target='" + hostweburl + "'&$select=Title,project_number,project_status";

    var executor = new SP.RequestExecutor(appweburl);
    executor.executeAsync({
        url: requestUrl,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;

            if (results !== undefined && results.length > 0) {
                for (var i = 0; i < results.length; i++) {
                    outPut.push({ projectNumber: results[i].Title, actualProjectValue: results[i].project_number, status: results[i].project_status });
                }
            }

            deferred.resolve(outPut);

            if (outPut !== undefined && outPut.length > 0) {

                $("#sidebar-wrapper").empty();
                var _open = "";
                var _close = "";

                outPut.forEach(function (project) {

                    if (project['status'] == "Open") {
                        _open += "<li onClick='clickOnNave(this);' ><a href='#'><span>" + project['actualProjectValue'] + "</span></a></li>";
                    } else if (project['status'] == "Past") {
                        _close += "<li onClick='clickOnNave(this);' ><a href='#'><span>" + project['actualProjectValue'] + "</span></a></li>";
                    }

                });

                var _nav = "<div id='cssmenu'>" +
                             "<ul>" +
                                "<li onClick='clickOnNave(this);' ><a href='#'><span>All Projects</span></a></li>" +
                                "<li class='active has-sub'><a id='a-open' onClick='clickHasSub(this)' href='#'><span>Open Projects</span></a>" +
                                   "<ul>" + _open + "</ul>" +
                                "</li>" +
                                 "<li class='last has-sub'><a onClick='clickHasSub(this)' href='#'><span>Past Projects</span></a>" +
                                   "<ul>" + _close + "</ul>" +
                                "</li>" +
                             "</ul>" +
                             "</div>";

                $("#sidebar-wrapper").append(_nav);

                $('#cssmenu>ul>li.has-sub>a').append('<span class="holder"></span>');

                getColor();

                $("#a-open").trigger("click");

                $("#totalcontractvalue").val('');
                $("#supervisorallocation").val('');
                $("#supervisorcostincurred").val('');
                $("#buildingmargin").val('');
                $("#insurances").val('');

                $("#costings").find('tbody').empty();

                $("#milestone").find('tbody').empty();
                $("#milestone").find('tbody').append('<tr><td class="wrapper-table-cell">' +
                '<table class="table table-bordered table-striped table-condensed table-responsive">' +
                           '<thead>' +
                               '<tr>' +
                                   '<th class="table-head-th">No data to display</th>' +
                               '</tr>' +
                           '</thead>' +
                   '</table>' +
                '</td></tr>');

                $('#status').prop('disabled', true);

                if (projectId != undefined && projectId != "") {

                    var listItems = $("#cssmenu li");

                    listItems.each(function (idx, li) {
                        var item = $(li);
                        if (item.find('a') != undefined && item.find('a').text() == projectId) {

                            setTimeout(function () {

                                clickOnNave($(li));

                            }, 2000);
                            
                        }

                    });

                }

                //$("#insurances").attr("disabled", 'disabled');

                /*getProjectValueAverage(Q).then(function (averageValues) {
                    $("#totalcontractvalue").val(parseFloat(averageValues.AvgTotalContractValue).toFixed(2));
                    $("#buildingmargin").val(parseFloat(averageValues.AvgBuildingMargin).toFixed(2));

                });*/

                $("#grdVariations .k-grid-toolbar").hide();
                $("#grdVariations").removeAttr('style');

                getAverageProjectContractAndBuildingMargin(Q);
            }

        },
        error: function (data) {
            deferred.reject(outPut);
            result = "inside error function";
        }
    });

    return deferred.promise;
}

function getAllProjectDetails(Q) {
    var deferred = Q.defer();
    var outPut = [];
    var results = [];
    var result;
    var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Project')/items?@target='" + hostweburl + "'&$select=Title,project_number,project_status,project_type";

    var executor = new SP.RequestExecutor(appweburl);
    executor.executeAsync({
        url: requestUrl,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;

            if (results !== undefined && results.length > 0) {
                for (var i = 0; i < results.length; i++) {
                    outPut.push({ projectName: results[i].Title, projectNumber: results[i].project_number, status: results[i].project_status, projectType: results[i].project_type });
                }
            }

            deferred.resolve(outPut);
            //console.log(outPut);

        },

        error: function (data) {
            deferred.reject(outPut);
            result = "inside error function";
        }
    });
    return deferred.promise;
}

function clickOnNave(e) {

    $(e).toggleClass("selected");
    $(e).siblings().removeClass("selected");
    $("#project-label").text($(e).find('a').text());
   
    projectId = 0;

    if ($(e).find('a') != undefined && $(e).find('a').text() != "All Projects") {

        projectId = $(e).find('a').text();
        
        //getProjectValues(Q, projectId);
        //getProjectStatus(Q, projectId);

        getProjectLevelData(Q, projectId);
        //getCostingsData(projectId);
        getCostingsDataByProjectID(Q, projectId);
        //getMilestoneData(projectId);
        milestoneCalenderData(Q, projectId);
        //Recent documents
        getRecentDocumentNames(Q, projectId);
        //Total project claim value And Remaining Claim Value
        setProjectAndRemainingClaim(Q, projectId);

        $('#status').prop('disabled', false);
        $("#grdVariations .k-grid-toolbar").show();
        $("#grdVariations").removeAttr('style');

        //$("#insurances").removeAttr("disabled");


    } else {

        $("#grdVariations .k-grid-toolbar").hide();
        $("#grdVariations").removeAttr('style');

        $("#totalcontractvalue").val('');
        $("#supervisorallocation").val('');
        $("#supervisorcostincurred").val('');
        $("#buildingmargin").val('');
        $("#insurances").val('');

        $("#costings").find('tbody').empty();
        $('#recentDocument').children('tbody').empty();

        $("#milestone").find('tbody').empty();
        $("#milestone").find('tbody').append('<tr><td class="wrapper-table-cell">' +
        '<table class="table table-bordered table-striped table-condensed table-responsive">' +
                   '<thead>' +
                       '<tr>' +
                           '<th class="table-head-th">No data to display</th>' +
                       '</tr>' +
                   '</thead>' +
           '</table>' +
        '</td></tr>');

        $('#status').prop('disabled', true);

        getAverageProjectContractAndBuildingMargin(Q);

        setProjectAndRemainingClaim(Q);

    }

    $('#grdVariations').data('kendoGrid').dataSource.read();
    $('#grdVariations').data('kendoGrid').refresh();
}

//update project data
function updateProjectData(Q, projectId, itemProperties) {

    var deffered = Q.defer();

    var projectListName = 'Project'
    var columnName = 'project_number';
    var ID = 'ID';

    //get relavent lookup ID, before insert data
    deffered.resolve(getItemIdByKeyForNew(Q, projectListName, columnName, projectId, ID).then(function (listItemId) {
        updateListItemFromNew(Q, projectListName, listItemId, itemProperties);
    }));

    return deffered.promise;
}

//update project status
function updateProjectStatus(Q, projectId, itemProperties) {

    var deffered = Q.defer();

    var projectListName = 'Project'
    var columnName = 'project_number';
    var ID = 'ID';

    //get relavent lookup ID, before insert data
    deffered.resolve(getItemIdByKey(Q, projectListName, columnName, projectId, ID).then(function (listItemId) {
        updateListItem(projectListName, listItemId, itemProperties);
    }));

    return deffered.promise;
}

//update business level data
function updateBusinessLevelData(Q, itemProperties) {
    var listName = 'Business';
    getBusinesData(Q).then(function (businessData) {
        var ItemId = businessData[0].ID;
        updateListItem(listName, ItemId, itemProperties);
    });
}

//Check Project Exist
function checkProjectExistFromNew(Q, projectID) {

    var deffered = Q.defer();

    var _appweburl = $("#appweburl", opener.document).val();
    var _hostweburl = $("#hostweburl", opener.document).val();

    var requestUrl = _appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Project')/items?@target='" + _hostweburl + "'&$select=Title&$filter=project_number eq '" + projectID + "'&$top=1 ";

    var executor = new SP.RequestExecutor(_appweburl);

    executor.executeAsync({
        url:requestUrl,
        method: "GET",  // instead of "type" from $.ajax has to use "method"!
        async: false,
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {

            var jsonObject = JSON.parse(data.body);
            var results = jsonObject.d['results'];

            deffered.resolve(results);
        },
        error: function (data) {
            //$("#message").html("Failed to create item.");
            deffered.reject(data);
        }
    });

    return deffered.promise;
}

//Insert data to sharepoint list with Q
function createItemQ(Q, metadata, listName) {

    var deffered = Q.defer();

    var formattedListItemName = listName.replace(/\s+/g, "");

    var item = $.extend({
        "__metadata": { "type": "SP.Data." + formattedListItemName[0].toUpperCase() + formattedListItemName.substring(1) + "ListItem" }
    }, metadata);

    var url = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('" + listName + "')/items?@target='" + hostweburl + "'";
    var bodyItem = JSON.stringify(item);

    var executor = new SP.RequestExecutor(appweburl);

    executor.executeAsync({
        url: url,
        method: "POST",  // instead of "type" from $.ajax has to use "method"!
        body: bodyItem,  // instead of "data" from $.ajax has to use "body"!
        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
            "content-type": "application/json;odata=verbose", // set content-type here
            //"content-length": bodyItem.length,
            "X-RequestDigest": $("#__REQUESTDIGEST").val()
        },
        success: function (data) {
            var body = JSON.parse(data.body);
            deffered.resolve(body.d.ID);
        },
        error: function (data) {
            //$("#message").html("Failed to create item.");
            deffered.reject("Error");
        }
    });

    return deffered.promise;
}

//Insert data to sharepoint list
function createItem(metadata, listName) {

    var formattedListItemName = listName.replace(/\s+/g, "");

    var item = $.extend({
        "__metadata": { "type": "SP.Data." + formattedListItemName[0].toUpperCase() + formattedListItemName.substring(1) + "ListItem" }
    }, metadata);

    var url = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('" + listName + "')/items?@target='" + hostweburl + "'";
    var bodyItem = JSON.stringify(item);

    var executor = new SP.RequestExecutor(appweburl);

    executor.executeAsync({
        url: url,
        method: "POST",  // instead of "type" from $.ajax has to use "method"!
        body: bodyItem,  // instead of "data" from $.ajax has to use "body"!
        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
            "content-type": "application/json;odata=verbose", // set content-type here
            //"content-length": bodyItem.length,
            "X-RequestDigest": $("#__REQUESTDIGEST").val()
        },
        success: function (data) {
            var body = JSON.parse(data.body);
           
        },
        error: function (data) {
            $("#message").html("Failed to create item.");
        }
    });
}

function createItemFromNew(Q, metadata, listName) {

    var deffered = Q.defer();

    var _appweburl = $("#appweburl", opener.document).val();
    var _hostweburl = $("#hostweburl", opener.document).val();

    var formattedListItemName = listName.replace(/\s+/g, "");

    var item = $.extend({
        "__metadata": { "type": "SP.Data." + formattedListItemName[0].toUpperCase() + formattedListItemName.substring(1) + "ListItem" }
    }, metadata);

    var url = _appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('" + listName + "')/items?@target='" + _hostweburl + "'";
    var bodyItem = JSON.stringify(item);

    var executor = new SP.RequestExecutor(_appweburl);

    executor.executeAsync({
        url: url,
        method: "POST",  // instead of "type" from $.ajax has to use "method"!
        body: bodyItem,  // instead of "data" from $.ajax has to use "body"!
        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
            "content-type": "application/json;odata=verbose", // set content-type here
            //"content-length": bodyItem.length,
            "X-RequestDigest": $("#__REQUESTDIGEST").val()
        },
        success: function (data) {
            var body = JSON.parse(data.body);
            //$("#message").html("Item created.");
            deffered.resolve(data);
        },
        error: function (data) {
            //$("#message").html("Failed to create item.");
            deffered.reject(data);
        }
    });

    return deffered.promise;
}

//insert data to Costings list
function insertDataToCostingsList(Q, projectId, costingData) {

    var deffered = Q.defer();

    var mainListName = 'Project'
    var listName = 'Costing';
    var columnName = 'project_number';
    var ID = 'ID';

    //get relavent lookup ID, before insert data
    deffered.resolve(getItemIdByKeyForNew(Q, mainListName, columnName, projectId, ID).then(function (lookupId) {

        if (costingData.length > 0) {
            for (var j = 0; j < costingData.length; j++) {

                //append lookup ID to array
                costingData[j]['ProjectNumberId'] = lookupId;

                //insert costing data 
                //createItem(costingData[j], listName);
                createItemFromNew(Q, costingData[j], listName);

            }
        }
    }));

    return deffered.promise;

}

//Insert data to project value list
function insertDataToProjectValueList(Q, projectId, projectValueData) {

    var deffered = Q.defer();

    var mainListName = 'Project'
    var listName = 'Project Value';
    var columnName = 'project_number';
    var ID = 'ID';
    //var lookupId = [];

    deffered.resolve(getItemIdByKeyForNew(Q, mainListName, columnName, projectId, ID).then(function (lookupId) {

        if (projectValueData) {
            projectValueData['ProjectNumberId'] = lookupId;

            //insert costing data 
            //createItem(projectValueData, listName);
            createItemFromNew(Q, projectValueData, listName);

        }
    }));

    return deffered.promise;
}

function createFolddersInLibrary(Q, libraryPath, folderName) {

    var deferred = Q.defer();

    var url = appweburl + "/_api/SP.AppContextSite(@target)/web/getfolderbyserverrelativeurl('" + libraryPath + "')/folders?@target= '" + hostweburl + "' ";

    var metadata = "{ '__metadata': { 'type': 'SP.Folder' }, 'ServerRelativeUrl': '" + folderName + "'}"

    var executor = new SP.RequestExecutor(appweburl);

    executor.executeAsync({
        url: url,
        method: "POST",
        body: metadata,
        headers: {
            "accept": "application/json; odata=verbose",
            "content-type": "application/json; odata=verbose"
        },
        success: function (data) {          
            var message = JSON.parse(data.body);
            deferred.resolve(message.Name);
            //console.log(message);
        },
        error: function (data) {
            var message = JSON.parse(data.body);         
            //console.log(message);
            deferred.reject(message);
        }
    });

    return deferred.promise;
}

//insert data to Project Header List
function insertDataToProgectHeader(headerValue) {
    var listName = 'Project Header';

    if (headerValue) {
        createItem(headerValue, listName);
    }
}

function getData() {
    var listName = 'List2';
    var columnName = 'Subject';
    var key = 'English';
    var ID = 'ID';

    var result = getItemIdByKey(listName, columnName, key, ID);

    //console.log(result);
}

//Validate ProjectCode with MYOB
function getMYOBProjectCodeValidate(AuthorizationCode, ProjectCode) {

    var externalUrl = "http://dixonmyobdataservice.cloudapp.net/MYOBDataSvc.svc/ProjectCodeExist";
    var jsonData = { AuthorizationCode: AuthorizationCode, ProjectCode: ProjectCode };

    $.ajax({
        url: "../_api/SP.WebProxy.invoke",
        type: "POST",
        data: JSON.stringify(
            {
                "requestInfo": {
                    "__metadata": { "type": "SP.WebRequestInfo" },
                    "Url": externalUrl, //this is the uri that calls the webservice api
                    "Method": "POST",
                    "Headers": {
                        "results": [{
                            "__metadata": { "type": "SP.KeyValue" },
                            "Key": "Accept",
                            "Value": "application/json;odata=verbose",
                            "ValueType": "Edm.String"
                        }, {
                            __metadata: { type: "SP.KeyValue" },
                            Key: "Content-Type",
                            Value: "application/json",
                            ValueType: "Edm.String"
                        }]
                    },
                    "Body": JSON.stringify(jsonData), //this is the data to be send to the webservice
                }
            }),
        headers: {
            "Accept": "application/json;odata=verbose",
            "Content-Type": "application/json;odata=verbose",
            "X-RequestDigest": $("#__REQUESTDIGEST").val()
        },
        success: function (data) {

            //console.log(data);

            if (data.d.Invoke.StatusCode == 200) {
                var body = JSON.parse(data.d.Invoke.Body);
                return body;
            }

            return false;
        },
        error: function (data) {
            return false;
        }

    });
}

//Get Get Milestone Cost Incurred from MYOB
function getMilestoneCostIncurred(Q, ProjectCode, Milestones) {

    var deferred = Q.defer();

    var externalUrl = "http://dixonmyobdataservice.cloudapp.net/MYOBDataSvc.svc/GetMilestoneCostIncurred";
    var jsonData = { ProjectCode: ProjectCode, Milestones: Milestones };

    $.ajax({
        url: "../_api/SP.WebProxy.invoke",
        type: "POST",
        data: JSON.stringify(
            {
                "requestInfo": {
                    "__metadata": { "type": "SP.WebRequestInfo" },
                    "Url": externalUrl, //this is the uri that calls the webservice api
                    "Method": "POST",
                    "Headers": {
                        "results": [{
                            "__metadata": { "type": "SP.KeyValue" },
                            "Key": "Accept",
                            "Value": "application/json;odata=verbose",
                            "ValueType": "Edm.String"
                        }, {
                            __metadata: { type: "SP.KeyValue" },
                            Key: "Content-Type",
                            Value: "application/json",
                            ValueType: "Edm.String"
                        }]
                    },
                    "Body": JSON.stringify(jsonData), //this is the data to be send to the webservice
                }
            }),
        headers: {
            "Accept": "application/json;odata=verbose",
            "Content-Type": "application/json;odata=verbose",
            "X-RequestDigest": $("#__REQUESTDIGEST").val()
        },
        success: function (data) {

            if (data.d.Invoke.StatusCode == 200) {
                var body = JSON.parse(data.d.Invoke.Body);
                deferred.resolve(body['MilestoneCostIncurreds']);

            } else {
                deferred.resolve([]);

            }
        },
        error: function (data) {
            deferred.reject([]);
        }

    });

    return deferred.promise;
}

//Set Accounts Array From Lists
function getAccounts() {

    Accounts = [];

    getSummaryAccountDetails(Q, true).then(function (summaryAccounts) {

        summaryAccounts.forEach(function (sumAcc) {

            getAccountIDBySummaryAccountId(Q, sumAcc['summaryAccountMyobId'], sumAcc['summaryAccountName'], true).then(function (accountDetails) {

                accountDetails.forEach(function (acc) {
                    Accounts.push({ Code: acc['account_myob_id'], ParentAccount: acc['summaryAccountMyobId'] });
                    //console.log(JSON.stringify(acc));
                });

            });

        });


    });

}

//get summary accounts details
function getSummaryAccountDetails(Q, isnew) {

    var deffered = Q.defer();
    var summaryAccounts = [];
    var outPut = [];
    var results = [];
    var result;

    var __appweburl = appweburl;
    var __hostweburl = hostweburl;

    if (isnew) {
        __appweburl = $("#appweburl", opener.document).val();
        __hostweburl = $("#hostweburl", opener.document).val();
    }

    var requestUrl = __appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Summary Account')/items?@target='" + __hostweburl + "'&$select=Title,summary_account_myob_id";

    var executor = new SP.RequestExecutor(__appweburl);
    executor.executeAsync({
        url: requestUrl,
        method: "GET",
        //async: false,
        //cache: false,
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;

            if (results !== undefined && results.length > 0) {
                for (var i = 0; i < results.length; i++) {
                    //Accounts.push({ AccountCode: results[i].Title, AccountName: results[i].AccountName, ParentAccount: results[i].ParentAccount });
                    summaryAccounts.push({ summaryAccountName: results[i].Title, summaryAccountMyobId: results[i].summary_account_myob_id });
                }
            }

            deffered.resolve(summaryAccounts);

        },
        error: function (data) {
            deffered.reject(data);
            result = "inside error function";
        }
    });

    return deffered.promise;
}

//get account IDs by summary account ID
function getAccountIDBySummaryAccountId(Q, summaryAccountMyobId, summaryAccountName, isnew) {

    var deffered = Q.defer();
    var accounts = [];
    var accountsDetails = [];
    var outPut = [];
    var results = [];
    var result;

    var __appweburl = appweburl;
    var __hostweburl = hostweburl;

    if (isnew) {
        __appweburl = $("#appweburl", opener.document).val();
        __hostweburl = $("#hostweburl", opener.document).val();
    }

    var requestUrl = __appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Account')/items?@target='" + __hostweburl + "'&$select=Title,account_myob_id&$filter=summary_account_myob_id/summary_account_myob_id eq '" + summaryAccountMyobId + "'";

    var executor = new SP.RequestExecutor(__appweburl);
    executor.executeAsync({
        url: requestUrl,
        method: "GET",
        async: false,
        //cache: false,
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;

            if (results !== undefined && results.length > 0) {
                for (var i = 0; i < results.length; i++) {
                    //Accounts.push({ AccountCode: results[i].Title, AccountName: results[i].AccountName, ParentAccount: results[i].ParentAccount });
                    accounts.push({ accountName: results[i].Title, accountMyobId: results[i].account_myob_id, summaryAccountMyobId: summaryAccountMyobId });
                }
            }

            accounts['summaryAccountName'] = summaryAccountName;
            accounts['summaryAccountMyobId'] = summaryAccountMyobId;
            
            deffered.resolve(accounts);

        },
        error: function (data) {
            deffered.reject(data);
            result = "inside error function";
        }
    });

    return deffered.promise;
}

//get projetc account details by project number
function getProjectAccountByProjectNumber(Q, projectId, isnew) {
    var deffered = Q.defer();
    var projectAccounts = [];
    var outPut = [];
    var results = [];
    var result;

    var __appweburl = appweburl;
    var __hostweburl = hostweburl;

    if (isnew) {
        __appweburl = $("#appweburl", opener.document).val();
        __hostweburl = $("#hostweburl", opener.document).val();
    }

    var requestUrl = __appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Project Account')/items?@target='" + __hostweburl + "'&$select=project_account_estimate,project_account_incurred,account_myob_id/account_myob_id&$expand=account_myob_id/account_myob_id&$filter=project_number/project_number eq '" + projectId + "'";

    var executor = new SP.RequestExecutor(__appweburl);
    executor.executeAsync({
        url: requestUrl,
        method: "GET",
        //async: false,
        //cache: false,
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;

            if (results !== undefined && results.length > 0) {
                for (var i = 0; i < results.length; i++) {
                    //Accounts.push({ AccountCode: results[i].Title, AccountName: results[i].AccountName, ParentAccount: results[i].ParentAccount });
                    projectAccounts.push({ projectId: projectId, accountMyobId: results[i].account_myob_id.account_myob_id, projectAccountEstimate: results[i].project_account_estimate, projectAccountIncurred: results[i].project_account_incurred });
                }
            }
            deffered.resolve(projectAccounts);

        },
        error: function (data) {
            deffered.reject(data);
            result = "inside error function";
        }
    });

    return deffered.promise;
}

//get sumOf_project_account_estimate
function getSumOfProjectAccountEstimate(Q, projectId, isnew) {
    var deffered = Q.defer();
    //var projectAccounts = [];
    //var outPut = [];
    var results = [];
    var result = 0;

    var __appweburl = appweburl;
    var __hostweburl = hostweburl;

    if (isnew) {
        __appweburl = $("#appweburl", opener.document).val();
        __hostweburl = $("#hostweburl", opener.document).val();
    }

    var requestUrl = __appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Project Account')/items?@target='" + __hostweburl + "'&$select=project_account_estimate&$filter=project_number/project_number eq '" + projectId + "'";

    var executor = new SP.RequestExecutor(__appweburl);
    executor.executeAsync({
        url: requestUrl,
        method: "GET",
        //async: false,
        //cache: false,
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;

            if (results !== undefined && results.length > 0) {
                for (var i = 0; i < results.length; i++) {
                    //Accounts.push({ AccountCode: results[i].Title, AccountName: results[i].AccountName, ParentAccount: results[i].ParentAccount });
                    //projectAccounts.push({ accountMyobId: results[i].account_myob_id.account_myob_id, projectAccountEstimate: results[i].project_account_estimate, projectAccountIncurred: results[i].project_account_incurred });

                    result +=  results[i].project_account_estimate
                }
            }
            deffered.resolve(result);

        },
        error: function (data) {
            deffered.reject(result);
            result = "inside error function";
        }
    });

    return deffered.promise;
}

//insert project details
function insertDataToProject(projectData) {

    var deffered = Q.defer();
    var listName = 'Project';

    if (projectData) {
        createItemFromNew(projectData, listName);
    }

}

function getProjectHeaderId(Q) {
    var deffered = Q.defer();
    var outPut = [];
    var results = [];
    var result;
    var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Project Header')/items?@target='" + hostweburl + "'&$select=ID&$top=1";

    var executor = new SP.RequestExecutor(appweburl);
    executor.executeAsync({
        url: requestUrl,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;
            if (results !== undefined && results.length > 0) {
                var Id = results[0].ID;
            }

            deffered.resolve(Id);

        },
        error: function (data) {
            deffered.reject(data);
            result = "inside error function";
        }
    });

    return deffered.promise;
}

function updateListItem(listTitle, listItemId, itemProperties) {

    var listItemUri = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('" + listTitle + "')/items(" + listItemId + ")?@target='" + hostweburl + "'";

    var itemToUpdate = {
        '__metadata': { 'type': getItemTypeForListName(listTitle) }
    };
    for (var prop in itemProperties) {
        itemToUpdate[prop] = itemProperties[prop];
    }

    updateJson(Q, listItemUri, itemToUpdate);
}

function updateListItemQ(Q,listTitle, listItemId, itemProperties) {

    var deffered = Q.defer();

    var listItemUri = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('" + listTitle + "')/items(" + listItemId + ")?@target='" + hostweburl + "'";

    var itemToUpdate = {
        '__metadata': { 'type': getItemTypeForListName(listTitle) }
    };
    for (var prop in itemProperties) {
        itemToUpdate[prop] = itemProperties[prop];
    }

    updateJson(Q, listItemUri, itemToUpdate).then(function (data) {
        deffered.resolve(data);
    });

    return deffered.promise;
}

function getItemTypeForListName(listName) {
    var formattedListItemName = listName.replace(/\s+/g, "");
    return "SP.Data." + formattedListItemName[0].toUpperCase() + formattedListItemName.substring(1) + "ListItem";
}

function updateJson(Q, endpointUri, itemToUpdate, isnew) {

    var deffered = Q.defer();
    var __appweburl = appweburl;

    if (isnew)
        __appweburl = $("#appweburl", opener.document).val();

    var executor = new SP.RequestExecutor(__appweburl);

    executor.executeAsync({
        url: endpointUri,
        method: "POST",  // instead of "type" from $.ajax has to use "method"!
        body: JSON.stringify(itemToUpdate),  // instead of "data" from $.ajax has to use "body"!
        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
            "content-type": "application/json;odata=verbose", // set content-type here
            "X-RequestDigest": $("#__REQUESTDIGEST").val(),
            "X-HTTP-Method": "MERGE",
            "If-Match": "*"
        },
        success: function (data) {
            deffered.resolve(data);
            //var body = JSON.parse(data.body);
            //$("#message").html("Item created.");
        },
        error: function (data) {
            deffered.reject(data);
            //$("#message").html("Failed to create item.");
        }
    });

    return deffered.promise;
}

//========================================= New Implemantations ================================
//==============================================================================================

function updateListItemFromNew(Q, listTitle, listItemId, itemProperties) {

    var deffered = Q.defer();

    var _appweburl = $("#appweburl", opener.document).val();
    var _hostweburl = $("#hostweburl", opener.document).val();

    var listItemUri = _appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('" + listTitle + "')/items(" + listItemId + ")?@target='" + _hostweburl + "'";

    var itemToUpdate = {
        '__metadata': { 'type': getItemTypeForListName(listTitle) }
    };

    for (var prop in itemProperties) {
        itemToUpdate[prop] = itemProperties[prop];
    }

    updateJson(Q, listItemUri, itemToUpdate, true).then(function (data) {
        deffered.resolve(data);
    });

    return deffered.promise;
}

function getDataByKeysFromNew(Q, projectId, accountId) {

    var _appweburl = $("#appweburl", opener.document).val();
    var _hostweburl = $("#hostweburl", opener.document).val();

    var deferred = Q.defer();
    var outPut = [];
    var results = [];
    var result;
    var ID = null;
    var requestUrl = _appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Project Account')/items?@target='" + _hostweburl + "'&$select=ID&$filter=(project_number/project_number eq '" + projectId + "' and account_myob_id/account_myob_id eq '" + accountId + "') ";

    var executor = new SP.RequestExecutor(_appweburl);
    executor.executeAsync({
        url: requestUrl,
        async: false,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;

            if (results !== undefined && results.length > 0) {
                var ID = results[0].ID;

            } else {
                ID = null;
            }

            deferred.resolve(ID);
        },
        error: function (data) {
            deferred.reject(false);
        }

    });

    return deferred.promise;

}

function getDataByKeys(Q, projectId, accountId) {
    var deferred = Q.defer();
    var outPut = [];
    var results = [];
    var result;
    var ID = null;
    var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Project Account')/items?@target='" + hostweburl + "'&$select=ID&$filter=(project_number/project_number eq '" + projectId + "' and account_myob_id/account_myob_id eq '" + accountId + "') ";

    var executor = new SP.RequestExecutor(appweburl);
    executor.executeAsync({
        url: requestUrl,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;

            if (results !== undefined && results.length > 0) {
                var ID = results[0].ID;

            } else {
                ID = null;
            }

            deferred.resolve(ID);
        },
        error: function (data) {
            deferred.reject(false);
        }

    });

    return deferred.promise;

}

//=============== GET Bussiness data ================
//===================================================

//get business data
function getBusinesData(Q) {
    var deferred = Q.defer();
    var outPut = [];
    var results = [];
    var result;
    //var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Business')/items?@target='" + hostweburl + "'&$select=business_financial_year,business_project_estimate,business_cost_budget&$top=1";
    var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Business')/items?@target='" + hostweburl + "'&$select=ID,business_financial_year,business_project_estimate,business_cost_budget&$filter=business_financial_year ge datetime'" + getCurrentFinancialYear() + "'&$top=1";

    var executor = new SP.RequestExecutor(appweburl);
    executor.executeAsync({
        url: requestUrl,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;

            deferred.resolve(results);

        },
        error: function (data) {
            deferred.reject(data);
            result = "inside error function";
        }
    });
    return deferred.promise;
}

//get milestone claim estimate
function getMilestoneClaimEstimate(Q) {
    var deferred = Q.defer();
    var outPut = [];
    var results = [];
    var result;
    //var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Milestone')/items?@target='" + hostweburl + "'&$select=milestone_claim_estimate,milestone_claim_actual";
    var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Milestone')/items?@target='" + hostweburl + "'&$select=milestone_claim_estimate,milestone_claim_actual&$filter=milestone_date_start ge datetime'" + getCurrentFinancialYear() + "'";

    var executor = new SP.RequestExecutor(appweburl);
    executor.executeAsync({
        url: requestUrl,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;

            deferred.resolve(results);

        },
        error: function (data) {
            deferred.reject(data);
            result = "inside error function";
        }
    });
    return deferred.promise;
}

function getProjectAccountEstimate(Q) {
    var deferred = Q.defer();
    var outPut = [];
    var results = [];
    var result;
    var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Project Account')/items?@target='" + hostweburl + "'&$select=project_account_estimate";

    var executor = new SP.RequestExecutor(appweburl);
    executor.executeAsync({
        url: requestUrl,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;

            deferred.resolve(results);

        },
        error: function (data) {
            deferred.reject(data);
            result = "inside error function";
        }
    });
    return deferred.promise;
}

function getProjectBuildingMargin(Q) {
    var deferred = Q.defer();
    var outPut = [];
    var results = [];
    var result;
    var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Project')/items?@target='" + hostweburl + "'&$select=project_number,project_building_margin";

    var executor = new SP.RequestExecutor(appweburl);
    executor.executeAsync({
        url: requestUrl,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;

            deferred.resolve(results);

        },
        error: function (data) {
            deferred.reject(data);
            result = "inside error function";
        }
    });
    return deferred.promise;
}

function setProjectAndRemainingClaim(Q, projectId) {
    //Get Total project claim-- Dont DELETE WITHOUT CONFIRM

    var totalProjectClaim = 0;
    var remainingClaimValue = 0;

    getAllMilestoneClaimEstimate(Q, projectId).then(function (totalMilestoneClaimEstimateValues) {

        for (var i = 0; i < totalMilestoneClaimEstimateValues.length; i++) {
            totalProjectClaim = totalProjectClaim + totalMilestoneClaimEstimateValues[i].milestone_claim_estimate;
            remainingClaimValue = remainingClaimValue + totalMilestoneClaimEstimateValues[i].milestone_claim_actual;
        }

        if (i == totalMilestoneClaimEstimateValues.length) {

            if (totalProjectClaim !== undefined)
                $("#totalprojectclaim").val("$" + $.number(parseFloat(totalProjectClaim).toFixed(2), 0));
            if (remainingClaimValue !== undefined)
                $("#remainingclaim").val("$" + $.number(parseFloat(totalProjectClaim - remainingClaimValue).toFixed(2), 0));
        }
    });

}

//get milestone claim estimate
function getAllMilestoneClaimEstimate(Q, projectNumber) {
    var deferred = Q.defer();
    var outPut = [];
    var results = [];
    var result;
    var requestUrl;

    if (projectNumber)
        requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Milestone')/items?@target='" + hostweburl + "'&$select=milestone_claim_estimate,milestone_claim_actual&$filter=project_number/project_number eq '" + projectNumber + "'";
    else
        requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Milestone')/items?@target='" + hostweburl + "'&$select=milestone_claim_estimate,milestone_claim_actual";
    //console.log(requestUrl);

    var executor = new SP.RequestExecutor(appweburl);
    executor.executeAsync({
        url: requestUrl,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;

            deferred.resolve(results);

        },
        error: function (data) {
            deferred.reject(data);
            result = "inside error function";
        }
    });
    return deferred.promise;
}

//get business level data
function getBusinessLevelData(Q) {
    var estimateProjectValue = 0;
    var actualProjectValue = 0;
    var costBudget = 0;
    var grossProfit = 0;
    var totalProjectClaim = 0;
    var remainingClaimValue = 0;

    var sumMilestoneClaimActual = 0;
    var sumProjectAccountEstimate = 0;
    var sumProjectBuildingMargin = 0;

    var assignMilestoneClaimData = 0;
    var assignProjectAccountEstimate = 0;
    // var assignProjectBuildingMargin = 0;
    CumulativeMilestoneClaims = [];
    CumulativeGrossMargin = [];

    getBusinesData(Q).then(function (businessData) {
        estimateProjectValue = businessData[0].business_project_estimate;
        costBudget = businessData[0].business_cost_budget;
        getMilestoneClaimEstimate(Q).then(function (milestoneClaimData) {
            assignMilestoneClaimData = milestoneClaimData;

            getProjectAccountEstimate(Q).then(function (projectAccountEstimate) {
                assignProjectAccountEstimate = projectAccountEstimate;

                getProjectBuildingMargin(Q).then(function (projectBuildingMargin) {

                    for (var i = 0; i < assignMilestoneClaimData.length; i++) {
                        actualProjectValue = actualProjectValue + assignMilestoneClaimData[i].milestone_claim_estimate;
                        sumMilestoneClaimActual = sumMilestoneClaimActual + assignMilestoneClaimData[i].milestone_claim_actual;
                        
                    }

                    for (var i = 0; i < assignProjectAccountEstimate.length; i++) {
                        sumProjectAccountEstimate = sumProjectAccountEstimate + assignProjectAccountEstimate[i].project_account_estimate;
                    }

                    for (var i = 0; i < projectBuildingMargin.length; i++) {
                        sumProjectBuildingMargin = sumProjectBuildingMargin + projectBuildingMargin[i].project_building_margin;
                    }

                    grossProfit = sumMilestoneClaimActual / actualProjectValue * sumProjectAccountEstimate * sumProjectBuildingMargin;
                    //remainingClaimValue = actualProjectValue - sumMilestoneClaimActual;
                    //totalProjectClaim = actualProjectValue;

                    //console.log('estimateProjectValue :' + estimateProjectValue + ', actualProjectValue :' + actualProjectValue + ', costBudget' + costBudget + ' ,grossProfit :' + grossProfit + ' ,totalProjectClaim :' + totalProjectClaim + ' ,remainingClaimValue :' + remainingClaimValue);

                    if (estimateProjectValue !== undefined)
                        $("#estimateprojectvalue").val("$" + $.number(parseFloat(estimateProjectValue).toFixed(2), 0));
                    if (actualProjectValue !== undefined)
                        $("#actualprojectvalue").val("$" + $.number(parseFloat(actualProjectValue).toFixed(2), 0));
                    if (costBudget !== undefined)
                        $("#costbudget").val("$" + $.number(parseFloat(costBudget).toFixed(2), 0));
                    if (grossProfit !== undefined)
                        $("#grossprofit").val("$" + $.number(parseFloat(grossProfit).toFixed(2), 0));
                    //if (totalProjectClaim !== undefined)
                    //$("#totalprojectclaim").val("$" + $.number(parseFloat(totalProjectClaim).toFixed(2), 0));
                    //if (remainingClaimValue !== undefined)
                    //$("#remainingclaim").val("$" + $.number(parseFloat(remainingClaimValue).toFixed(2), 0));

                    CostBudgetVsMoneyReceivedDollars = [];
                    var estValuePerMonth = parseFloat(estimateProjectValue).toFixed(2) / 12;
                    CostBudgetVsMoneyReceivedDollars.push({ x: new Date(new Date().getFullYear(), 6, 1), y: 0 });

                    for (var i = 1; i <= 12; i++) {
                        CostBudgetVsMoneyReceivedDollars.push({ x: new Date(new Date().getFullYear(), 6 + i, 1), y: parseInt(estValuePerMonth * i) });
                    }

                    getCumulativeMilestoneClaims(Q).then(function (cumulativeMilestoneClaims) {

                        for (var i = 0; i < cumulativeMilestoneClaims.length; i++) {
                            CumulativeMilestoneClaims.push({ x: new Date(new Date(cumulativeMilestoneClaims[i].milestone_claim_actual_date).getFullYear(), new Date(cumulativeMilestoneClaims[i].milestone_claim_actual_date).getMonth(), new Date(cumulativeMilestoneClaims[i].milestone_claim_actual_date).getDate()), y: parseInt(cumulativeMilestoneClaims[i].milestone_claim_actual) });

                            //projectBuildingMargin.forEach(function (buildingMargin) {

                                //if (buildingMargin.project_number == cumulativeMilestoneClaims[i].project_number) {

                            CumulativeGrossMargin.push({ x: new Date(new Date(cumulativeMilestoneClaims[i].milestone_claim_actual_date).getFullYear(), new Date(cumulativeMilestoneClaims[i].milestone_claim_actual_date).getMonth(), new Date(cumulativeMilestoneClaims[i].milestone_claim_actual_date).getDate()), y: parseInt(cumulativeMilestoneClaims[i].milestone_claim_actual * (12.5 / 100)) });

                                //}

                            //});
                            
                        }

                    });

                });
            });

        });
    });

    getAllMilestoneClaimEstimate(Q).then(function (totalMilestoneClaimEstimateValues) {

        for (var i = 0; i < totalMilestoneClaimEstimateValues.length; i++) {
            totalProjectClaim = totalProjectClaim + totalMilestoneClaimEstimateValues[i].milestone_claim_estimate;
            remainingClaimValue = remainingClaimValue + totalMilestoneClaimEstimateValues[i].milestone_claim_actual;
        }

        if (i == totalMilestoneClaimEstimateValues.length) {

            if (totalProjectClaim !== undefined)
                $("#totalprojectclaim").val("$" + $.number(parseFloat(totalProjectClaim).toFixed(2), 0));
            if (remainingClaimValue !== undefined)
                $("#remainingclaim").val("$" + $.number(parseFloat(totalProjectClaim - remainingClaimValue).toFixed(2), 0));
        }
    });

}

//get current finance year
function getCurrentFinancialYear() {

    var prefix = 'T07:00:00Z';
    // Return today's date and time
    var currentTime = new Date()

    // returns the month (from 0 to 11)
    var month = currentTime.getMonth() + 1

    // returns the day of the month (from 1 to 31)
    //var day = currentTime.getDate()

    // returns the year (four digits)
    var year = currentTime.getFullYear();

    var currentFinancialYear = 1999;

    //currentFinancialYear = year + '-07-30' + prefix;

    if (7 <= month) {
        currentFinancialYear = year + '-07-01' + prefix;
    }

    else {
        currentFinancialYear = (year - 1) + '-07-01' + prefix;
    }

    //console.log(currentFinancialYear);
    return currentFinancialYear;
}

//get average projectContract and BuildingMargin values
function getAverageProjectContractAndBuildingMargin(Q) {
    var allBuildingMargin = 0;
    var allInsurances = 0;
    var allTotalContractValue = 0;
    var averageAllBuildingMargin = 0;
    var averageAllTotalContractValue = 0;
    var averageAllInsurances = 0;
    var averageVariationAmount = 0;

    var lastBuildingMarginCount = 0

    getAllProjectDetails(Q).then(function (projecteDetails) {

        for (var j = 0; j < projecteDetails.length; j++) {

            var projectId = projecteDetails[j].projectNumber;

            getProjectAccountByProjectNumber(Q, projectId, false).then(function (projectAccouts) {


                var projectListName = 'Project'
                var projectLookupColumn = 'project_number';

                var ID = 'project_building_margin';
                getItemIdByKey(Q, projectListName, projectLookupColumn, projectAccouts[0].projectId, ID).then(function (projectBuildingMargin) {
                    lastBuildingMarginCount++;
                    var totalContractValue = 0;
                    var insurances = 0;

                    for (var i = 0; i < projectAccouts.length; i++) {

                        totalContractValue = totalContractValue + projectAccouts[i].projectAccountEstimate;

                        if (projectAccouts[i].accountMyobId == "5-1009") {
                            insurances = insurances + projectAccouts[i].projectAccountEstimate;
                        }

                    }

                    //console.log(totalContractValue);

                    allTotalContractValue = allTotalContractValue + totalContractValue;

                    allBuildingMargin = allBuildingMargin + projectBuildingMargin;

                    allInsurances = allInsurances + insurances;

                    //bind data inside of below condition
                    if (projecteDetails.length == lastBuildingMarginCount) {

                        var variationAmountValue = 0;

                        getVariationAmount(Q).then(function (variationAmount) {

                            for (var i = 0; i < variationAmount.length; i++) {
                                variationAmountValue = variationAmountValue + variationAmount[i].variation_amount;
                            }

                            averageAllBuildingMargin = allBuildingMargin / projecteDetails.length;
                            averageAllTotalContractValue = allTotalContractValue / projecteDetails.length;
                            averageAllInsurances = allInsurances / projecteDetails.length;
                            averageVariationAmount = variationAmountValue / projecteDetails.length;
                            //console.log('averageAllBuildingMargin :' + averageAllBuildingMargin, 'averageAllTotalContractValue: ' + averageAllTotalContractValue);

                            $("#totalcontractvalue").val("$" + $.number(parseFloat(averageAllTotalContractValue + averageVariationAmount).toFixed(2), 0));
                            $("#insurances").val("$" + $.number(parseFloat(averageAllInsurances).toFixed(2), 0));
                            $("#buildingmargin").val($.number(parseFloat(averageAllBuildingMargin).toFixed(2), 2) + "%");

                        });

                    }

                });

            });
        }
    });
}

//===================================================

//get project level data
function getProjectLevelData(Q, projectId) {

    $("#totalcontractvalue").val('');
    $("#supervisorallocation").val('');
    $("#supervisorcostincurred").val('');
    $("#buildingmargin").val('');
    $("#insurances").val('');

    var totalContractValue = 0;
    var insurances = 0;
    var buildingMargin = 0;
    var projectStatus;

    getProjectAccountByProjectNumber(Q, projectId, false).then(function (projectAccouts) {

        for (var i = 0; i < projectAccouts.length; i++) {
            totalContractValue = totalContractValue + projectAccouts[i].projectAccountEstimate;

            if (projectAccouts[i].accountMyobId == "5-1009") {
                insurances = insurances + projectAccouts[i].projectAccountEstimate;
            }
        }

        //console.log(totalContractValue);

        var projectListName = 'Project'
        var projectLookupColumn = 'project_number';
        var ID = 'project_building_margin';

        getItemIdByKey(Q, projectListName, projectLookupColumn, projectId, ID).then(function (projectBuildingMargin) {
            buildingMargin = projectBuildingMargin;

            var status = 'project_status';

            getItemIdByKey(Q, projectListName, projectLookupColumn, projectId, status).then(function (status) {
                projectStatus = status;

                getVariationAmount(Q, projectId).then(function (variationAmount) {

                    for (var i = 0; i < variationAmount.length; i++) {
                        totalContractValue = totalContractValue + variationAmount[i].variation_amount;
                    }

                    if (totalContractValue !== undefined)
                        $("#totalcontractvalue").val("$" + $.number(parseFloat(totalContractValue).toFixed(2), 0));
                    if (buildingMargin !== undefined)
                        $("#buildingmargin").val($.number(parseFloat(buildingMargin).toFixed(2), 2) + " %");
                    if (insurances !== undefined)
                        $("#insurances").val("$" + $.number(parseFloat(insurances).toFixed(2), 0));

                    if (projectStatus == "Open")
                        $('#status option[value=Open]').prop('selected', true);
                    else if (projectStatus == "Past")
                        $('#status option[value=Past]').prop('selected', true);
                    else
                        $('#status option[value=Open]').prop('selected', true);

                });

            });
        });

    });

}

function getVariationAmount(Q, projectNumber) {
    var outPut = [];
    var results = [];
    var result = [];
    var requestUrl;

    if (projectNumber)
        requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Variations')/items?@target='" + hostweburl + "'&$select=variation_amount&$filter=project_number/project_number eq '" + projectNumber + "' and variation_status eq 'Approved'";
    else
        requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Variations')/items?@target='" + hostweburl + "'&$select=variation_amount&$filter=variation_status eq 'Approved'";

    var deffered = Q.defer();

    var executor = new SP.RequestExecutor(appweburl);
    executor.executeAsync({
        url: requestUrl,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;

            if (results.length > 0) {
                result = results;
                //result['filterId'] = key;
            }

            deffered.resolve(result);
        },
        error: function (data) {
            result = "inside error function";
            deffered.reject = data;
        }
    });

    return deffered.promise;
}

//get Costings data
function getCostingsDataByProjectID(Q, projectId) {

    var costings = [];
    var projectAccountsdetails = [];
    var summaryAccountDetails = [];

    getProjectAccountByProjectNumber(Q, projectId, false).then(function (projectAccouts) {
        //console.log(projectAccouts);
        projectAccountsdetails = projectAccouts;
        //get project accounts details
        if (projectAccouts.length > 0) {

            //get summary account details
            getSummaryAccountDetails(Q, false).then(function (summaryAccounts) {
                //console.log(summaryAccounts);
                summaryAccountDetails = summaryAccounts;

                if (summaryAccounts.length > 0) {
                    //get Account details
                    for (var i = 0; i < summaryAccounts.length; i++) {


                        getAccountIDBySummaryAccountId(Q, summaryAccounts[i].summaryAccountMyobId, summaryAccounts[i].summaryAccountName, false).then(function (accountDetails) {
                            //console.log(accountDetails);

                            var summaryAccountName = accountDetails.summaryAccountName;
                            var summaryAccountMyobId = accountDetails.summaryAccountMyobId;
                            var projectAccountEstimate = 0;
                            var projectAccouts = 0;

                            for (var j = 0; j < projectAccountsdetails.length; j++) {
                                for (var k = 0; k < accountDetails.length; k++) {

                                    if (projectAccountsdetails[j].accountMyobId == accountDetails[k].accountMyobId) {

                                        projectAccountEstimate = projectAccountEstimate + projectAccountsdetails[j].projectAccountEstimate;
                                        projectAccouts = projectAccouts + projectAccountsdetails[j].projectAccountIncurred;
                                    }

                                }
                            }

                            costings.push({ Description: summaryAccountName, Costed: projectAccountEstimate, projectAccouts: projectAccouts, Incurred: projectAccouts, SummaryAccountMyobId: summaryAccountMyobId });

                            //bind costing data inside of if condition
                            if (summaryAccountDetails.length == costings.length) {
                                //console.log(costings);

                                costings.sort(SortByCostingsSummaryAccountMyobId);

                                $("#costings").find('tbody').empty();

                                var tbody = '';
                                var td = '';
                                var tds = [];

                                var rowCount = parseInt(costings.length / 2);
                                var rowBal = parseInt(costings.length % 2);

                                for (var i = 0; i < rowCount; i++) {

                                    td = '<td class="td-left">' + costings[i]["Description"] + '</td>' +
                                      '<td class="td-right">$' + $.number(parseFloat(costings[i]["Costed"].toString()).toFixed(2), 0) + '</td>' +
                                      '<td class="td-right">$' + $.number(parseFloat(costings[i]["Incurred"].toString()).toFixed(2), 0) + '</td>';

                                    tds.push({ td: td });
                                }

                                if (rowBal > 0) {

                                    td = '<td class="td-left">' + costings[rowCount + 1]["Description"] + '</td>' +
                                     '<td class="td-right">$' + $.number(parseFloat(costings[rowCount + 1]["Costed"].toString()).toFixed(2), 0) + '</td>' +
                                     '<td class="td-right">$' + $.number(parseFloat(costings[rowCount + 1]["Incurred"].toString()).toFixed(2), 0) + '</td>';

                                    tds.push({ td: td });
                                }

                                var c = 0;

                                for (var i = rowCount + rowBal; i < costings.length; i++) {

                                    tds[c].td += '<td class="td-left">' + costings[i]["Description"] + '</td>' +
                                     '<td class="td-right">$' + $.number(parseFloat(costings[i]["Costed"].toString()).toFixed(2), 0) + '</td>' +
                                     '<td class="td-right">$' + $.number(parseFloat(costings[i]["Incurred"].toString()).toFixed(2), 0) + '</td>';

                                    c++;
                                }

                                /*for (var i = 1; i <= rowCount; i++) {

                                    var td = '<td class="td-left">' + costings[(2 * i) - 2]["Description"] + '</td>' +
                                         '<td class="td-right">$' + $.number(parseFloat(costings[(2 * i) - 2]["Costed"].toString()).toFixed(2), 0) + '</td>' +
                                         '<td class="td-right">$' + $.number(parseFloat(costings[(2 * i) - 2]["Incurred"].toString()).toFixed(2), 0) + '</td>';

                                    td += '<td class="td-left">' + costings[(2 * i) - 1]["Description"] + '</td>' +
                                       '<td class="td-right">$' + $.number(parseFloat(costings[(2 * i) - 1]["Costed"].toString()).toFixed(2), 0) + '</td>' +
                                       '<td class="td-right">$' + $.number(parseFloat(costings[(2 * i) - 1]["Incurred"].toString()).toFixed(2), 0) + '</td>';

                                    tds.push({ td: td });

                                    if (rowBal > 0 && i == rowCount) {

                                        var td = '<td class="td-left">' + costings[(2 * (i + 1)) - 2]["Description"] + '</td>' +
                                        '<td class="td-right">$' + $.number(parseFloat(costings[(2 * (i + 1)) - 2]["Costed"].toString()).toFixed(2), 0) + '</td>' +
                                        '<td class="td-right">$' + $.number(parseFloat(costings[(2 * (i + 1)) - 2]["Incurred"].toString()).toFixed(2), 0) + '</td>';

                                        td += '<td class="td-left"></td>' +
                                           '<td class="td-right"></td>' +
                                           '<td class="td-right"></td>';

                                        tds.push({ td: td });
                                    }

                                }*/

                                tds.forEach(function (td) {
                                    tbody += '<tr>' + td['td'] + '</tr>';

                                });

                                $("#costings").find('tbody').append(tbody);
                            }

                        });
                    }
                }

                else {
                    //console.log('Summary Account list is empty');
                }
            });
        }

        else {
            //console.log(projectId + ' does not exist');
        }
    });


}

//get milestone data --  
function getMilestoneData(Q, projectNumber) {
    var deferred = Q.defer();
    var outPut = [];
    var results = [];
    var result;
    //var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Milestone')/items?@target='" + hostweburl + "'&$select=milestone_name,milestone_date_end,milestone_claim_estimate&$filter=project_number/project_number eq '" + projectNumber + "'";
    var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Milestone')/items?@target='" + hostweburl + "'&$select=ID,milestone_name,milestone_date_start,milestone_date_end,milestone_claim_actual_date,milestone_claim_estimate&$filter=project_number/project_number eq '" + projectNumber + "'";

    var executor = new SP.RequestExecutor(appweburl);
    executor.executeAsync({
        url: requestUrl,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;

            if (results !== undefined && results.length > 0) {
                //for (var i = 0; i < results.length; i++) {
                //    outPut.push({ milestoneName: results[i].milestone_name, milestoneDate: results[i].milestone_date_end, milestoneClaimEstimate: results[i].milestone_claim_estimate });
                //}
            }

            deferred.resolve(results);
        },

        error: function (data) {
            deferred.reject(data);
            result = "inside error function";
        }
    });
    return deferred.promise;
}

//get milestone calender data
function milestoneCalenderData(Q, projectNumber) {

    var listName = 'Project';
    var columnName = 'project_number';
    var key = projectNumber;
    var ID = 'project_building_margin';
    var outPut = [];
    var Milestones = [];

    //$("#milestone").isLoading({ text: "Loading", position: "overlay" });
    $("#milestone").find('tbody').empty();

    getItemIdByKey(Q, listName, columnName, key, ID).then(function (buildingMargin) {

        getSumOfProjectAccountEstimate(Q, projectNumber, false).then(function (sumOfProjectAccountEstimate) {

            getMilestoneData(Q, projectNumber).then(function (milestone) {
                for (var i = 0; i < milestone.length; i++) {
                    var milestoneName = milestone[i].milestone_name;
                    var dateForClaim = milestone[i].milestone_date_end;
                    var claimActualDate = milestone[i].milestone_claim_actual_date;
                    var progressclaimstage = milestone[i].milestone_claim_estimate;

                    if (buildingMargin == null)
                        buildingMargin = 0;

                    var costAllocated = milestone[i].milestone_claim_estimate / (1 + buildingMargin / 100);
                    var costIncurred = 0; //get from MYOB
                    var GSTinclusive = ((milestone[i].milestone_claim_estimate / sumOfProjectAccountEstimate) * 100).toFixed(2); //not provides logic

                    //console.log(milestoneName, dateForClaim, progressclaimstage, costAllocated, costIncurred, GSTinclusive);
                    outPut.push({ Title: milestoneName, DateForClaim: dateForClaim, ProgressClaimStage: progressclaimstage, GST: GSTinclusive, CostAllocated: costAllocated, CostIncurred: costIncurred, claimActualDate: claimActualDate });
                    Milestones.push(milestoneName);
                }

                //================ Bind MS UI =====================
                //=================================================
               
                var tbody = '';

                ProjectScheduleMonitoringEstimates = [];
                ProjectScheduleMonitoringActuals = [];

                if (outPut !== undefined && outPut.length > 0) {

                    getMilestoneCostIncurred(Q, projectNumber, Milestones).then(function (data) {

                        if (data != undefined && data.length > 0)
                            data.forEach(function (milestoneIncurred) {

                                outPut.forEach(function (MI) {
                                    if (MI['Title'] == milestoneIncurred['Mileston']) {
                                        MI['CostIncurred'] = milestoneIncurred['Amount'];
                                    }
                                });

                            });

                        outPut.sort(SortByDateForClaim);

                        tbody += '<tr>'

                        outPut.forEach(function (milestone) {

                            var _dateForClaim = milestone['DateForClaim'].toString().split('-')[2].substring(0, 2) + "/" + milestone['DateForClaim'].toString().split('-')[1] + "/" + milestone['DateForClaim'].toString().split('-')[0];

                            tbody += '<td class="wrapper-table-cell">' +
                                         '<table class="table table-bordered table-striped table-condensed table-responsive">' +
                                            '<thead>' +
                                                '<tr>' +
                                                    '<th class="table-head-th">Milestone</th>' +
                                                    '<th class="table-head-th">' + milestone['Title'] + '</th>' +
                                                '</tr>' +
                                            '</thead>' +
                                            '<tbody>' +
                                                '<tr>' +
                                                    '<td>Date for claim</td>' +
                                                    '<td>' + _dateForClaim + '</td>' +
                                                '</tr>' +
                                                '<tr>' +
                                                    '<td>Progress claim stage</td>' +
                                                    '<td class="td-right">$' + $.number(parseFloat(milestone['ProgressClaimStage'].toString()).toFixed(2), 0) + '</td>' +
                                                '</tr>' +
                                                '<tr>' +
                                                    '<td>GST incl.</td>' +
                                                    '<td class="td-right">' + parseFloat(milestone['GST'].toString()).toFixed(2) + ' %</td>' +
                                                '</tr>' +
                                                '<tr>' +
                                                    '<td>Cost allocated</td>' +
                                                    '<td class="td-right">$' + $.number(parseFloat(milestone['CostAllocated'].toString()).toFixed(2), 0) + '</td>' +
                                                '</tr>' +
                                                '<tr>' +
                                                    '<td>Cost incurred</td>' +
                                                    '<td class="td-right">$' + $.number(parseFloat(milestone['CostIncurred'].toString()).toFixed(2), 0) + '</td>' +
                                                '</tr>' +
                                            '</tbody>' +
                                        '</table>' +
                                        '</td>';
                        });

                        tbody += '</tr>';

                        $("#milestone").find('tbody').empty();
                        $("#milestone").find('tbody').append(tbody);
                        
                    });

                } else {

                    tbody += '<tr><td class="wrapper-table-cell">' +
                                 '<table class="table table-bordered table-striped table-condensed table-responsive">' +
                                        '<thead>' +
                                            '<tr>' +
                                                '<th class="table-head-th">No data to display</th>' +
                                            '</tr>' +
                                        '</thead>' +
                                '</table>' +
                             '</td></tr>';

                    $("#milestone").find('tbody').empty();
                    $("#milestone").find('tbody').append(tbody);
                    //$.isLoading("hide");
                }

                //================================================

            });

        });

    });
}

//insert project account data , when project create in first time
function insertProjectAccountData(Q, projectId, projectValueData) {//accountId

    var deffered = Q.defer();

    //console.log('insertProjectAccountData');

    var projectListName = 'Project'
    var projectLookupColumn = 'project_number';

    var accountListName = 'Account';
    var accoutLookupColumn = 'account_myob_id';

    var listName = 'Project Account';
    var ID = 'ID';

    //get project id lookup value -- here change by Eranga for new page. -->getItemIdByKey
    getItemIdByKeyForNew(Q, projectListName, projectLookupColumn, projectId, ID).then(function (lookupProjectId) {

        //if project id exist
        //console.log("lookupProjectId" + lookupProjectId);

        if (lookupProjectId != null) {
            //projectValueData['project_numberId'] = lookupProjectId;

            deffered.resolve(projectValueData.forEach(function (pro) {

                //get Account ID lookup value
                getItemIdByKeyForNew(Q, accountListName, accoutLookupColumn, pro['account_id'], ID).then(function (lookupAccountId) {
                    //if account id exist
                    //console.log("lookupAccountId" + lookupAccountId);

                    if (lookupAccountId != null) {
                        //pro['account_myob_id'] = lookupAccountId;

                        var obj = {
                            project_account_estimate: pro['project_account_estimate'],
                            project_account_incurred: pro['project_account_incurred'],
                            project_numberId: lookupProjectId,
                            account_myob_idId: lookupAccountId
                        };

                        //console.log(proval);
                        //createItem(Q, projectValueData, listName);
                        createItemFromNew(Q, obj, listName);
                    }
                    else {
                        //console.log('Account ID' + accountId + 'does not exist');
                    }
                });

            }));

        }

        else {
            //console.log('Project ID' + projectId + 'does not exist');
        }

    });

    return deffered.promise;

}

//update Project account data when project exist
function updateProjectAccountData(Q, projectId, projectValueData) {

    var deffered = Q.defer();

    var projectListName = 'Project'
    var projectLookupColumn = 'project_number';

    var accountListName = 'Account';
    var accoutLookupColumn = 'account_myob_id';

    var listName = 'Project Account';
    var ID = 'ID';
    var i = 0;

    var notExistItems = [];

    projectValueData.forEach(function (pro) {

        getDataByKeysFromNew(Q, projectId, pro['account_id']).then(function (ItemId) {

            if (ItemId) {

                var obj = {

                    project_account_estimate: pro['project_account_estimate'],
                    project_account_incurred: pro['project_account_incurred']
                };

                updateListItemFromNew(Q, listName, ItemId, obj);

                i++;
                if (projectValueData.length == i) {
                    if (notExistItems.length > 0) {

                        insertProjectAccountData(Q, projectId, notExistItems).then(function () {
                            deffered.resolve('ok');

                        });
                    }
                    else
                        deffered.resolve('ok');
                }

            } else {

                i++;
                notExistItems.push(pro);

                if (projectValueData.length == i) {
                    if (notExistItems.length > 0) {

                        insertProjectAccountData(Q, projectId, notExistItems).then(function () {
                            deffered.resolve('ok');

                        });
                    }
                    else
                        deffered.resolve('ok');
                }

                /*
                //get project id lookup value --  here change by Eranga for new page. -->getItemIdByKey
                getItemIdByKeyForNew(Q, projectListName, projectLookupColumn, projectId, ID).then(function (lookupProjectId) {

                    //if project id exist
                    //console.log(lookupProjectId);
                    if (lookupProjectId != undefined && lookupProjectId != null) {
                        //pro['project_numberId'] = lookupProjectId;

                        //get Account ID lookup value
                        getItemIdByKeyForNew(Q, accountListName, accoutLookupColumn, pro['account_id'], ID).then(function (lookupAccountId) {
                            //if account id exist
                            //console.log(lookupAccountId);
                            if (lookupAccountId != undefined && lookupAccountId != null) {
                                //pro['account_myob_idId'] = lookupAccountId;

                                var obj = {
                                    project_account_estimate: pro['project_account_estimate'],
                                    project_account_incurred: pro['project_account_incurred'],
                                    project_numberId: lookupProjectId,
                                    account_myob_idId: lookupAccountId
                                };

                                createItemFromNew(Q, obj, listName);

                                i++;
                                if (projectValueData.length == i)
                                    deffered.resolve('ok');
                            }
                            else {
                                //console.log('Account ID ' + accountId + ' does not exist');
                            }
                        });
                    }

                    else {
                        //console.log('Project ID ' + projectId + ' does not exist');
                    }

                });
                */
            }
        });

    });

    return deffered.promise;
}

//insert milestone data
function insertMilestoneData(Q, projectId, milestoneData, milestoneAccountData) {

    var deffered = Q.defer();

    var projectListName = 'Project'
    var projectLookupColumn = 'project_number';

    var milestoneListName = 'Milestone';
    var ID = 'ID';

    var i = 0;

    getItemIdByKeyForNew(Q, projectListName, projectLookupColumn, projectId, ID).then(function (projectLookupId) {

        //if project id exist
        //console.log(projectLookupId);
        if (projectLookupId != null) {
            //get project lookup id

            milestoneData.forEach(function (mi) {

                mi['project_numberId'] = projectLookupId;

                createItemFromNew(Q, mi, milestoneListName).then(function () {

                    var miAccountData = [];

                    milestoneAccountData.forEach(function (miAcc) {
                        if (mi.milestone_name == miAcc.milestone_name) {
                            miAccountData.push(miAcc);
                        }
                    });
                    
                    i++;
                    insertMilestoneAccountData(Q, projectId, miAccountData);

                    if (i == milestoneData.length) {
                        deffered.resolve('ok');
                    }

                });

            });
        }

        else {
            //console.log('Project ID' + projectId + 'does not exist');
        }


    });

    return deffered.promise;
}

//insert milestone account data
function insertMilestoneAccountData(Q, projectNumber, milestoneAccountData) {

    var deffered = Q.defer();

    var milestoneListName = 'Milestone'
    var milestoneLookupColumn = 'milestone_name';

    var accountListName = 'Account';
    var accoutLookupColumn = 'account_myob_id';

    var listName = 'Milestone Account';
    var ID = 'ID';
    var i = 0;

    milestoneAccountData.forEach(function (miacc) {

        //get milestone id lookup value
        getItemIdByProjectNameAndMilestoneNameFromNew(Q, projectNumber, miacc['milestone_name']).then(function (lookupMilestoneId) {

            //if milestone id exist
            //console.log(lookupMilestoneId);
            if (lookupMilestoneId != null) {
                miacc['milestone_idId'] = lookupMilestoneId;

                //get Account ID lookup value
                getItemIdByKeyForNew(Q, accountListName, accoutLookupColumn, miacc['account_id'], ID).then(function (lookupAccountId) {
                    //if account id exist
                    //console.log(lookupAccountId);

                    if (lookupAccountId != null) {

                        //milestoneAccountData.forEach(function (miacc) {

                        miacc['account_myob_idId'] = lookupAccountId;

                        var obj = {
                            milestone_idId: miacc['milestone_idId'],
                            milestone_account_estimate: miacc['milestone_account_estimate'],
                            account_myob_idId: lookupAccountId
                        };

                        createItemFromNew(Q, obj, listName).then(function () {
                            i++;

                            if (i == milestoneAccountData.length)
                                deffered.resolve('ok');
                        });

                        //});
                    }
                    else {
                        //console.log('Account ID ' + accountId + ' does not exist');
                    }
                });
            }

            else {
                //console.log('Milestone ID ' + milestoneName + ' does not exist');
            }

        });

    });

    return deffered.promise;
}

//get milestone item id by project number and milestone name
function getItemIdByProjectNameAndMilestoneNameFromNew(Q, projectNumber, milestoneName) {

    var _appweburl = $("#appweburl", opener.document).val();
    var _hostweburl = $("#hostweburl", opener.document).val();

    var deferred = Q.defer();
    var outPut = [];
    var results = [];
    var result;
    var ID = null;
    var requestUrl = _appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Milestone')/items?@target='" + _hostweburl + "'&$select=ID&$filter=(project_number/project_number eq '" + projectNumber + "' and milestone_name eq '" + milestoneName + "') ";

    var executor = new SP.RequestExecutor(_appweburl);
    executor.executeAsync({
        url: requestUrl,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;

            if (results !== undefined && results.length > 0) {
                var ID = results[0].ID;

            } else {
                ID = null;
            }

            deferred.resolve(ID);
        },
        error: function (data) {
            deferred.reject(false);
        }

    });

    return deferred.promise;
}

//update milestone data
function updateMilestoneData(Q, projectNumber, milestoneData, milestoneAccountData) {

    var deffered = Q.defer();

    var milestoneListName = 'Milestone'
    var milestoneLookupColumn = 'milestone_name';

    var listName = 'Milestone';
    var ID = 'ID';
    var i = 0;

    var notExistItems = [];

    milestoneData.forEach(function (mi) {

        getItemIdByProjectNameAndMilestoneNameFromNew(Q, projectNumber, mi['milestone_name']).then(function (ItemId) {

            if (ItemId) {

                updateListItemFromNew(Q, listName, ItemId, mi);

                var miAccountData = [];

                milestoneAccountData.forEach(function (miAcc) {
                    if (mi.milestone_name == miAcc.milestone_name) {
                        miAccountData.push(miAcc);
                    }
                });

                setTimeout(function () {

                    updateMilestoneAccountData(Q, projectNumber, miAccountData);

                    i++;
                    if (i == milestoneData.length) {
                        deffered.resolve('ok');
                    }

                    //console.log(i);

                }, 3000);//-- wait 3 sec

            }
            else {

                var miData = [];
                miData.push(mi);

                insertMilestoneData(Q, projectNumber, miData);

                var miAccountData = [];

                milestoneAccountData.forEach(function (miAcc) {
                    if (mi.milestone_name == miAcc.milestone_name) {
                        miAccountData.push(miAcc);
                    }
                });

                setTimeout(function () {

                    updateMilestoneAccountData(Q, projectNumber, miAccountData);
                    i++;

                    if (i == milestoneData.length) {
                        deffered.resolve('ok');
                    }

                }, 3000);//-- wait 3 sec
            }
        });

    });

    return deffered.promise;
}

function updateMilestoneAccountData(Q, projectNumber, milestoneAccountData) {

    var deffered = Q.defer();

    var listName = 'Milestone Account';
    var i = 0;

    milestoneAccountData.forEach(function (acc) {

        //get milestone id by project name and milestone name
        getItemIdByProjectNameAndMilestoneNameFromNew(Q, projectNumber, acc['milestone_name']).then(function (milestoneId) {
            //if milestone id exist
            if (milestoneId) {
                //get Account ID lookup value
                getMilestoneAccountIdByMilestoneAndaccountMyobIdFromNew(Q, milestoneId, acc['account_id']).then(function (milestoneAccountId) {
                    //if account id exist
                    //console.log(milestoneAccountId);
                    if (milestoneAccountId != null) {

                        var obj = { milestone_account_estimate: acc['milestone_account_estimate'] };

                        updateListItemFromNew(Q, listName, milestoneAccountId, obj).then(function () {
                            i++;

                            if (i == milestoneAccountData.length) {
                                deffered.resolve('ok');
                            }
                        });
                    }
                    else {

                        var accData = [];
                        accData.push(acc);

                        insertMilestoneAccountData(Q, projectNumber, accData).then(function () {
                            i++;

                            if (i == milestoneAccountData.length) {
                                deffered.resolve('ok');
                            }
                        });
                    }
                });

            }

            else {
                //console.log('milestone id does not exist, related to projectnumber ' + projectNumber + 'and milestone name' + milestoneName);
            }

        });

    });

    return deffered.promise;
}

//get milestone item id by project number and milestone name
function getItemIdByProjectNameAndMilestoneNameFromNew(Q, projectNumber, milestoneName) {

    var _appweburl = $("#appweburl", opener.document).val();
    var _hostweburl = $("#hostweburl", opener.document).val();

    var deferred = Q.defer();
    var outPut = [];
    var results = [];
    var result;
    var ID = null;
    var requestUrl = _appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Milestone')/items?@target='" + _hostweburl + "'&$select=ID&$filter=(project_number/project_number eq '" + projectNumber + "' and milestone_name eq '" + milestoneName + "') ";

    var executor = new SP.RequestExecutor(_appweburl);
    executor.executeAsync({
        url: requestUrl,
        async: false,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;

            if (results !== undefined && results.length > 0) {
                var ID = results[0].ID;

            } else {
                ID = null;
            }

            deferred.resolve(ID);
        },
        error: function (data) {
            deferred.reject(false);
        }

    });

    return deferred.promise;
}

//get milestoneAccountID id by milestone id and accountMyobId
function getMilestoneAccountIdByMilestoneAndaccountMyobIdFromNew(Q, milestoneId, accountMyobId) {

    var _appweburl = $("#appweburl", opener.document).val();
    var _hostweburl = $("#hostweburl", opener.document).val();

    var deferred = Q.defer();
    var outPut = [];
    var results = [];
    var result;
    var ID = null;
    var requestUrl = _appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Milestone Account')/items?@target='" + _hostweburl + "'&$select=ID&$filter=(milestone_id/ID eq '" + milestoneId + "' and account_myob_id/account_myob_id eq '" + accountMyobId + "') ";

    var executor = new SP.RequestExecutor(_appweburl);
    executor.executeAsync({
        url: requestUrl,
        async: false,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;

            if (results !== undefined && results.length > 0) {
                var ID = results[0].ID;

            } else {
                ID = null;
            }

            deferred.resolve(ID);
        },
        error: function (data) {
            deferred.reject(false);
        }

    });

    return deferred.promise;

}

//==============================================================================================
//=====================Get Documents============================================================
//==============================================================================================

function GetDocumentsForGroup(groupName) {

    console.log("Getting documents for group: " + groupName);

    var requestHeaders = {
        "Accept": "application/json;odata=nometadata",
        "X-RequestDigest": jQuery("#__REQUESTDIGEST").val()
    };

    $.ajax({
        url: _spPageContextInfo.webAbsoluteUrl + "/_api/search/query?querytext='SiteTitle:\"" + groupName + "\" AND ContentTypeId:0x0101007DB6FD427B6228409ED888DF766B27C9'&selectproperties='Title,Path,SiteTitle'",
        headers: requestHeaders,
        success: function (data) {

            var groupDocuments = data.PrimaryQueryResult.RelevantResults.Table.Rows;

            //Iterate all the documents
            for (var i = 0; i < groupDocuments.length; i++) {

                var documentProps = groupDocuments[i].Cells;

                //Iterate all the properties
                for (var j = 0; j < documentProps.length; j++) {

                    if (documentProps[j].Key == "Title") {
                        console.log(documentProps[j].Value);
                    }

                }

            }
        },
        error: function (jqxr, errorCode, errorThrown) {
            console.log(jqxr.responseText);
        }
    });
}

//get summary accounts details
function GetDocumentsForFolder(Q, isnew) {

    var deffered = Q.defer();
    var summaryAccounts = [];
    var outPut = [];
    var results = [];
    var result;

    var __appweburl = appweburl;
    var __hostweburl = hostweburl;

    if (isnew) {
        __appweburl = $("#appweburl", opener.document).val();
        __hostweburl = $("#hostweburl", opener.document).val();
    }

    console.log(__appweburl);
    console.log(__hostweburl);

    var requestUrl = "https://spmysolution-c3c6b35d9e1312.sharepoint.com/_api/web/GetFolderByServerRelativeUrl('/Commercial')/Files";

    var executor = new SP.RequestExecutor(__appweburl);
    executor.executeAsync({
        url: requestUrl,
        method: "GET",
        //async: false,
        //cache: false,
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;

            console.log(JSON.stringify(results));

            deffered.resolve(data);

        },
        error: function (data) {
            deffered.reject(data);
            result = "inside error function";
        }
    });

    return deffered.promise;
}

//==============================================================================================
//========================= DELETE FUNCTIONS ===================================================

//delete list item
function deleteListItem(Q, listTitle, listItemId, isNew) {

    var __appweburl = appweburl;
    var __hostweburl = hostweburl;

    if (isNew) {
        __appweburl = $("#appweburl", opener.document).val();
        __hostweburl = $("#hostweburl", opener.document).val();
    }

    var listItemUri = __appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('" + listTitle + "')/items(" + listItemId + ")?@target='" + __hostweburl + "'";

    var deffered = Q.defer();

    var executor = new SP.RequestExecutor(__appweburl);

    executor.executeAsync({
        url: listItemUri,
        method: "POST",  // instead of "type" from $.ajax has to use "method"!
        async: true,
        headers: {
            "accept": "application/json;odata=verbose",
            "content-type": "application/json;odata=verbose", // set content-type here
            "X-RequestDigest": $("#__REQUESTDIGEST").val(),
            "X-HTTP-Method": "DELETE",
            "If-Match": "*"
        },
        success: function (data) {
            deffered.resolve(data);
        },
        error: function (data) {
            deffered.reject(data);
        }
    });

    return deffered.promise;
}

//delete ProjectAccount data by Project Number
function deleteProjectAccountData(Q, projectNumber, isNew) {

    var deffered = Q.defer();

    var projectAccountName = 'Project Account';
    var lookupColumn = 'project_number/project_number';
    var ID = 'ID';
    var delCount = 0;

    var itemDeleteListTitle = '';
    //get project project account data forspecific project
    getItemIdsByKey(Q, projectAccountName, lookupColumn, projectNumber, ID, isNew).then(function (lookupProjectId) {
        //console.log(lookupProjectId.length);
        for (var i = 0; i < lookupProjectId.length; i++) {

            deleteListItem(Q, projectAccountName, lookupProjectId[i].ID, isNew).then(function () {
                delCount++;

                if (delCount == lookupProjectId.length)
                    deffered.resolve('ok');

            });
        }
    });

    return deffered.promise;
}

//delete milestones data according to the project Number
function deleteMilestoneData(Q, projectNumber, isNew) {

    var deffered = Q.defer();

    var milestonetName = 'Milestone';
    var lookupColumn = 'project_number/project_number';
    var ID = 'ID';

    var milestoneAccountName = 'Milestone Account';
    var milestoneAccountlookup = "milestone_id/ID"

    var miDelCount = 0;

    getItemIdsByKey(Q, milestonetName, lookupColumn, projectNumber, ID, isNew).then(function (lookupMilestoneIds) {
        //console.log(lookupMilestoneIds);
        for (var i = 0; i < lookupMilestoneIds.length; i++) {


            getItemIdsByKey(Q, milestoneAccountName, milestoneAccountlookup, lookupMilestoneIds[i].ID, ID, isNew).then(function (lookupMilestoneAccountIds) {
                var milestoneId = lookupMilestoneAccountIds.filterId;
                var accDelCount = 0;

                for (var j = 0; j < lookupMilestoneAccountIds.length; j++) {
                    //console.log(lookupMilestoneAccountIds[j].ID + ': milestone Account deteted')
                    deleteListItem(Q, milestoneAccountName, lookupMilestoneAccountIds[j].ID, isNew).then(function () {
                        accDelCount++;

                        if (miDelCount == lookupMilestoneIds.length && accDelCount == lookupMilestoneAccountIds.length)
                            deffered.resolve('ok');
                    });
                }

            });

            deleteListItem(Q, milestonetName, lookupMilestoneIds[i].ID, isNew).then(function () {
                miDelCount++;

            });

        }
    });

    return deffered.promise;
}

//Get items by key
function getItemIdsByKey(Q, listName, columnName, key, ID, isNew) {

    var __appweburl = appweburl;
    var __hostweburl = hostweburl;

    if (isNew) {
        __appweburl = $("#appweburl", opener.document).val();
        __hostweburl = $("#hostweburl", opener.document).val();
    }

    var outPut = [];
    var results = [];
    var result;
    var requestUrl = __appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('" + listName + "')/items?@target='" + __hostweburl + "'&$select=" + ID + "&$filter=" + columnName + " eq '" + key + "' ";

    var deffered = Q.defer();

    var executor = new SP.RequestExecutor(__appweburl);
    executor.executeAsync({
        url: requestUrl,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;

            if (results.length > 0) {
                result = results;
            }
            else
                result = null;

            deffered.resolve(result);
        },
        error: function (data) {
            result = "inside error function";
            deffered.reject = data;
        }
    });

    return deffered.promise;
}

//==============================================================================================
//======================== VARIATIONS ==========================================================

//insert variation data
function insertVariationsData(Q, projectId, variationData) {

    var projectListName = 'Project'
    var projectLookupColumn = 'project_number';

    var variationListName = 'Variations';
    var ID = 'ID';

    var deffered = Q.defer();

    getItemIdByKey(Q, projectListName, projectLookupColumn, projectId, ID).then(function (projectLookupId) {

        if (projectLookupId != null) {
            //get project lookup id
            variationData['project_numberId'] = projectLookupId;

            createItemQ(Q, variationData, variationListName).then(function (Id) {
                deffered.resolve(Id);
            });
        }
        else {
            deffered.resolve('Error');
        }
    });

    return deffered.promise;
}

//update variation data
function updateVariationData(Q, projectNumber, listItemId, itemProperties) {
    var listTitle = 'Variations';
    var filePath;
    var correctFilePath;

    getProjectFolderName(Q, projectNumber).then(function (projectFolderName) {
        var splitFileInput = jQuery('#getFile').val().split('akepath');
        var fileName = splitFileInput[splitFileInput.length - 1];

        filePath = hostweburl + '/Jobs/' + projectFolderName + '/Milestone Claims' + fileName;
        correctFilePath = filePath.replace(/\s/g, "%20");

        //addfile path to array
        itemProperties['Title'] = correctFilePath;

        updateListItem(listTitle, listItemId, itemProperties);

    });

}

function getAllVariationData(Q, projectNumber) {
    var outPut = [];
    var results = [];
    var result = [];
    var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Variations')/items?@target='" + hostweburl + "'&$select=ID,variation_number,Title,variation_status,variation_amount,variation_adjusted_contract,variation_description,variation_issue_date&$filter=project_number/project_number eq '" + projectNumber + "'&$orderby=variation_number desc ";

    var deffered = Q.defer();

    var executor = new SP.RequestExecutor(appweburl);
    executor.executeAsync({
        url: requestUrl,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;

            if (results.length > 0) {
                result = results;
                //result['filterId'] = key;
            }
            else
                result = null;

            deffered.resolve(result);
        },
        error: function (data) {
            result = "inside error function";
            deffered.reject = data;
        }
    });

    return deffered.promise;
}

//==============================================================================================
//===================== CREATE FOLDERS AND LIBRARIES ===========================================

//Below method related to create folder structure when project create 
function createFolddersInLibrary(Q, libraryPath, folderName, isNew) {

    var __appweburl = appweburl;
    var __hostweburl = hostweburl;

    if (isNew) {
        __appweburl = $("#appweburl", opener.document).val();
        __hostweburl = $("#hostweburl", opener.document).val();
    }

    var deferred = Q.defer();

    var url = __appweburl + "/_api/SP.AppContextSite(@target)/web/getfolderbyserverrelativeurl('" + libraryPath + "')/folders?@target= '" + __hostweburl + "' ";

    var metadata = "{ '__metadata': { 'type': 'SP.Folder' }, 'ServerRelativeUrl': '" + folderName + "'}"

    var executor = new SP.RequestExecutor(__appweburl);

    executor.executeAsync({
        url: url,
        method: "POST",
        body: metadata,
        headers: {
            "accept": "application/json; odata=verbose",
            "content-type": "application/json; odata=verbose"
        },
        success: function (data) {
            var message = JSON.parse(data.body);
            deferred.resolve(message.Name);
            //console.log(message);
        },
        error: function (data) {
            var message = JSON.parse(data.body);
            //console.log(message);
            deferred.reject(message);
        }
    });

    return deferred.promise;
}

function createFolders(projectType, projectName, projectNumber, subFolders, isNew) {

    var rootLibraryPath = "Jobs";
    var projectTypePrefix = "";

    if (projectType == "Residential")
        projectTypePrefix = "4.1";
    else if (projectType == "Commercial")
        projectTypePrefix = "4.2";

    var libraryPath = rootLibraryPath + '/' + projectTypePrefix + ' ' + projectType;

    //var projectName = 'Terry Hardie';
    //var projectNumber = '1900';
    var subFolder0 = projectNumber + '-' + projectName;

    var projectPath = libraryPath + '/' + subFolder0;
    //var subFolder1 = 'Milestone Claims';
    //var subFolder2 = 'Variations'

    //create project folder
    createFolddersInLibrary(Q, libraryPath, subFolder0, isNew).then(function (message) {

        for (var i = 0; i < subFolders.length; i++) {

            createFolddersInLibrary(Q, projectPath, subFolders[i], isNew).then(function (message) {

            });
        }
    });
}

//==============================================================================================
//========================== EXPORT EXEL =======================================================

//inport1 data
function getImport1Data(Q, projectNumber) {

    var projectListName = 'Project';
    var filterColumnName = 'project_number';
    var key = projectNumber;
    var selectColumns = 'Title,project_address,project_principle_name,project_principle_address,project_principle_email,project_principle_business_name,project_principle_abn,project_principle_ect,project_rep_name,project_rep_address,project_rep_email,project_rep_business_name,project_rep_abn,project_rep_ect,project_building_margin,project_contingency,project_status,project_type';

    var deffered = Q.defer();

    //project data
    getItemIdsByKey(Q, projectListName, filterColumnName, key, selectColumns, false).then(function (projectData) {

        if (projectData.length > 0) {
            //console.log(projectData);

            getProjectAccountByProjectNumber(Q, projectNumber, false).then(function (projectAccountData) {
                // project_account_estimate, account_myob_id
                if (projectAccountData.length > 0) {

                    for (var i = 0; i < projectAccountData.length; i++) {

                        //console.log(projectAccountData[i].accountMyobId + ' , ' + projectAccountData[i].projectAccountEstimate);
                        projectData.push({ "AccountMyobId": projectAccountData[i].accountMyobId, "ProjectAccountEstimate": projectAccountData[i].projectAccountEstimate, "ProjectAccountIncurred": projectAccountData[i].projectAccountIncurred });
                    }

                    deffered.resolve(projectData);

                } else
                    deffered.resolve(projectData);
            });
        }
        else
            deffered.resolve(projectData);
    });

    return deffered.promise;
}

//import 2 data
function getImport2Data(Q, projectNumber) {
    var results = [];
    var newMilestoneData = [];
    var milestoneCount = 0;

    var deffered = Q.defer();

    getMilestoneData(Q, projectNumber).then(function (milestoneData) {

        if (milestoneData.length > 0) {

            for (var i = 0; i < milestoneData.length; i++) {

                var milestoneId = milestoneData[i].ID;

                //project data
                getMilestoneAccountByMilestoneId(Q, milestoneId, milestoneData[i]).then(function (projectData) {

                    milestoneCount++;

                    if (projectData.length > 0) {
                        //console.log(projectData);
                        for (var j = 0; j < projectData.length; j++) {
                            //import 2 out put
                            results.push({ Milestone: projectData.milestoneData.milestone_name, StartDate: projectData.milestoneData.milestone_date_start, EndDate: projectData.milestoneData.milestone_date_end, Account: projectData[j].account_myob_id.account_myob_id, Cost: projectData[j].milestone_account_estimate });
                        }
                    }

                    if (milestoneData.length == milestoneCount)
                        deffered.resolve(results);
                });

            }
        }
        else
            deffered.resolve(results);

    });

    return deffered.promise;
}

function getMilestoneAccountByMilestoneId(Q, milestoneId, milestoneData) {


    var deffered = Q.defer();
    var outPut = [];
    var results = [];
    var result = [];
    var ID = null;
    var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Milestone Account')/items?@target='" + hostweburl + "'&$select=milestone_account_estimate,account_myob_id/account_myob_id&$expand=account_myob_id/account_myob_id&$filter=milestone_id/ID eq '" + milestoneId + "' ";

    var executor = new SP.RequestExecutor(appweburl);
    executor.executeAsync({
        url: requestUrl,
        async: false,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;

            if (results !== undefined && results.length > 0) {
                result = results;
                result['milestoneData'] = milestoneData;

            } else {
                result = null;
            }

            deffered.resolve(result);
        },
        error: function (data) {
            deffered.reject(false);
        }

    });

    return deffered.promise;
}

//==============================================================================================
//===================================== GET RECENT DOCUMENTS ===================================

//recent document
function getRecentDocumentNames(Q, projectNumber) {
    var rootLibraryName = 'Jobs';
    var claimLibraryName = 'Milestone Claims';
    var variationDocumentName = 'Variations';
    var recentDocuments = [];

    $('#recentDocument').children('tbody').empty();
    LinkDocuments = [];

    getProjectFolderName(Q, projectNumber).then(function (projectFolderPath) {

        //console.log(projectFolderPath);
        var milestoneLibraryPath = rootLibraryName + '/' + projectFolderPath + '/' + claimLibraryName;
        var variationLibraryPath = rootLibraryName + '/' + projectFolderPath + '/' + variationDocumentName;

        //console.log('milestone library path :'+milestoneLibraryPath);
        //console.log('variation library path :'+variationLibraryPath);

        //get milestone claim document
        getDocumentNames(Q, milestoneLibraryPath).then(function (milestoneDocuments) {

            if (milestoneDocuments.length > 0) {
                for (var i = 0; i < milestoneDocuments.length; i++) {
                    milestoneDocuments[i]['documentLibraryName'] = milestoneLibraryPath;
                }
            }
            //console.log(milestoneDocuments);


            //get variation documents
            getDocumentNames(Q, variationLibraryPath).then(function (variationDocuments) {
                //console.log(variationDocuments);

                if (variationDocuments.length > 0) {
                    for (var i = 0; i < variationDocuments.length; i++) {
                        variationDocuments[i]['documentLibraryName'] = variationLibraryPath;
                    }
                }

                //if recent document is available
                if (milestoneDocuments.length > 0 || variationDocuments.length > 0) {

                    if (milestoneDocuments.length > 0 && variationDocuments.length > 0) {
                        recentDocuments = variationDocuments.concat(milestoneDocuments);
                    }

                    else if (milestoneDocuments.length > 0 && !variationDocuments.length > 0) {
                        recentDocuments = milestoneDocuments;
                    }

                    else if (!milestoneDocuments.length > 0 && variationDocuments.length > 0) {
                        recentDocuments = variationDocuments;
                    }

                    //sort recent doument by date
                    recentDocuments.sort(function (a, b) {
                        return new Date(b.TimeLastModified).getTime() - new Date(a.TimeLastModified).getTime()
                    });

                    for (var i = 0; i < recentDocuments.length; i++) {
                        var documentDateIso = recentDocuments[i].TimeLastModified;
                        var documentDate = new Date(documentDateIso).toLocaleDateString();
                        var fullDocumentName = recentDocuments[i].Name;
                        var splitDocumentName = fullDocumentName.split('.');
                        var documentName = splitDocumentName[0];
                        //var documentUrl = results[i].ServerRelativeUrl;      
                        var documentAuthor = recentDocuments[i].Author.Title;
                        var documentUrl = hostweburl + "/" + recentDocuments[i].documentLibraryName + "/" + fullDocumentName;
                        var correctDocumentUrl = documentUrl.replace(/\s/g, "%20");

                        LinkDocuments.push({ DocumentDate: documentDate, DocumentName: documentName, DocumentUrl: documentUrl, DocumentAuthor: documentAuthor, CorrectDocumentUrl: correctDocumentUrl });

                        //var recentDocument = { DocumentDate: documentDate, DocumentName: documentName, DocumentUrl: documentUrl, DocumentAuthor: documentAuthor };
                        //recentDocuments.push(recentDocument);

                        var tr;
                        tr = $('<tr/>');
                        tr.append("<td>" + documentDate + "</td>");
                        tr.append("<td> <a href=" + correctDocumentUrl + ">" + documentName + "</a></td>");
                        tr.append("<td>" + documentAuthor + "</td>");
                        $('#recentDocument').children('tbody').append(tr);

                    }
                }
                //console.log(recentDocuments);
            });

        });
    });

}

//variation document
function getVariavionDocumentNames(Q, projectNumber) {
    var rootLibraryName = 'Jobs';
    var variationDocumentName = 'Variations';
    var recentDocuments = [];

    LinkDocuments = [];

    var deffered = Q.defer();

    getProjectFolderName(Q, projectNumber).then(function (projectFolderPath) {

        var variationLibraryPath = rootLibraryName + '/' + projectFolderPath + '/' + variationDocumentName;

        //get variation documents
        getDocumentNames(Q, variationLibraryPath).then(function (variationDocuments) {
            //console.log(variationDocuments);

            if (variationDocuments.length > 0) {
                for (var i = 0; i < variationDocuments.length; i++) {
                    variationDocuments[i]['documentLibraryName'] = variationLibraryPath;
                }
            }

            //if recent document is available
            if (variationDocuments.length > 0) {

                recentDocuments = variationDocuments;

                //sort recent doument by date
                recentDocuments.sort(function (a, b) {
                    return new Date(b.TimeLastModified).getTime() - new Date(a.TimeLastModified).getTime()
                });

                for (var i = 0; i < recentDocuments.length; i++) {

                    var documentDateIso = recentDocuments[i].TimeLastModified;
                    var documentDate = new Date(documentDateIso).toLocaleDateString();
                    var fullDocumentName = recentDocuments[i].Name;
                    var splitDocumentName = fullDocumentName.split('.');
                    var documentName = splitDocumentName[0];
                    //var documentUrl = results[i].ServerRelativeUrl;      
                    var documentAuthor = recentDocuments[i].Author.Title;
                    var documentUrl = hostweburl + "/" + recentDocuments[i].documentLibraryName + "/" + fullDocumentName;
                    var correctDocumentUrl = documentUrl.replace(/\s/g, "%20");

                    LinkDocuments.push({ DocumentDate: documentDate, DocumentName: documentName, DocumentUrl: documentUrl, DocumentAuthor: documentAuthor, CorrectDocumentUrl: correctDocumentUrl });
                }

                deffered.resolve(LinkDocuments);

            } else
                deffered.resolve(LinkDocuments);

        });


    });

    return deffered.promise;
}

//get document details
function getDocumentNames(Q, libraryPath) {
    var deffered = Q.defer();
    var results = [];

    var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/GetFolderByServerRelativeUrl('" + libraryPath + "')/Files?@target='" + hostweburl + "'&$select=Name,TimeLastModified,ServerRelativeUrl&$orderby=TimeLastModified desc&$expand=Author&$top=10";


    var executor = new SP.RequestExecutor(appweburl);
    executor.executeAsync({
        url: requestUrl,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;

            deffered.resolve(results);
        },
        error: function (error) {
            deffered.reject(error);
        }
    });

    return deffered.promise;
}

//get document details
function getAllDocumentNames(Q, libraryPath) {
    var deffered = Q.defer();
    var results = [];

    var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/GetFolderByServerRelativeUrl('" + libraryPath + "')/Files?@target='" + hostweburl + "'&$select=Name,TimeLastModified,ServerRelativeUrl&$orderby=TimeLastModified desc&$expand=Author";


    var executor = new SP.RequestExecutor(appweburl);
    executor.executeAsync({
        url: requestUrl,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;

            deffered.resolve(results);
        },
        error: function (error) {
            deffered.reject(error);
        }
    });

    return deffered.promise;
}

//get project folder name
function getProjectFolderName(Q, projectNumber) {
    var deffered = Q.defer();
    var projectListName = 'Project'
    var filterColumn = 'project_number';
    var selectedColumn = 'project_type,Title';

    getItemIdsByKey(Q, projectListName, filterColumn, projectNumber, selectedColumn).then(function (outPut) {

        //if project id exist
        //console.log(projectType);
        if (outPut != null) {

            var projectTypePrefix = "";

            if (outPut[0].project_type == "Residential")
                projectTypePrefix = "4.1";
            else if (outPut[0].project_type == "Commercial")
                projectTypePrefix = "4.2";

            deffered.resolve(projectTypePrefix + ' ' + outPut[0].project_type + "/" + projectNumber + "-" + outPut[0].Title);
            //console.log(outPut[0].project_type + "/" + projectNumber + "-" + outPut[0].Title);
        }

        else {
            //console.log('Project ID' + projectType + 'does not exist');
        }
    });
    return deffered.promise;
}

//==============================================================================================
//=============================== CHARTS DATA=======================================================

//projectScheduleMonitoringData
function projectScheduleMonitoringData(Q, projectNumber) {

    var outPut = [];
    var deffered = Q.defer();

    getMilestoneData(Q, projectNumber).then(function (milestone) {

        for (var i = 0; i < milestone.length; i++) {
            var dateForClaim = milestone[i].milestone_date_end;
            var claimActualDate = milestone[i].milestone_claim_actual_date;

            outPut.push({ DateForClaim: dateForClaim, claimActualDate: claimActualDate });
        }

        deffered.resolve(outPut);

    });

    return deffered.promise;
}

function getCumulativeMilestoneClaims(Q) {

    var deferred = Q.defer();
    var outPut = [];
    var results = [];
    var result;
    //var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Milestone')/items?@target='" + hostweburl + "'&$select=milestone_claim_estimate,milestone_claim_actual";
    var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Milestone')/items?@target='" + hostweburl + "'&$select=milestone_claim_actual,milestone_claim_actual_date&$filter=milestone_claim_actual_date ge datetime'" + getCurrentFinancialYear() + "'";

    var executor = new SP.RequestExecutor(appweburl);
    executor.executeAsync({
        url: requestUrl,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;

            deferred.resolve(results);

        },
        error: function (data) {
            deferred.reject(data);
            result = "inside error function";
        }
    });
    return deferred.promise;
}

//==============================================================================================







