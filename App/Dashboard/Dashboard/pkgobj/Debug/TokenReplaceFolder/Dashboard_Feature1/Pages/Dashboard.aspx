﻿<%@ Page Language="C#" MasterPageFile="~masterurl/default.master" Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <SharePoint:ScriptLink ID="ScriptLink1" Name="sp.js" runat="server" OnDemand="true" LoadAfterUI="true" Localizable="false" />
    <WebPartPages:AllowFraming ID="AllowFraming1" runat="server" />

    <script type="text/javascript" src="../Scripts/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.runtime.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.js"></script>
    <script type="text/javascript" src="../Scripts/App.js"></script>
    <script type="text/javascript" src="../Scripts/q.js"></script>
    
    <!--Start Dashboard references -->

    <!-- Bootstrap Core CSS -->
    <link href="../Content/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Sidebar CSS -->
    <link href="../Content/css/sidebar.css" rel="stylesheet" />

    <!-- Custom CSS -->
    <link href="../Content/css/style.css" rel="stylesheet" />

    <!-- Slider CSS -->
    <link rel="stylesheet" href="../Content/css/slider-style.css" type="text/css" media="all" />
    <link rel="stylesheet" href="../Content/css/jquery-ui.css" type="text/css" media="all" />

    <!-- jQuery -->
    <script type="text/javascript" src="../Scripts/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script type="text/javascript" src="../Scripts/bootstrap.min.js"></script>

    <!-- Slider JS files -->
    <script type="text/javascript" src="../Scripts/jquery-ui.js"></script>
    <script type="text/javascript" src="../Scripts/slider.js"></script>
    <!-- End Dashboard references-->

  

    <!---------------New References ---------------------------->
    <script type="text/javascript" src="../Scripts/dist/cpexcel.js"></script>
    <script type="text/javascript" src="../Scripts/shim.js"></script>
    <script type="text/javascript" src="../Scripts/jszip.js"></script>
    <script type="text/javascript" src="../Scripts/xlsx.js"></script>
    <script type="text/javascript" src="../Scripts/dist/ods.js"></script>
    <script type="text/javascript" src="../Scripts/jquery1.min.js"></script>
    <script type="text/javascript" src="../Scripts/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Scripts/popupWindow.js"></script>

    <!-- Kendo customize css -->
    <link href="../Content/kendo-customize.css" rel="stylesheet" />
    <link href="../Content/kendo.common.min.css" rel="stylesheet" />
    <link href="../Content/kendo.silver.min.css" rel="stylesheet" />
    <link href="../Content/kendo.dataviz.min.css" rel="stylesheet" />
    <link href="../Content/kendo.dataviz.silver.min.css" rel="stylesheet" />
    <!---------------end new references ------------------------>


    <!----------------- Chart.js -------- -->
    <script type="text/javascript" src="../Scripts/canvasjs.min.js"></script>
    <!-- -->

    <!----------------- Number.js -------- -->
    <script type="text/javascript" src="../Scripts/jquery.number.js"></script>
    <!-- -->

     <!-- Dialog JS files -->
    <%--<script type="text/javascript" src="../Scripts/jquery-1.11.2.min.js"></script>--%>
    <script type="text/javascript" src="../Scripts/jquery.dialogBox.js"></script>
    <link href="../Content/css/jquery.dialogbox.css" rel="stylesheet" />
    <!-- -->

     <!-- Loading CSS & JS -->
   <%-- <link href="../Content/css/font-awesome.css" rel="stylesheet" />
    <link href="../Content/css/loadingstyle.css" rel="stylesheet" />
    <script type="text/javascript" src="../Scripts/jquery.isloading.js"></script>--%>
    <!---------------------------------------------------------->

    <style type="text/css">
        .hover {
            text-decoration: none !important;
            color: #fff !important;
            background: #195F8B !important;
            font-size: 14px;
        }

        .ul-item {
            list-style-type: none;
        }

        .btn-width {
            width: 175px !important;
        }

        .th-left {
            text-align: left !important;
        }

        .td-left {
            text-align: left !important;
        }

        #s4-titlerow {
            display: none !important;
        }

        .dialog-box-container {
            padding-top: 15px !important;
            /*padding-bottom:10px !important;*/
        }

        .option {
            padding-top: 10px;
            margin-left: -20px;
            font-weight: 800 !important;
        }

        .option-notfound {
            padding-top: 10px;
            margin-left: -20px;
            font-weight: 800 !important;
            font-size: 30px;
            padding: 10px;
        }
    </style>

    <style type="text/css">
        #cssmenu,
        #cssmenu ul,
        #cssmenu ul li,
        #cssmenu ul li a {
            margin: 0;
            padding: 0;
            border: 0;
            list-style: none;
            line-height: 1;
            display: block;
            position: relative;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        #cssmenu {
            /*width: 200px;*/
            font-family: Helvetica, Arial, sans-serif;
            color: #ffffff;
        }

            #cssmenu ul ul {
                display: none;
            }

        .align-right {
            float: right;
        }

        #cssmenu > ul > li > a {
            padding: 15px 20px;
            border-left: 1px solid #1682ba;
            border-right: 1px solid #1682ba;
            border-top: 1px solid #1682ba;
            cursor: pointer;
            z-index: 2;
            font-size: 14px;
            font-weight: bold;
            text-decoration: none;
            color: #ffffff;
            text-shadow: 0 1px 1px rgba(0, 0, 0, 0.35);
            background: #36aae7;
            background: -webkit-linear-gradient(#36aae7, #1fa0e4);
            background: -moz-linear-gradient(#36aae7, #1fa0e4);
            background: -o-linear-gradient(#36aae7, #1fa0e4);
            background: -ms-linear-gradient(#36aae7, #1fa0e4);
            background: linear-gradient(#36aae7, #1fa0e4);
            box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.15);
        }

            #cssmenu > ul > li > a:hover,
            #cssmenu > ul > li.active > a,
            #cssmenu > ul > li.open > a {
                color: #eeeeee;
                background: #1fa0e4;
                background: -webkit-linear-gradient(#1fa0e4, #1992d1);
                background: -moz-linear-gradient(#1fa0e4, #1992d1);
                background: -o-linear-gradient(#1fa0e4, #1992d1);
                background: -ms-linear-gradient(#1fa0e4, #1992d1);
                background: linear-gradient(#1fa0e4, #1992d1);
            }

        #cssmenu > ul > li.open > a {
            box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.15), 0 1px 1px rgba(0, 0, 0, 0.15);
            border-bottom: 1px solid #1682ba;
        }

        #cssmenu > ul > li:last-child > a,
        #cssmenu > ul > li.last > a {
            border-bottom: 1px solid #1682ba;
        }

        .holder {
            width: 0;
            height: 0;
            position: absolute;
            top: 0;
            right: 0;
        }

            .holder::after,
            .holder::before {
                display: block;
                position: absolute;
                content: "";
                width: 6px;
                height: 6px;
                right: 20px;
                z-index: 10;
                -webkit-transform: rotate(-135deg);
                -moz-transform: rotate(-135deg);
                -ms-transform: rotate(-135deg);
                -o-transform: rotate(-135deg);
                transform: rotate(-135deg);
            }

            .holder::after {
                top: 17px;
                border-top: 2px solid #ffffff;
                border-left: 2px solid #ffffff;
            }

        #cssmenu > ul > li > a:hover > span::after,
        #cssmenu > ul > li.active > a > span::after,
        #cssmenu > ul > li.open > a > span::after {
            border-color: #eeeeee;
        }

        .holder::before {
            top: 18px;
            border-top: 2px solid;
            border-left: 2px solid;
            border-top-color: inherit;
            border-left-color: inherit;
        }

        #cssmenu ul ul li a {
            cursor: pointer;
            border-bottom: 1px solid #32373e;
            border-left: 1px solid #32373e;
            border-right: 1px solid #32373e;
            padding: 10px 20px;
            z-index: 1;
            text-decoration: none;
            font-size: 13px;
            color: #eeeeee;
            background: #49505a;
            box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.1);
        }

        #cssmenu ul ul li:hover > a,
        #cssmenu ul ul li.open > a,
        #cssmenu ul ul li.active > a {
            background: #424852;
            color: #ffffff;
        }

        #cssmenu ul ul li:first-child > a {
            box-shadow: none;
        }

        #cssmenu ul ul ul li:first-child > a {
            box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.1);
        }

        #cssmenu ul ul ul li a {
            padding-left: 30px;
        }

        #cssmenu > ul > li > ul > li:last-child > a,
        #cssmenu > ul > li > ul > li.last > a {
            border-bottom: 0;
        }

        #cssmenu > ul > li > ul > li.open:last-child > a,
        #cssmenu > ul > li > ul > li.last.open > a {
            border-bottom: 1px solid #32373e;
        }

        #cssmenu > ul > li > ul > li.open:last-child > ul > li:last-child > a {
            border-bottom: 0;
        }

        #cssmenu ul ul li.has-sub > a::after {
            display: block;
            position: absolute;
            content: "";
            width: 5px;
            height: 5px;
            right: 20px;
            z-index: 10;
            top: 11.5px;
            border-top: 2px solid #eeeeee;
            border-left: 2px solid #eeeeee;
            -webkit-transform: rotate(-135deg);
            -moz-transform: rotate(-135deg);
            -ms-transform: rotate(-135deg);
            -o-transform: rotate(-135deg);
            transform: rotate(-135deg);
        }

        #cssmenu ul ul li.active > a::after,
        #cssmenu ul ul li.open > a::after,
        #cssmenu ul ul li > a:hover::after {
            border-color: #ffffff;
        }
    </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <WebPartPages:WebPartZone runat="server" FrameType="TitleBarOnly" ID="full" Title="loc:full" />


    <div id="wrapper">
        <div id="sidebar-wrapper">
            <%--<div id='cssmenu'>
                <ul>
                    <li class='active'><a href='#'><span>All Projects</span></a></li>
                    <li class="has-sub open"><a style="text-shadow: rgba(0, 0, 0, 0.34902) 0px 1px 1px;"><span style="border-color: rgba(0, 0, 0, 0.34902);">Open Projects</span><span class="holder" style="border-color: rgba(0, 0, 0, 0.34902);"></span></a>
                        <ul style="display: block;">
                            <li><a href="#"><span>Product 1</span></a></li>
                            <li><a href="#"><span>Product 2</span></a></li>
                        </ul>
                    </li>
                    <li class='has-sub last'><a href='#'><span>Close Projects</span></a>
                        <ul>
                            <li ><a href='#'><span>Product 1</span></a></li>
                            <li ><a href='#'><span>Product 2</span></a></li>
                            <li ><a href='#'><span>Product 1</span></a></li>
                            <li ><a href='#'><span>Product 2</span></a></li>
                        </ul>
                    </li>
                </ul>
            </div>--%>

            <div id='cssmenu'>
                <ul>
                    <li onclick='clickOnNave(this);'><a href='#'><span>All Projects</span></a></li>
                    <li class='active has-sub'><a id='a-open' onclick='clickHasSub(this)' href='#'><span>Open Projects</span></a>
                        <ul></ul>
                    </li>
                    <li class='last has-sub'><a onclick='clickHasSub(this)' href='#'><span>Past Projects</span></a>
                        <ul></ul>
                    </li>
                </ul>
            </div>

        </div>
        <!-- Sidebar -->
        <%--<div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="selected" ><a href="#">All Projects</a></li>
                <li class="sidebar-brand selected hover"><span class="hover">Open Projects</span></li>
                <li class="sidebar-brand selected hover"><span class="hover">Close Projects</span></li>
            </ul>
        </div>--%>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12 no-padding">
                        <a href="#menu-toggle" class="btn btn-default btn-menu-toggle no-border-radius btn-width" id="menu-toggle"><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span> Show/Hide project list</a>
                    </div>
                    <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12 no-padding">
                        <a href="#" id="UpdateBussinessData" class="btn btn-default btn-menu-toggle no-border-radius btn-width"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span> Update Bussiness Data</a>
                    </div>
                    <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12 no-padding">
                        <%--<a href="https://secure.myob.com/oauth2/account/authorize?client_id=2p9jsgqnauqyfscqaj89pexa&redirect_uri=https://rukshan-f1cbcd86dc77f5.sharepoint.com/sites/Dashboard/Dashboard/Pages/NewProject.aspx&response_type=code&scope=CompanyFile" id="newProject" class="btn btn-default btn-menu-toggle no-border-radius btn-width" ><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>New Project</a>--%>
                        <a href="#" id="newProject" class="btn btn-default btn-menu-toggle no-border-radius btn-width"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> New Project</a>
                    </div>
                    <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12 no-padding">
                        <button id="chart" class="btn btn-default btn-menu-toggle no-border-radius btn-width" data-toggle="modal" data-target="#charts-modal"><span class="glyphicon glyphicon-indent-right" aria-hidden="true"></span> Charts</button>
                    </div>
                    <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12 no-padding">
                        <button id="btnExport" class="btn btn-default btn-menu-toggle no-border-radius btn-width" ><span class="glyphicon glyphicon-cloud-download" aria-hidden="true""></span> Export</button>
                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <div class="row margin-top5">

                    <input type="hidden" id="appweburl" />
                    <input type="hidden" id="hostweburl" />
                    <input type="hidden" id="imported_project_id" />

                    <!-- top form -->
                    <div class="col-md-12 no-padding">
                        <div class="col-md-12 no-padding top-form-wrapper">
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-8 control-label no-padding-md" style="font-weight: 100;">Estimate project value for Financial Year</label>
                                        <div class="col-sm-4">
                                            <input type="text" id="estimateprojectvalue" value="" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-8 control-label no-padding-md" style="font-weight: 100;">Actual project value for Financial Year</label>
                                        <div class="col-sm-4">
                                            <input type="text" id="actualprojectvalue" value="" disabled="disabled" class="form-control" />
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-8 control-label no-padding-md">Cost Budget for Financial Year</label>
                                        <div class="col-sm-4">
                                            <%--<input type="text" id="Text1" value="$650000" class="form-control">--%>
                                            <input type="text" id="costbudget" value="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-8 control-label no-padding-md">Gross Profit for Financial Year</label>
                                        <div class="col-sm-4">
                                            <%--<input type="text" id="Text2" value="$120000" class="form-control">--%>
                                            <input type="text" id="grossprofit" value="" disabled="disabled" class="form-control" />
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-8 control-label no-padding-md">Total project claim value</label>
                                        <div class="col-sm-4">
                                            <input type="text" id="totalprojectclaim" value="" disabled="disabled" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-8 control-label no-padding-md">Remaining Claim Value</label>
                                        <div class="col-sm-4">
                                            <input type="text" id="remainingclaim" value="" disabled="disabled" class="form-control" />
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="col-md-12 no-padding margin-top20">
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <div class="col-lg-12 no-padding padding-for-large">
                                    <h4 class="project-label margin-bottom20">Project - <span id="project-label">All Projects</span></h4>
                                    <form class="form-horizontal">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-lg-7 col-sm-6 control-label no-padding-md">Total contract value</label>
                                            <div class="col-lg-5 col-sm-6">
                                                <input type="text" id="totalcontractvalue" value="" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-lg-7 col-sm-6 control-label no-padding-md">Supervisor allocation</label>
                                            <div class="col-lg-5 col-sm-6">
                                                <input type="text" id="supervisorallocation" disabled="disabled" value="" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-lg-7 col-sm-6 control-label no-padding-md">Supervisor cost incurred</label>
                                            <div class="col-lg-5 col-sm-6">
                                                <input type="text" id="supervisorcostincurred" disabled="disabled" value="" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-lg-7 col-sm-6 control-label no-padding-md">Building margin</label>
                                            <div class="col-lg-5 col-sm-6">
                                                <input type="text" id="buildingmargin" value="" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-lg-7 col-sm-6 control-label no-padding-md">Insurances</label>
                                            <div class="col-lg-5 col-sm-6">
                                                <input type="text" id="insurances" disabled="disabled" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-lg-7 col-sm-6 control-label no-padding-md">Status</label>
                                            <div class="col-lg-5 col-sm-6">
                                                <%--<div class="dropdown pull-right width-100">
                                                    <button class="btn btn-default dropdown-toggle width-100 no-border-radius" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                                                        Open
                                               
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                                        <li role="presentation" class="text-center"><a role="menuitem" tabindex="-1" href="#">Open</a></li>
                                                        <li role="presentation" class="text-center"><a role="menuitem" tabindex="-1" href="#">Close</a></li>
                                                    </ul>
                                                </div>--%>
                                                <div class="dropdown pull-right width-100">
                                                    <select id="status" class="form-control" style="padding: 6px 7px;">
                                                        <option value="Open" selected="selected">Open</option>
                                                        <option value="Past">Past</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <!-- Milestones -->
                            <div class="col-lg-9 col-md-6 col-sm-12 no-padding">
                                <div class="col-md-12">
                                    <h4>Milestones
                                        <%--<button type="submit" class="btn btn-sm btn-default btn-export">Edit</button>--%></h4>
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-6 col-sm-12 no-padding">
                                <!-- Tables -->
                                <div class="col-md-12 no-padding">
                                    <!-- Table 1 -->
                                    <div class="col-md-12 milestone-wrapper">
                                        <table id="milestone" class="table milestone-wrapper-table sub-tables">
                                            <tbody>
                                                <tr>
                                                    <td class="wrapper-table-cell"></td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.End Table 1 -->
                                </div>
                                <!-- /.End Tables -->

                            </div>
                            <!-- /.End Milestones -->
                        </div>

                        <div class="col-md-12 no-padding margin-top20">
                            <!-- Costings table -->
                            <div class="col-lg-12 col-md-12 col-sm-12 overflow-x-scroll">
                                <h4>Costings</h4>
                                <table id="costings" class="table table-bordered table-striped table-condensed table-responsive">
                                    <thead>
                                        <tr>
                                            <th class="table-head-th th-left">Description</th>
                                            <th class="table-head-th">Costed</th>
                                            <th class="table-head-th">Incurred</th>
                                            <th class="table-head-th th-left">Description</th>
                                            <th class="table-head-th">Costed</th>
                                            <th class="table-head-th">Incurred</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.End Costings table -->

                            <!-- Variations table -->
                           <%-- <div class="col-lg-6 col-md-12 col-sm-12 overflow-x-scroll">
                                <h4>Variations</h4>
                                <div id="grdVariations"></div>
                                <br />
                            </div>--%>
                            <!-- /.End Variations table -->
                        </div>
                    </div>
                    <!-- /.end top form -->

                    <!-- right sidebar -->
                    <div class="col-md-12 no-padding right-sidebar">
                        <div class="col-md-6 col-sm-6">
                         <h4>Variations</h4>
                                <div id="grdVariations"></div>
                            </div>
                        <%--<div class="col-md-6 col-sm-6">
                            <!-- Yammer -->
                            <table class="table table-bordered table-condensed table-responsive">
                                <thead>
                                    <tr>
                                        <th class="table-head-th">Project Number (Yammer)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- /.End Yammer -->
                        </div>--%>

                        <div class="col-md-6 col-sm-6">
                            <!-- Recent docs -->
                             <h4>Recent Documents</h4>
                            <table id="recentDocument" class="table table-bordered table-striped table-condensed table-responsive">
                                <thead>
                                    <tr>
                                        <th class="table-head-th">Date</th>
                                        <th class="table-head-th">Document</th>
                                        <th class="table-head-th">Author</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                            <!-- /.End recent docs -->
                        </div>
                    </div>
                    <!-- /.end right sidebar -->
                </div>
            </div>

        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->

    <div>
        <p id="message">
            <!-- The following content will be replaced with the user name when you run the app - see App.js -->
        </p>
    </div>

    <script type="text/javascript">

        //-----------------------------------------------------------------
        var Variations = [];
        var projectId;
        var chart;

        $(document).ready(function () {

            $("#newProject").attr("href", "https://secure.myob.com/oauth2/account/authorize?client_id=2p9jsgqnauqyfscqaj89pexa&redirect_uri=" + $("#appweburl").val() + "/Pages/NewProject.aspx&response_type=code&scope=CompanyFile");

            var dataSource = new kendo.data.DataSource({
                pageSize: 10,
                transport: {
                    /* the other CRUD settings are ommitted for brevity */
                    create: function (e) {

                        var variationData = {
                            variation_number: e.data.VariationNumber,
                            Title: '#',
                            variation_status: e.data.Status['Name'],
                            variation_amount: e.data.Amount,
                            variation_adjusted_contract: e.data.AdjestedValue,
                            variation_description: e.data.Description,
                            variation_issue_date: kendo.toString(e.data.DateOfIssue, "d")
                        };

                        insertVariationsData(Q, projectId, variationData).then(function (Id) {
                            e.data.Id = Id;
                            e.success(e.data);

                        });

                    }, update: function (e) {

                        //update variation data
                        var listTitle = 'Variations';
                        var listItemId = e.data.Id;

                        var itemProperties = {
                            variation_number: e.data.VariationNumber,
                            Title: e.data.Title,//'RecentDocument/Residential/',
                            variation_status: e.data.Status['Name'],
                            variation_amount: e.data.Amount,
                            variation_adjusted_contract: e.data.AdjestedValue,
                            variation_description: e.data.Description,
                            variation_issue_date: kendo.toString(e.data.DateOfIssue, "d")
                        };

                        updateListItemQ(Q, listTitle, listItemId, itemProperties).then(function () {
                            e.success(e.data);

                            $('#grdVariations').data('kendoGrid').dataSource.read();
                            $('#grdVariations').data('kendoGrid').refresh();

                        });

                    }, destroy: function (e) {

                        //delete variation data
                        var listTitle = 'Variations';
                        var listItemId = e.data.Id;

                        deleteListItem(Q, listTitle, listItemId);

                    }, read: function (e) {

                        Variations = [];

                        if (projectId > 0) {

                            getAllVariationData(Q, projectId).then(function (variationData) {

                                if (variationData != null && variationData != undefined) {

                                    variationData.forEach(function (V) {
                                        if (V.Milestone == V.milestone_name)
                                            _milestomeForndInList = true;

                                        var _status = { Name: V.variation_status, StatusId: 1 };
                                        if (V.variation_status == "Approved")
                                            _status['StatusId'] = 2;

                                        var TitleExist = false;
                                        var Template = "";

                                        if (V.Title != '#') {
                                            TitleExist = true;
                                            Template = '<a href="' + V.Title + '">' + V.variation_number + '</a>';
                                        }

                                        Variations.push({

                                            Id: V.ID,
                                            VariationNumber: V.variation_number,
                                            Title: V.Title,
                                            TitleExist: TitleExist,
                                            Template: Template,
                                            Status: _status,
                                            DateOfIssue: kendo.parseDate(V.variation_issue_date),
                                            Description: V.variation_description,
                                            Amount: V.variation_amount,
                                            AdjestedValue: V.variation_adjusted_contract

                                        });

                                    });
                                }

                                e.success(Variations);

                            });

                        } else
                            e.success(Variations);
                    }
                },
                //data: Variations,
                autoSync: false,
                schema: {
                    model: {
                        id: "Id",
                        fields: {
                            Id: { editable: false, nullable: true },
                            VariationNumber: { type: "number", validation: { required: true } },
                            Title: { type: "string" },
                            TitleExist: { type: "bool", defaultValue: false },
                            Template: { type: "string", editable: false },
                            Status: { defaultValue: { StatusId: 1, Name: "Pending" } },
                            DateOfIssue: { type: "date", validation: { required: true } },
                            Description: { type: "string", validation: { required: true } },
                            Amount: { type: "number", validation: { required: true, min: 0 } },
                            AdjestedValue: { type: "number", validation: { required: false, min: 0 } }
                        }
                    }
                }
            });

            $("#grdVariations").kendoGrid({
                dataSource: dataSource,
                pageable: true,
                scorable: true,
                sortable: true,
                height: 300,
                toolbar: ["create"],
                columns: [
                    {
                        field: "VariationNumber", format: "{0:0}", title: "Variation #", width: "90px", editor: editNumber,
                        template: '#= TitleExist ? Template : VariationNumber #'
                    },
                    { field: "Status", title: "Status", width: "80px", editor: statusDropDownEditor, template: "#=Status.Name#" },
                    { field: "DateOfIssue", title: "Date of issue", width: "100px", format: "{0:dd/MM/yyyy}" },
                    { field: "Description", title: "Description", width: "140px" },
                    { field: "Amount", title: "Amount", format: "{0:c0}", width: "70px", attributes: { style: "text-align:right;" }, editor: editNumber },
                    { field: "AdjestedValue", title: "Adj. value", format: "{0:c0}", width: "80px", attributes: { style: "text-align:right;" }, editor: editNumber },
                    {
                        command: [
                          { name: "edit" },
                          { name: "destroy" },
                          {
                              name: "Link",
                              click: function (e) {

                                  var dataItem = this.dataItem($(e.currentTarget).closest("tr"));

                                  getVariavionDocumentNames(Q, projectId).then(function (Documents) {

                                      $("#SelectedLinkedUrl").val('');
                                      var _content = '<div style="overflow:auto;height:400px;">';
                                      var confirmValue = "Link";

                                      if (Documents.length > 0) {
                                          for (var i = 0; i < Documents.length; i++) {

                                              _content += '<div class="col-sm-12 col-md-12 col-lg-12">' +
                                                            '<div class="row">' +
                                                                '<div class="col-sm-2 col-md-2 col-lg-2">' +
                                                                    '<input onclick="clickOnLinkRadio(this);" type="radio" value=' + Documents[i]['CorrectDocumentUrl'] + ' name="link-rario" class="form-control" />' +
                                                                '</div>' +
                                                                '<div class="col-sm-10 col-md-10 col-lg-10">' +
                                                                    '<label class="option">' + Documents[i]['DocumentName'] + '</label>' +
                                                                '</div>' +
                                                            '</div>' +
                                                         '</div>';

                                          }

                                      } else {

                                          _content += '<div class="col-sm-12 col-md-12 col-lg-12">' +
                                                             '<div class="row">' +
                                                                '<div class="col-sm-2 col-md-2 col-lg-2">' +
                                                                    '<label class="option-notfound">Could Not Found Any Document</label>' +
                                                               '</div>' +
                                                            '</div>' +
                                                      '</div>';

                                          confirmValue = "OK";
                                      }

                                      _content += '</div>';

                                      $('#link-dialogBox').dialogBox({
                                          width: 650,
                                          height: 600,
                                          hasClose: true,
                                          hasBtn: true,
                                          confirmValue: confirmValue,
                                          confirm: function () {

                                              if ($("#SelectedLinkedUrl").val() != '') {

                                                  var Id = dataItem['Id'];

                                                  if (Id > 0) {

                                                      var itemProperties = {
                                                          variation_number: dataItem.VariationNumber,
                                                          Title: $("#SelectedLinkedUrl").val(),
                                                          variation_status: dataItem.Status['Name'],
                                                          variation_amount: dataItem.Amount,
                                                          variation_adjusted_contract: dataItem.AdjestedValue,
                                                          variation_description: dataItem.Description,
                                                          variation_issue_date: kendo.toString(dataItem.DateOfIssue, "d")
                                                      };

                                                      updateListItemQ(Q, 'Variations', Id, itemProperties).then(function () {

                                                          $('#grdVariations').data('kendoGrid').dataSource.read();
                                                          $('#grdVariations').data('kendoGrid').refresh();

                                                      });
                                                  }
                                              }

                                          },
                                          cancelValue: 'Cancel',
                                          title: 'Link Document',
                                          hasClose: true,
                                          content: _content
                                      });

                                  });

                              }
                          }
                        ], title: "&nbsp;", width: 235
                    }],
                editable: "inline",
                edit: function (e) {

                    if (e.model.isNew()) {
                        //set field
                        e.model.set("TitleExist", false);
                    }
                }

            });

        });

        function clickOnLinkRadio(e) {

            $("#SelectedLinkedUrl").val($(e).attr('value'));
        }

        $("#btnExport").click(function (e) {

            if (projectId > 0) {

                var table1 = '<table id="tbl1" class="table2excel">' +
                               '<tr >' +
                                  '<td style="width:200px;">Field</td>' +
                                  '<td style="width:300px;">EstimateValue</td>' +
                                  '<td style="width:150px;">ActualValue</td>' +
                               '</tr>';

                getImport1Data(Q, projectId).then(function (Import1) {

                    var i = 0;

                    Import1.forEach(function (Imp1) {

                        if (i == 0) {

                            table1 += '<tr><td>Project Number</td><td>' + projectId + '</td></tr>' +
                                        '<tr><td>Project Type</td><td>' + (Imp1['project_type'] == null || Imp1['project_type'] == undefined ? "" : Imp1['project_type']) + '</td><td></td></tr>' +
                                        '<tr><td>Project Name</td><td>' + (Imp1['Title'] == null || Imp1['Title'] == undefined ? "" : Imp1['Title']) + '</td><td></td></tr>' +
                                        '<tr><td>Project Address</td><td>' + (Imp1['project_address'] == null || Imp1['project_address'] == undefined ? "" : Imp1['project_address']) + '</td><td></td></tr>' +
                                        '<tr><td>Principle Name</td><td>' + (Imp1['project_principle_name'] == null || Imp1['project_principle_name'] == undefined ? "" : Imp1['project_principle_name']) + '</td><td></td></tr>' +
                                        '<tr><td>Principle Address</td><td>' + (Imp1['project_principle_address'] == null || Imp1['project_principle_address'] == undefined ? "" : Imp1['project_principle_address']) + '</td><td></td></tr>' +
                                        '<tr><td>Principle Email</td><td>' + (Imp1['project_principle_email'] == "null" ? "" : Imp1['project_principle_email']) + '</td><td></td></tr>' +
                                        '<tr><td>Principle Business Name</td><td>' + (Imp1['project_principle_business_name'] == null || Imp1['project_principle_business_name'] == undefined ? "" : Imp1['project_principle_business_name']) + '</td><td></td></tr>' +
                                        '<tr><td>PrincipleABN</td><td>' + (Imp1['project_principle_abn'] == null || Imp1['project_principle_abn'] == undefined ? "" : Imp1['project_principle_abn']) + '</td><td></td></tr>' +
                                        '<tr><td>PrincipleECT</td><td>' + (Imp1['project_principle_ect'] == null || Imp1['project_principle_ect'] == undefined ? "" : Imp1['project_principle_ect']) + '</td><td></td></tr>' +
                                        '<tr><td>Representative Name</td><td>' + (Imp1['project_rep_name'] == null || Imp1['project_rep_name'] == undefined ? "" : Imp1['project_rep_name']) + '</td><td></td></tr>' +
                                        '<tr><td>Representative Address</td><td>' + (Imp1['project_rep_address'] == null || Imp1['project_rep_address'] == undefined ? "" : Imp1['project_rep_address']) + '</td><td></td></tr>' +
                                        '<tr><td>Representative Email</td><td>' + (Imp1['project_rep_email'] == null || Imp1['project_rep_email'] == undefined ? "" : Imp1['project_rep_email']) + '</td><td></td></tr>' +
                                        '<tr><td>Representative Business Name</td><td>' + (Imp1['project_rep_business_name'] == null || Imp1['project_rep_business_name'] == undefined ? "" : Imp1['project_rep_business_name']) + '</td><td></td></tr>' +
                                        '<tr><td>Representative ABN</td><td>' + (Imp1['project_rep_abn'] == null || Imp1['project_rep_abn'] == undefined ? "" : Imp1['project_rep_abn']) + '</td><td></td></tr>' +
                                        '<tr><td>Representative ECT</td><td>' + (Imp1['project_rep_ect'] == null || Imp1['project_rep_ect'] == undefined ? "" : Imp1['project_rep_ect']) + '</td><td></td></tr>' +
                                        '<tr><td>Building Margin</td><td data-type="Number">' + (Imp1['project_building_margin'] == null || Imp1['project_building_margin'] == undefined ? 0 : Imp1['project_building_margin']) + '</td><td></td></tr>' +
                                        '<tr><td>Contingency</td><td data-type="Number">' + (Imp1['project_contingency'] == null || Imp1['project_contingency'] == undefined ? 0 : Imp1['project_contingency']) + '</td><td></td></tr>';

                        } else
                            table1 += '<tr><td>' + Imp1['AccountMyobId'] + '</td><td data-style="Currency" data-type="Number">' + (Imp1['ProjectAccountEstimate'] == null || Imp1['ProjectAccountEstimate'] == undefined ? 0 : Imp1['ProjectAccountEstimate']) + '</td><td data-style="Currency" data-type="Number">' + (Imp1['ProjectAccountIncurred'] == null || Imp1['ProjectAccountIncurred'] == undefined ? 0 : Imp1['ProjectAccountIncurred']) + '</td></tr>';

                        i++;

                    });

                    table1 += '</table>';

                    tablesToExcel([$(table1)], ['Import_1'], 'DixonConstruction_' + projectId + '.xls', 'Excel');

                });
            }

            e.preventDefault();

        });

        var tablesToExcel = (function () {

            var uri = 'data:application/vnd.ms-excel;base64,'
            , tmplWorkbookXML = '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">'
              + '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Author>Axel Richter</Author><Created>{created}</Created></DocumentProperties>'
              + '<Styles>'
              + '<Style ss:ID="Currency"><NumberFormat ss:Format="Currency"></NumberFormat></Style>'
              + '<Style ss:ID="Date"><NumberFormat ss:Format="dd-mmm-yy"></NumberFormat></Style>'
              + '<Style ss:ID="Header"><Font ss:Bold="1"></Font><Alignment ss:Horizontal="Center"></Alignment></Style>'
              + '</Styles>'
              + '{worksheets}</Workbook>'
            , tmplWorksheetXML = '<Worksheet ss:Name="{nameWS}"><Table>{rows}</Table></Worksheet>'
            , tmplCellXML = '<Cell {attributeStyleID}{attributeFormula}><Data ss:Type="{nameType}">{data}</Data></Cell>'
            , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
            , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
            return function (tables, wsnames, wbname, appname) {
                var ctx = "";
                var workbookXML = "";
                var worksheetsXML = "";
                var rowsXML = "";

                for (var i = 0; i < tables.length; i++) {

                    tables[i] = tables[i][0];

                    //console.log(tables[i].rows[0].cells);

                    for (var j = 0; j < tables[i].rows[0].cells.length; j++) {
                        var width = tables[i].rows[0].cells[j].style.width.toString().replace('p', '').replace('x', '');
                        rowsXML += '<Column ss:Width="' + width + '"/>';
                    }

                    for (var j = 0; j < tables[i].rows.length; j++) {
                        rowsXML += '<Row>'
                        for (var k = 0; k < tables[i].rows[j].cells.length; k++) {
                            var dataType = tables[i].rows[j].cells[k].getAttribute("data-type");
                            var dataStyle = tables[i].rows[j].cells[k].getAttribute("data-style");
                            if (tables[i].rows[j].cells[k].style.width && tables[i].rows[j].cells[k].style.width != "") dataStyle = "Header";
                            var dataValue = tables[i].rows[j].cells[k].getAttribute("data-value");
                            dataValue = (dataValue) ? dataValue : tables[i].rows[j].cells[k].innerHTML;
                            var dataFormula = tables[i].rows[j].cells[k].getAttribute("data-formula");
                            dataFormula = (dataFormula) ? dataFormula : (appname == 'Calc' && dataType == 'DateTime') ? dataValue : null;
                            ctx = {
                                attributeStyleID: (dataStyle == 'Currency' || dataStyle == 'Date' || dataStyle == 'Header') ? ' ss:StyleID="' + dataStyle + '"' : ''
                                   , nameType: (dataType == 'Number' || dataType == 'DateTime' || dataType == 'Boolean' || dataType == 'Error') ? dataType : 'String'
                                   , data: (dataFormula) ? '' : dataValue
                                   , attributeFormula: (dataFormula) ? ' ss:Formula="' + dataFormula + '"' : ''
                            };
                            rowsXML += format(tmplCellXML, ctx);
                        }
                        rowsXML += '</Row>'
                    }
                    ctx = { rows: rowsXML, nameWS: wsnames[i] || 'Sheet' + i };
                    worksheetsXML += format(tmplWorksheetXML, ctx);
                    rowsXML = "";
                }

                ctx = { created: (new Date()).getTime(), worksheets: worksheetsXML };
                workbookXML = format(tmplWorkbookXML, ctx);

                //console.log(workbookXML);

                var link = document.createElement("A");
                link.href = uri + base64(workbookXML);
                link.download = wbname || 'Workbook.xls';
                link.target = '_blank';
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        })();

        function statusDropDownEditor(container, options) {
            $('<input required data-text-field="Name" data-value-field="StatusId" data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                    autoBind: false,
                    dataSource: [{ StatusId: 1, Name: "Pending" }, { StatusId: 2, Name: "Approved" }]
                });
        }

        function editNumber(container, options) {
            $('<input data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoNumericTextBox({
                    spinners: false,
                    format: "n0"
                });
        }

        //-----------------------------------------------------------------

        $("#newProject").click(function (e) {

            e.preventDefault();
            $("#imported_project_id").val('');

            var w = 500;
            var h = 560;
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);

            var win = window.open($("#newProject").attr("href"), "New Project", 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
            var winTimer = window.setInterval(function () {
                if (win.closed !== false) {
                    // !== is required for compatibility with Opera
                    window.clearInterval(winTimer);
                    //Rebind the navigation list ===============

                    setTimeout(function () {

                        var imported_project_id = $("#imported_project_id").val();
                        //console.log(imported_project_id);
                        if (imported_project_id != undefined && imported_project_id != "")
                            getProjectData(Q, imported_project_id);

                    }, 3000);
                }
            }, 200);

        });

        $("#addFileButton").click(function (e) {
            e.preventDefault();

            // Define the folder path for this example.
            var serverRelativeUrlToFolder = 'DashboardDocuments';
            var fileInput = jQuery('#getFile');
            var newName = jQuery('#displayName').val();

            uploadFile(fileInput, newName, serverRelativeUrlToFolder);

        });

        $("#menu-toggle").click(function (e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });

        $("#import").click(function (e) {
            e.preventDefault();
            $("#xlf").trigger('click');
        });

        $("#chart").click(function (e) {
            e.preventDefault();
            $("#optionsRadios1").removeAttr('checked');
            $("#optionsRadios2").removeAttr('checked');

            if (chart)
                chart.destroy;

        });

        function GetClearValue(value) {

            var val = 0;
            var _val = value.toString().replace('$', '').replace(',', '').trim();

            if (_val.indexOf("-") >= 0) {
                val = parseFloat(_val.replace('-', '').trim()).toFixed(2);
            } else
                val = parseFloat(_val).toFixed(2);

            return val;
        }

        $("#UpdateBussinessData").click(function (e) {
            e.preventDefault();

            var businessProperties = {
                business_project_estimate: GetClearValue($("#estimateprojectvalue").val().trim()),
                business_cost_budget: GetClearValue($("#costbudget").val().trim())
            };

            updateBusinessLevelData(Q, businessProperties);

            if (projectId > 0) {

                var statusProperties = { project_status: $('#status').val() };

                updateProjectStatus(Q, projectId, statusProperties).then(function () {

                    setTimeout(function () {

                        getProjectData(Q, 'All Projects');

                    }, 2000);
                });


                /*var itemProperties1 = { 'Status': $('#status').val() };
                updateProjectData(Q, projectId, 'Project', itemProperties1).then(function () {

                    //Rebind the navigation list ===============

                    setTimeout(function () {
                        getProjectData();
                    }, 2000);
                });*/
            }

        });

        var X = XLSX;
        var XW = {
            /* worker message */
            msg: 'xlsx',
            /* worker scripts */
            rABS: './xlsxworker2.js',
            norABS: './xlsxworker1.js',
            noxfer: './xlsxworker.js'
        };

        var rABS = typeof FileReader !== "undefined" && typeof FileReader.prototype !== "undefined" && typeof FileReader.prototype.readAsBinaryString !== "undefined";
        var use_worker = typeof Worker !== 'undefined';
        var transferable = use_worker;
        var wtf_mode = false;
        var Accounts = [];

        function fixdata(data) {
            var o = "", l = 0, w = 10240;
            for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w, l * w + w)));
            o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w)));
            return o;
        }

        function ab2str(data) {
            var o = "", l = 0, w = 10240;
            for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint16Array(data.slice(l * w, l * w + w)));
            o += String.fromCharCode.apply(null, new Uint16Array(data.slice(l * w)));
            return o;
        }

        function s2ab(s) {
            var b = new ArrayBuffer(s.length * 2), v = new Uint16Array(b);
            for (var i = 0; i != s.length; ++i) v[i] = s.charCodeAt(i);
            return [v, b];
        }

        function xw_noxfer(data, cb) {
            var worker = new Worker(XW.noxfer);
            worker.onmessage = function (e) {
                switch (e.data.t) {
                    case 'ready': break;
                    case 'e': console.error(e.data.d); break;
                    case XW.msg: cb(JSON.parse(e.data.d)); break;
                }
            };
            var arr = rABS ? data : btoa(fixdata(data));
            worker.postMessage({ d: arr, b: rABS });
        }

        function xw_xfer(data, cb) {
            var worker = new Worker(rABS ? XW.rABS : XW.norABS);
            worker.onmessage = function (e) {
                switch (e.data.t) {
                    case 'ready': break;
                    case 'e': console.error(e.data.d); break;
                    default: xx = ab2str(e.data).replace(/\n/g, "\\n").replace(/\r/g, "\\r"); console.log("done"); cb(JSON.parse(xx)); break;
                }
            };
            if (rABS) {
                var val = s2ab(data);
                worker.postMessage(val[1], [val[1]]);
            } else {
                worker.postMessage(data, [data]);
            }
        }

        function xw(data, cb) {
            transferable = false;
            if (transferable) xw_xfer(data, cb);
            else xw_noxfer(data, cb);
        }

        function to_json(workbook) {
            var result = {};
            workbook.SheetNames.forEach(function (sheetName) {
                var roa = X.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                if (roa.length > 0) {
                    result[sheetName] = roa;
                }
            });
            return result;
        }

        function process_wb(wb) {

            var outputObj = to_json(wb);
            $('#xlf').val("");

            //Set Accounts Array=====================================================
            SetAccounts(outputObj['Accounts']);

            //============================ Costings =================================
            SetCostings(outputObj);

        }

        var xlf = document.getElementById('xlf');

        function handleFile(e) {
            rABS = false;
            use_worker = false;
            var files = e.target.files;
            var f = files[0];
            {
                var reader = new FileReader();
                var name = f.name;

                reader.onload = function (e) {
                    if (typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
                    var data = e.target.result;
                    if (use_worker) {
                        xw(data, process_wb);
                    } else {
                        var wb;
                        if (rABS) {
                            wb = X.read(data, { type: 'binary' });
                        } else {
                            var arr = fixdata(data);
                            wb = X.read(btoa(arr), { type: 'base64' });
                        }
                        process_wb(wb);
                    }
                };

                if (rABS) reader.readAsBinaryString(f);
                else reader.readAsArrayBuffer(f);
            }
        }

        if (xlf != null && xlf != undefined && xlf.addEventListener) xlf.addEventListener('change', handleFile, false);

        function SetAccounts(xflAccountsTab) {

            Accounts = [];
            var i = 0;

            xflAccountsTab.forEach(function (account) {
                var parent = "YES";
                if (account['Parent Account'] !== undefined)
                    parent = account['Parent Account'];

                Accounts.push({ Id: i, Code: account['Account Code'], Name: account["Account Name"], ParentAccount: parent });
                i++;
            });
        }

        function IsCurrentParent(childAccount, parentAccount) {
            var res = false;

            Accounts.forEach(function (account) {
                if (account.Code == childAccount && account.ParentAccount == parentAccount) {
                    res = true;
                }
            });

            return res;
        }

        function SetCostings(outputObj) {

            $("#costings").find('tbody').empty();
            var rows = [];
            var activeRows = [];
            var disabledRows = [];

            Accounts.forEach(function (account) {

                if (account["ParentAccount"] === "YES") {

                    var Costed = 0;
                    var index = 0;

                    outputObj['Import_1'].forEach(function (imp) {
                        index++;

                        if (index > 17) {
                            if (IsCurrentParent(imp.Field, account["Code"])) {
                                Costed += parseInt(imp.Value);
                            }
                        }

                    });

                    if (Costed == 0) {
                        //disabledRows.push({ Title: account["Name"], Costed: 0, Incurred: 0 });
                        disabledRows.push({ Title: account["Name"], Costed: '0', Incurred: '0' });

                    } else {
                        //activeRows.push({ Title: account["Name"], Costed: Costed, Incurred: parseInt(Costed * (70 / 100)) });
                        activeRows.push({ Title: account["Name"], Costed: Costed.toString(), Incurred: (Costed * (70 / 100)).toString() });

                    }
                }

            });

            //merge two arrays
            var costingData = activeRows.concat(disabledRows);

            insertDataToCostingsList(projectId, costingData);

        }

    </script>

    <!-- Charts modal -->
    <div id="charts-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog chart-modal">
            <div class="modal-content no-border-radius">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-sm-12">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Sample Charts</h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-12">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1">
                                        Project Schedule Monitoring (project specific)
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                        Cost Budget vs Money Received (for business)
                                    </label>
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-12 margin-top20">
                            <div class="col-sm-12 margin-top10">
                                <%--<img alt="chart1" id="chartImg" src="../Images/chart1.jpg" class="img-responsive img-chart" />--%>

                                <div class="panel panel-default" >
                                    <div class="panel-body">
                                        <div id="chartContainer" style="width:100%;height:450px"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <script type="text/javascript">

                            function drawProjectScheduleMonitoringChart() {

                                ProjectScheduleMonitoringEstimates = [];
                                ProjectScheduleMonitoringActuals = [];

                                if (projectId > 0) {

                                    projectScheduleMonitoringData(Q, projectId).then(function (milestoneData) {

                                        var _value = 0;

                                        milestoneData.forEach(function (milestone) {
                                            _value++;

                                            ProjectScheduleMonitoringEstimates.push({ x: new Date(new Date(milestone['DateForClaim']).getFullYear(), new Date(milestone['DateForClaim']).getMonth(), new Date(milestone['DateForClaim']).getDate()), y: _value });
                                            ProjectScheduleMonitoringActuals.push({ x: new Date(new Date(milestone['claimActualDate']).getFullYear(), new Date(milestone['claimActualDate']).getMonth(), new Date(milestone['claimActualDate']).getDate()), y: _value });
                                        });

                                        showProjectScheduleMonitoringChart();

                                    });

                                } else {

                                    showProjectScheduleMonitoringChart();
                                    alert("Please select a project");
                                }

                            }

                            function showProjectScheduleMonitoringChart() {

                                chart = new CanvasJS.Chart("chartContainer",
                                          {
                                              theme: "theme2",
                                              height: 450, //in pixels
                                              width: 750,
                                              title: {
                                                  text: "Project Schedule Monitoring"
                                              },
                                              toolTip: {
                                                  shared: true,
                                              },
                                              animationEnabled: true,
                                              axisX: {
                                                  valueFormatString: "DD-MMM",
                                                  interval: 1,
                                                  intervalType: "month",
                                                  labelAngle: 50,
                                                  includeZero: true,

                                              },
                                              axisY: {
                                                  includeZero: false,
                                                  valueFormatString: "Milestone #",
                                                  interval: 1,
                                              },
                                              data: [
                                               {
                                                   type: "line",
                                                   lineThickness: 3,
                                                   name: "Estimates",
                                                   color: "#17A611",
                                                   dataPoints: ProjectScheduleMonitoringEstimates

                                               }, {
                                                   type: "line",
                                                   lineThickness: 3,
                                                   name: "Actuals",
                                                   color: "#0072c6",
                                                   dataPoints: ProjectScheduleMonitoringActuals
                                               }

                                              ]
                                          });

                                chart.render();
                                $('.canvasjs-chart-credit').css("display", "none");
                            }

                            function drawCostBudgetVsMoneyReceivedChart() {

                                chart = new CanvasJS.Chart("chartContainer",
                                 {
                                     theme: "theme2",
                                     height: 450, //in pixels
                                     width: 750,
                                     title: {
                                         text: "Cost Budget vs Money Received"
                                     },
                                     toolTip: {
                                         shared: true,
                                         //toolTipContent: "x:{x}, y: M{y}"
                                     },
                                     animationEnabled: true,
                                     axisX: {
                                         valueFormatString: "MMM",
                                         interval: 1,
                                         intervalType: "month",
                                         labelAngle: 50,

                                     },
                                     axisY: {
                                         includeZero: true,
                                         valueFormatString: "$#K",
                                     },
                                     data: [
                                         {
                                             type: "line",
                                             lineThickness: 3,
                                             name: "Cost Budget",
                                             color: "rgb(38,128,0)",
                                             dataPoints: CostBudgetVsMoneyReceivedDollars

                                         }, {
                                             type: "area",
                                             lineThickness: 3,
                                             name: "Cumulative Milestone Claims",
                                             color: "rgb(110,190,220)",
                                             dataPoints: CumulativeMilestoneClaims

                                         }, {
                                             type: "area",
                                             lineThickness: 3,
                                             name: "Cumulative Gross Margin",
                                             color: "rgb(0,0,0)",
                                             dataPoints: CumulativeGrossMargin

                                         },

                                     ]
                                 });

                                chart.render();
                                $('.canvasjs-chart-credit').css("display", "none");
                              
                            }

                            $(document).ready(function () {

                                $("#optionsRadios1").click(function () {

                                    drawProjectScheduleMonitoringChart();

                                });

                                $("#optionsRadios2").click(function () {
                                    
                                    drawCostBudgetVsMoneyReceivedChart();

                                });

                            });

                        </script>

                        <script type="text/javascript">

                            var DefaultProjectScheduleMonitoring = [{ x: new Date(2015, 06, 1) }, { x: new Date(2015, 07, 1) }, { x: new Date(2015, 08, 1) }, { x: new Date(2015, 09, 1) },
                                { x: new Date(2015, 10, 1) }, { x: new Date(2015, 11, 1) }, { x: new Date(2015, 12, 1) }, { x: new Date(2015, 13, 1) },
                                {x: new Date(2015, 14, 1)}, {x: new Date(2015, 15, 1)}, {x: new Date(2015, 16, 1)}, {x: new Date(2015, 17, 1)}]

                            window.onload = function () {

                                var chart = new CanvasJS.Chart("chartContainer",
                                {
                                    theme: "theme2",
                                    height: 450, //in pixels
                                    width: 750,
                                    title: {
                                        text: "Project Schedule Monitoring"
                                    },
                                    toolTip: {
                                        shared: true,
                                        //toolTipContent: "x:{x}, y: M{y}"
                                    },
                                    animationEnabled: true,
                                    axisX: {
                                        valueFormatString: "MMM",
                                        interval: 1,
                                        intervalType: "month",
                                        labelAngle: 50,

                                    },
                                    axisY: {
                                        includeZero: true,
                                        valueFormatString: "$#K",
                                    },
                                    data: [
                                    /*{
                                        type: "line",
                                        lineThickness: 3,
                                        name: "Default",
                                        color: "#fff",//#0072c6
                                        dataPoints: DefaultProjectScheduleMonitoring

                                    },{
                                        type: "line",
                                        lineThickness: 3,
                                        name: "Estimates",
                                        color: "#17A611",//#17A611#0072c6
                                        dataPoints: ProjectScheduleMonitoringEstimates

                                    },*/ {
                                        type: "line",
                                        lineThickness: 3,
                                        name: "Actuals",
                                        //cursor: "pointer",
                                        color: "#0072c6",//#0072c6
                                        dataPoints: CostBudgetVsMoneyReceivedDollars
                                        /*dataPoints: [

                                       {
                                           x: new Date(2015, 06, 1),
                                           y: 0,
                                       },
                                           {
                                               x: new Date(2015, 07, 1),
                                           }
                                           , {
                                               x: new Date(2015, 08, 1),

                                           }, {
                                               x: new Date(2015, 09, 1),

                                           }, {
                                               x: new Date(2015, 10, 1),

                                           }, {
                                               x: new Date(2015, 11, 1),

                                           }, {
                                               x: new Date(2015, 12, 1),

                                           }, {
                                               x: new Date(2015, 13, 1),

                                           }, {
                                               x: new Date(2015, 13, 1),

                                           }, {
                                               x: new Date(2015, 14, 1),

                                           }, {
                                               x: new Date(2015, 15, 1),

                                           }, {
                                               x: new Date(2015, 16, 1),
                                               
                                           }, {
                                               x: new Date(2015, 17, 1),
                                               y: 45000,

                                           },

                                        ]*/
                                    }

                                    ]
                                });

                                chart.render();
                                $('.canvasjs-chart-credit').css("display", "none");
                            }
                        </script>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- Charts modal -->

    <!-- Link File modal -->
    <div id="link-dialogBox"></div>
    <input type="hidden" value="" id="SelectedLinkedUrl" />
    
    <!-- Link File modal -->

</asp:Content>
