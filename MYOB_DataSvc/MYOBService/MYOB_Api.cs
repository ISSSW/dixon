﻿using System;
using System.Linq;
using MYOB.AccountRight.SDK;
using MYOB.AccountRight.SDK.Contracts;
using MYOB.AccountRight.SDK.Services;
using MYOBService.Helpers;
using MYOB.AccountRight.SDK.Services.GeneralLedger;
using MYOB.AccountRight.SDK.Contracts.Version2.GeneralLedger;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web.Hosting;
using Microsoft.WindowsAzure.ServiceRuntime;

namespace MYOBService
{
    public class MYOB_Api
    {
        private const string LocalApiUrl = "http://localhost:8080/accountright";
        private const string DeveloperKey = "2p9jsgqnauqyfscqaj89pexa";
        private const string DeveloperSecret = "sXGcS4tbNxnFpuBVh2Gfm87v";

        private const bool UseCloud = true;
        private IApiConfiguration _configurationCloud;
        private IApiConfiguration _configurationLocal;

        private IOAuthKeyService _oAuthKeyService;
        private OAuthTokens _OAuthTokens;

        private ICompanyFileCredentials companyFileCredential;

        public MYOB_Api()
        {
            companyFileCredential = new CompanyFileCredentials("Russell", "Russell");
        }

        public OAuthTokens Authentication(string authorizationCode)
        {
            try
            {
                //If developer key  enable (see above) and set useCVloud to true the following section manages OAuth token and accessing cloud service 
                _configurationCloud = new ApiConfiguration(DeveloperKey, DeveloperSecret, "http://dixonapp.azurewebsites.net/MYOB_Auth.html");//http://dixonapp.azurewebsites.net/
                _oAuthKeyService = new OAuthKeyService();

                //Get tokens if not stored
                if (_oAuthKeyService.OAuthResponse == null)
                {
                    var oauthService = new OAuthService(_configurationCloud);

                    _oAuthKeyService.OAuthResponse =
                       oauthService.GetTokens(authorizationCode);
                }

                return _oAuthKeyService.OAuthResponse;

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool ProjectCodeExist(string path, string authorizationCode, string projectCode, out string msg, out List<JournalTransaction> journalTransactions)
        {
            bool _res = false;
            msg = "DoesNotFound";
            journalTransactions = null;

            try
            {
                //If developer key  enable (see above) and set useCVloud to true the following section manages OAuth token and accessing cloud service 
                _configurationCloud = new ApiConfiguration(DeveloperKey, DeveloperSecret, path);//"https://spmysolution-c3c6b35d9e12f8.sharepoint.com/sites/dixon/Dashboard/Pages/NewProject.aspx"
                _oAuthKeyService = new OAuthKeyService();

                //Get tokens if not stored
                if (_oAuthKeyService.OAuthResponse == null)
                {
                    var oauthService = new OAuthService(_configurationCloud);
                    this._OAuthTokens = oauthService.GetTokens(authorizationCode);

                    _oAuthKeyService.OAuthResponse = this._OAuthTokens;

                    SetOAuthTokens(this._OAuthTokens, path);

                    //oauthService.GetTokens(authorizationCode);
                    //this._OAuthTokens = oauthService.RenewTokens(this._OAuthTokens);
                }
                else
                    SetOAuthTokens(_oAuthKeyService.OAuthResponse, path);

                var cfsCloud = new CompanyFileService(_configurationCloud, null, _oAuthKeyService);
                var companyFiles = cfsCloud.GetRange();

                if (companyFiles != null && companyFiles.Count() > 0)
                {
                    //Company file credential here
                    //ICompanyFileCredentials companyFileCredential = new CompanyFileCredentials("Russell", "Russell");

                    var jobSvc = new JobService(_configurationCloud, null,
                                               _oAuthKeyService);

                    var jobs = jobSvc.GetRange(companyFiles[0], "$filter=IsHeader eq false", companyFileCredential);

                    if (jobs != null)
                    {
                        foreach (var job in jobs.Items)
                        {
                            if (job.Number == projectCode)
                            {
                                _res = true;
                                msg = "OK";

                                var journalTransactionSvc = new JournalTransactionService(_configurationCloud, null,
                                                   _oAuthKeyService);

                                var __journalTransactions = from _jt in journalTransactionSvc.GetRange(companyFiles[0], "?$filter=Lines/any(x: x/Job/Number eq '" + projectCode + "')", companyFileCredential).Items
                                                            where _jt.JournalType == JournalType.Purchase
                                                            select _jt;

                                if (__journalTransactions != null)
                                    journalTransactions = __journalTransactions.ToList();

                                break;
                            }

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                msg = ex.Message;
                return false;
            }

            return _res;
        }

        public List<Account> GetAccountsData(string authorizationCode, out string msg)
        {
            List<Account> _res = null;
            msg = "DoesNotFound";

            try
            {
                //If developer key  enable (see above) and set useCVloud to true the following section manages OAuth token and accessing cloud service 
                _configurationCloud = new ApiConfiguration(DeveloperKey, DeveloperSecret, "https://rukshan-f1cbcd86dc77f5.sharepoint.com/sites/Dashboard/Dashboard/Pages/NewProject.aspx");
                _oAuthKeyService = new OAuthKeyService();

                //Get tokens if not stored
                if (_oAuthKeyService.OAuthResponse == null)
                {
                    var oauthService = new OAuthService(_configurationCloud);

                    _oAuthKeyService.OAuthResponse =
                       oauthService.GetTokens(authorizationCode);

                    //var acc = oauthService.GetTokens(authorizationCode);

                }

                var cfsCloud = new CompanyFileService(_configurationCloud, null, _oAuthKeyService);
                var companyFiles = cfsCloud.GetRange();

                if (companyFiles != null && companyFiles.Count() > 0)
                {
                    //Company file credential here
                    //ICompanyFileCredentials companyFileCredential = new CompanyFileCredentials("Russell", "Russell");

                    var accSvc = new AccountService(_configurationCloud, null,
                                               _oAuthKeyService);

                    _res = accSvc.GetRange(companyFiles[0], null, companyFileCredential).Items.ToList().FindAll(f => f.Type == AccountType.CostOfSales).ToList<Account>();
                }

            }
            catch (Exception ex)
            {
                msg = ex.Message;
                return null;
            }

            return _res;
        }

        public List<JournalTransaction> GetJournalTransactions(GetJournalTransactions_Input inputData, out string msg)
        {
            msg = "DoesNotFound";
            List<JournalTransaction> journalTransactions =
                new List<JournalTransaction>();

            try
            {
                string _path = "";
                var _SavedOAuthTokens = GetOAuthTokens(out _path);

                //If developer key  enable (see above) and set useCVloud to true the following section manages OAuth token and accessing cloud service 
                _configurationCloud = new ApiConfiguration(DeveloperKey, DeveloperSecret, _path);
                _oAuthKeyService = new OAuthKeyService();

                //Get tokens if not stored
                if (_oAuthKeyService.OAuthResponse == null)
                {
                    var oauthService = new OAuthService(_configurationCloud);
                    this._OAuthTokens = oauthService.RenewTokens(_SavedOAuthTokens);

                    _oAuthKeyService.OAuthResponse = this._OAuthTokens;

                }

                var cfsCloud = new CompanyFileService(_configurationCloud, null, _oAuthKeyService);
                var companyFiles = cfsCloud.GetRange();

                if (companyFiles != null && companyFiles.Count() > 0)
                {

                    foreach (var projectCode in inputData.ProjectCodes)
                    {

                        var journalTransactionSvc = new JournalTransactionService(_configurationCloud, null,
                                           _oAuthKeyService);

                        var __journalTransactions = from _jt in journalTransactionSvc.GetRange(companyFiles[0], "?$filter=Lines/any(x: x/Job/Number eq '" + projectCode.Code + "')", companyFileCredential).Items
                                                    where _jt.JournalType == JournalType.Purchase
                                                    select _jt;

                        if (__journalTransactions != null)
                            journalTransactions.AddRange(__journalTransactions.ToList());
                    }

                    msg = "OK";
                }

            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }

            return journalTransactions;
        }

        public void SetOAuthTokens(OAuthTokens oAuthTokens, string path)
        {

            string _xmlStr = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                            "<OAuthTokens>" +
                              "<oAuthToken AccessToken=\"" + oAuthTokens.AccessToken + "\" ExpiresIn=\"" + oAuthTokens.ExpiresIn + "\" HasExpired=\"flse\" ReceivedTime=\"" + oAuthTokens.ReceivedTime.ToString() + "\" RefreshToken=\"" + oAuthTokens.RefreshToken + "\" Scope=\"" + oAuthTokens.Scope + "\" TokenType=\"" + oAuthTokens.TokenType + "\" Path=\"" + path + "\" />" +
                            "</OAuthTokens>";

            string cachePath = RoleEnvironment.GetLocalResource("OAuthTokens").RootPath;
            string _path = Path.Combine(cachePath, "OAuthTokens.xml");

            System.IO.File.WriteAllText(_path, _xmlStr);

        }

        public OAuthTokens GetOAuthTokens(out string path)
        {
            path = "";
            OAuthTokens tokens =
                new OAuthTokens();

            try
            {
                string cachePath = RoleEnvironment.GetLocalResource("OAuthTokens").RootPath;
                string _path = Path.Combine(cachePath, "OAuthTokens.xml");

                string text = System.IO.File.ReadAllText(_path);

                if (System.IO.File.Exists(_path))
                {
                    XDocument doc = XDocument.Load(_path);

                    var _OAuthTokens = doc.Elements("OAuthTokens").ToList();

                    if (_OAuthTokens != null && _OAuthTokens.Count > 0)
                    {
                        if (_OAuthTokens[0].Elements() != null && _OAuthTokens[0].Elements().Count() > 0)
                        {
                            tokens.AccessToken = _OAuthTokens[0].Elements().ToList()[0].Attribute("AccessToken").Value;
                            tokens.ExpiresIn = int.Parse(_OAuthTokens[0].Elements().ToList()[0].Attribute("ExpiresIn").Value);
                            tokens.ReceivedTime = DateTime.Parse(_OAuthTokens[0].Elements().ToList()[0].Attribute("ReceivedTime").Value);
                            tokens.RefreshToken = _OAuthTokens[0].Elements().ToList()[0].Attribute("RefreshToken").Value;
                            tokens.Scope = _OAuthTokens[0].Elements().ToList()[0].Attribute("Scope").Value;
                            tokens.TokenType = _OAuthTokens[0].Elements().ToList()[0].Attribute("TokenType").Value;

                            path = _OAuthTokens[0].Elements().ToList()[0].Attribute("Path").Value;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                path = ex.Message;
            }

            return tokens;
        }
    }
}
