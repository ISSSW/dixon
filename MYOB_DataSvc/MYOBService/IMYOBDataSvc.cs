﻿using MYOB.AccountRight.SDK.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace MYOBService
{
    
    [ServiceContract]
    public interface IMYOBDataSvc
    {
        [OperationContract]
        GetAccessToken_OutPut GetAccessToken(GetAccessToken_Input input);

        [OperationContract]
        ProjectCodeExist_OutPut ProjectCodeExist(ProjectCodeExist_Input input);

        [OperationContract]
        string GetData(string input);

        [OperationContract]
        GetMilestoneCostIncurred_OutPut GetMilestoneCostIncurred(GetMilestoneCostIncurred_Input input);

    }

    public class ProjectCodeExist_Input
    {
        public string AuthorizationCode { get; set; }
        public string ProjectCode { get; set; }
        public string Path { get; set; }
    }

    public class ProjectCodeExist_OutPut
    {
        public string Msg { get; set; }
        public bool IsValidated { get; set; }
        public List<Accounts_OutPut> Accounts { get; set; }
    }

    public class GetAccessToken_Input
    {
        public string Code { get; set; }
    }

    public class GetAccessToken_OutPut
    {
        public string Msg { get; set; }
        public OAuthTokens OAuthTokens { get; set; }
    }

    public class GetCostings_OutPut
    {
        public string Msg { get; set; }
        public int ProjectId { get; set; }
        public string Description { get; set; }
        public string Costed { get; set; }
        public string Incurred { get; set; }
    }

    public class GetNewProjectDetails_Input
    {
        public OAuthTokens OAuthTokens { get; set; }

        public string Code { get; set; }
    }

    public class GetNewProjectDetails_OutPut
    {
        public string Msg { get; set; }
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
    }

    public class Accounts_OutPut
    {
        public string DisplayID { get; set; }
        public decimal Amount { get; set; }
    }

    public class GetJournalTransactions_Input
    {
        public string Path { get; set; }

        public List<GetAccessToken_Input> ProjectCodes { get; set; }
    }

    public class GetMilestoneCostIncurred_Input
    {
        public string ProjectCode { get; set; }
        public List<string> Milestones { get; set; }
    }

    public class GetMilestoneCostIncurred_OutPut
    {
        public string ProjectCode { get; set; }
        public List<MilestoneCostIncurred> MilestoneCostIncurreds { get; set; }
    }

    public class MilestoneCostIncurred
    {
        public string Mileston { get; set; }
        public decimal Amount { get; set; }
    }

    
}
