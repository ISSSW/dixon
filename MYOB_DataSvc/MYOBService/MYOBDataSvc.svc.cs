﻿using Microsoft.WindowsAzure.ServiceRuntime;
using MYOB.AccountRight.SDK.Contracts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web.Hosting;

namespace MYOBService
{
    public class MYOBDataSvc : IMYOBDataSvc
    {
        private MYOB_Api api = new MYOB_Api();

        [WebInvoke(Method = "POST",
                    ResponseFormat = WebMessageFormat.Json,
                    RequestFormat = WebMessageFormat.Json,
                    UriTemplate = "GetAccessToken")]
        public GetAccessToken_OutPut GetAccessToken(GetAccessToken_Input input)
        {
            OAuthTokens _OAuthTokens = api.Authentication(input.Code);

            if (_OAuthTokens != null)
            {
                return new GetAccessToken_OutPut() { Msg = "OK", OAuthTokens = _OAuthTokens };
            }

            return new GetAccessToken_OutPut() { Msg = "Error" };
        }

        [WebInvoke(Method = "POST",
                   ResponseFormat = WebMessageFormat.Json,
                   RequestFormat = WebMessageFormat.Json,
                   UriTemplate = "ProjectCodeExist")]
        public ProjectCodeExist_OutPut ProjectCodeExist(ProjectCodeExist_Input input)
        {
            ProjectCodeExist_OutPut outPut =
                new ProjectCodeExist_OutPut();

            string _msg = "";
            List<MYOB.AccountRight.SDK.Contracts.Version2.GeneralLedger.JournalTransaction> _journalTransactions = null;

            outPut.IsValidated = api.ProjectCodeExist(input.Path, input.AuthorizationCode, input.ProjectCode, out _msg, out _journalTransactions);
            outPut.Msg = _msg;

            #region JournalTransactions
            //============ Check JournalTransactions ===============
            List<Accounts_OutPut> trnsList =
                new List<Accounts_OutPut>();

            if (_journalTransactions != null)
            {
                foreach (var trns in _journalTransactions)
                    foreach (var line in trns.Lines)
                        if (line.Job != null && line.Account != null)
                            trnsList.Add(new Accounts_OutPut() { Amount = line.Amount, DisplayID = line.Account.DisplayID });
            }

            if (trnsList != null && trnsList.Count > 0)
            {
                outPut.Accounts = (trnsList.GroupBy(s => new { s.DisplayID }).Select(g => new Accounts_OutPut()
                                          {
                                              DisplayID = g.Key.DisplayID,
                                              Amount = g.Sum(x => Math.Round(Convert.ToDecimal(x.Amount), 2))
                                          }
                                   )).ToList();
            }

            //========================================================
            #endregion

            return outPut;
        }

         [WebInvoke(Method = "POST",
                   ResponseFormat = WebMessageFormat.Json,
                   RequestFormat = WebMessageFormat.Json,
                   UriTemplate = "GetMilestoneCostIncurred")]
        public GetMilestoneCostIncurred_OutPut GetMilestoneCostIncurred(GetMilestoneCostIncurred_Input input)
        {
            GetMilestoneCostIncurred_OutPut outPut =
                new GetMilestoneCostIncurred_OutPut();

            outPut.MilestoneCostIncurreds = new List<MilestoneCostIncurred>();

            try
            {
                if (input.Milestones != null && input.Milestones.Count > 0)
                {
                    foreach (var item in input.Milestones)
                    {
                        outPut.MilestoneCostIncurreds.Add(new MilestoneCostIncurred()
                        {
                            Mileston = item,
                            Amount = 0
                        });
                    }

                    string _msg = "";

                    var jtIn = new GetJournalTransactions_Input();
                    jtIn.ProjectCodes = new List<GetAccessToken_Input>();
                    jtIn.ProjectCodes.Add(new GetAccessToken_Input() { Code = input.ProjectCode });

                    List<MYOB.AccountRight.SDK.Contracts.Version2.GeneralLedger.JournalTransaction> journalTransactions__ = api.GetJournalTransactions(jtIn, out _msg);

                    foreach (var item in journalTransactions__)
                    {
                        if (item.Lines != null)
                        {
                            foreach (var line in item.Lines)
                            {
                                if (line.Job != null && line.Job.Number == input.ProjectCode && line.Account != null)
                                {
                                    if (line.Account.DisplayID.StartsWith("5-") && !string.IsNullOrEmpty(line.LineDescription) && line.Amount != 0)
                                    {
                                        var milestone = outPut.MilestoneCostIncurreds.Find(m => line.LineDescription.EndsWith(m.Mileston));

                                        if (milestone != null)
                                            milestone.Amount += line.Amount;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                outPut.MilestoneCostIncurreds.Add(new MilestoneCostIncurred()
                {
                    Mileston = ex.Message
                });
            }

            return outPut;
        }

        [WebInvoke(Method = "GET",
                   ResponseFormat = WebMessageFormat.Json,
                   RequestFormat = WebMessageFormat.Json,
                   UriTemplate = "GetData/{input}")]
        public string GetData(string input)
        {
            try
            {
                string outPut = "";
                GetJournalTransactions_Input obj =
                    new GetJournalTransactions_Input();

                obj.ProjectCodes = new List<GetAccessToken_Input>();
                obj.ProjectCodes.Add(new GetAccessToken_Input() { Code = input });

                //var items = api.GetJournalTransactions(obj, out outPut);
                string _pth = "";
                //var obk = api.GetOAuthTokens(out _pth);

                /* var obk = new OAuthTokens()
                 {
                     AccessToken = "AccessToken" + input,
                     ExpiresIn = int.Parse("10"),
                     ReceivedTime = DateTime.Now,
                     RefreshToken = "RefreshToken",
                     Scope = "Scope",
                     TokenType = "TokenType"
                 };

                 api.SetOAuthTokens(obk, input);*/

                var obk = api.GetOAuthTokens(out _pth);

                string cachePath = RoleEnvironment.GetLocalResource("OAuthTokens").RootPath;
                string filePath = Path.Combine(cachePath, "OAuthTokens.xml");

                /*if (items != null && items.Count > 0)
                {
                    string _out = "";

                    foreach (var item in items[0].Lines.ToList())
                        _out += "Account " + item.Account.DisplayID + " Amount" + item.Amount.ToString() + Environment.NewLine;

                    return _out += "~ Ref:" + filePath + "~" + cachePath + "~" + File.Exists(filePath);// obk.RefreshToken;
                }*/

                return "You Entered : " + input + "~ AccessToken:" + obk.AccessToken;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
