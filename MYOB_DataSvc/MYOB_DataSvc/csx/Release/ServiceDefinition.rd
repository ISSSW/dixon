﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="MYOB_DataSvc" generation="1" functional="0" release="0" Id="54dcd560-2583-4ca3-9abc-152fb62c6ebe" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="MYOB_DataSvcGroup" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="MYOBService:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/MYOB_DataSvc/MYOB_DataSvcGroup/LB:MYOBService:Endpoint1" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="MYOBService:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/MYOB_DataSvc/MYOB_DataSvcGroup/MapMYOBService:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="MYOBServiceInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/MYOB_DataSvc/MYOB_DataSvcGroup/MapMYOBServiceInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:MYOBService:Endpoint1">
          <toPorts>
            <inPortMoniker name="/MYOB_DataSvc/MYOB_DataSvcGroup/MYOBService/Endpoint1" />
          </toPorts>
        </lBChannel>
      </channels>
      <maps>
        <map name="MapMYOBService:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/MYOB_DataSvc/MYOB_DataSvcGroup/MYOBService/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapMYOBServiceInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/MYOB_DataSvc/MYOB_DataSvcGroup/MYOBServiceInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="MYOBService" generation="1" functional="0" release="0" software="D:\DEVELOPMENT\Insight\Dixon\MYOB_DataSvc\MYOB_DataSvc\csx\Release\roles\MYOBService" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="-1" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="80" />
            </componentports>
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;MYOBService&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;MYOBService&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="MYOBService.svclog" defaultAmount="[1000,1000,1000]" defaultSticky="true" kind="Directory" />
              <resourceReference name="OAuthTokens" defaultAmount="[10,10,10]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/MYOB_DataSvc/MYOB_DataSvcGroup/MYOBServiceInstances" />
            <sCSPolicyUpdateDomainMoniker name="/MYOB_DataSvc/MYOB_DataSvcGroup/MYOBServiceUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/MYOB_DataSvc/MYOB_DataSvcGroup/MYOBServiceFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="MYOBServiceUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="MYOBServiceFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="MYOBServiceInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="7cfb72af-1263-4460-9f40-369e71f13790" ref="Microsoft.RedDog.Contract\ServiceContract\MYOB_DataSvcContract@ServiceDefinition">
      <interfacereferences>
        <interfaceReference Id="39ce577f-0121-42cf-843d-9ade7d7c64b5" ref="Microsoft.RedDog.Contract\Interface\MYOBService:Endpoint1@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/MYOB_DataSvc/MYOB_DataSvcGroup/MYOBService:Endpoint1" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>