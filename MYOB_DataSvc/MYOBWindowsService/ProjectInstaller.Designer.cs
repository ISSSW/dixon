﻿namespace MYOBWindowsService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.myobServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.myobServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // myobServiceProcessInstaller
            // 
            this.myobServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.myobServiceProcessInstaller.Password = null;
            this.myobServiceProcessInstaller.Username = null;
            // 
            // myobServiceInstaller
            // 
            this.myobServiceInstaller.Description = "Dixon SP List update service";
            this.myobServiceInstaller.DisplayName = "Dixon SP List update service";
            this.myobServiceInstaller.ServiceName = "Dixon SP Service";
            this.myobServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.myobServiceProcessInstaller,
            this.myobServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller myobServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller myobServiceInstaller;
    }
}