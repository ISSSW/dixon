﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;

namespace MYOBWindowsService
{
    public partial class MYOB_DataSvc : ServiceBase
    {
        private System.Diagnostics.EventLog myobEventLog;
        JavaScriptSerializer _serializer = new JavaScriptSerializer();

        int eventId = 0;

        public MYOB_DataSvc(string[] args)
        {
            InitializeComponent();

            string eventSourceName = "MyobSource";
            string logName = "MyobNewLog";

            if (args.Count() > 0)
            {
                eventSourceName = args[0];
            }

            if (args.Count() > 1)
            {
                logName = args[1];
            }

            myobEventLog = new System.Diagnostics.EventLog();

            if (!System.Diagnostics.EventLog.SourceExists(eventSourceName))
            {
                System.Diagnostics.EventLog.CreateEventSource(eventSourceName, logName);
            }

            myobEventLog.Source = eventSourceName;
            myobEventLog.Log = logName;
        }

        protected override void OnStart(string[] args)
        {
            myobEventLog.WriteEntry("Service Start @ " + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));

            // Set up a timer to trigger every minute.
            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = 60000; // 60 seconds
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            timer.Start();

            // Update the service state to Start Pending.
            ServiceStatus serviceStatus = new ServiceStatus();
            serviceStatus.dwCurrentState = ServiceState.SERVICE_START_PENDING;
            serviceStatus.dwWaitHint = 100000;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);

            // Update the service state to Running.
            serviceStatus.dwCurrentState = ServiceState.SERVICE_RUNNING;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);
        }

        public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            // TODO: Insert monitoring activities here.

            /*try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://dixonmyobdataservice.cloudapp.net/MYOBDataSvc.svc/UpdateSpLists");
                httpWebRequest.ContentType = "text/json";
                httpWebRequest.Method = "POST";

                List<Project> projects =
                    new List<Project>();

                projects.Add(new Project() { Code = "1900" });

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json =  _serializer.Serialize(projects);

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    //OutPut OutPut = serializer.Deserialize<OutPut>(result.ToString());

                    myobEventLog.WriteEntry("Updated SP Lists @ " + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"), EventLogEntryType.Information, eventId++);
                }
            }
            catch
            {
                myobEventLog.WriteEntry("Error Occured In SP Lists Updated @ " + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"), EventLogEntryType.Information, eventId++);
            }*/

            myobEventLog.WriteEntry("Updated SP Lists @ " + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"), EventLogEntryType.Information, eventId++);
        }

        protected override void OnStop()
        {
            myobEventLog.WriteEntry("Service Stop @ " + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
        }

        public enum ServiceState
        {
            SERVICE_STOPPED = 0x00000001,
            SERVICE_START_PENDING = 0x00000002,
            SERVICE_STOP_PENDING = 0x00000003,
            SERVICE_RUNNING = 0x00000004,
            SERVICE_CONTINUE_PENDING = 0x00000005,
            SERVICE_PAUSE_PENDING = 0x00000006,
            SERVICE_PAUSED = 0x00000007,
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct ServiceStatus
        {
            public long dwServiceType;
            public ServiceState dwCurrentState;
            public long dwControlsAccepted;
            public long dwWin32ExitCode;
            public long dwServiceSpecificExitCode;
            public long dwCheckPoint;
            public long dwWaitHint;
        };

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool SetServiceStatus(IntPtr handle, ref ServiceStatus serviceStatus);
    }
}
