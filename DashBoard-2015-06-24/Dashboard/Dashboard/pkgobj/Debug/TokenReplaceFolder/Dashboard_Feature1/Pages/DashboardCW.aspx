﻿<%@ Page language="C#" Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<WebPartPages:AllowFraming ID="AllowFraming" runat="server" />

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title></title>

    <script type="text/javascript" src="../Scripts/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="/_layouts/15/MicrosoftAjax.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.runtime.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.js"></script>

     <!-- Bootstrap Core CSS -->
    <link href="../Content/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Sidebar CSS -->
    <link href="../Content/css/sidebar.css" rel="stylesheet" />

    <!-- Custom CSS -->
    <link href="../Content/css/style.css" rel="stylesheet" />

    <!-- Slider CSS -->
    <link rel="stylesheet" href="../Content/css/slider-style.css" type="text/css" media="all" />
    <link rel="stylesheet" href="../Content/css/jquery-ui.css" type="text/css" media="all" />

    <!-- jQuery -->
    <script type="text/javascript" src="../Scripts/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script type="text/javascript" src="../Scripts/bootstrap.min.js"></script>

    <!-- Slider JS files -->
   <script type="text/javascript" src="../Scripts/jquery-ui.js"></script>
   <script type="text/javascript" src="../Scripts/slider.js"></script>
    <!-- End Dashboard references-->


    <!---------------New References ---------------------------->
    <script type="text/javascript" src="../Scripts/dist/cpexcel.js"></script>
    <script type="text/javascript" src="../Scripts/shim.js"></script>
    <script type="text/javascript" src="../Scripts/jszip.js"></script>
    <script type="text/javascript" src="../Scripts/xlsx.js"></script>
    <script type="text/javascript" src="../Scripts/dist/ods.js"></script>
    <script type="text/javascript" src="../Scripts/jquery1.min.js"></script>
    <script type="text/javascript" src="../Scripts/kendo.all.min.js"></script>
 
    <!-- Kendo customize css -->
    <link href="../Content/kendo-customize.css" rel="stylesheet" />
    <link href="../Content/kendo.common.min.css" rel="stylesheet" />
    <link href="../Content/kendo.silver.min.css" rel="stylesheet" />
    <!---------------end new references ------------------------>



    <script type="text/javascript">
        // Set the style of the app part page to be consistent with the host web.
        // Get the URL of the host web and load the styling of it.
        function setStyleSheet() {
            var hostUrl = ""
            if (document.URL.indexOf("?") != -1) {
                var params = document.URL.split("?")[1].split("&");
                for (var i = 0; i < params.length; i++) {
                    p = decodeURIComponent(params[i]);
                    if (/^SPHostUrl=/i.test(p)) {
                        hostUrl = p.split("=")[1];
                        document.write("<link rel=\"stylesheet\" href=\"" + hostUrl +
                            "/_layouts/15/defaultcss.ashx\" />");
                        break;
                    }
                }
            }
            // if no host web URL was available, load the default styling
            if (hostUrl == "") {
                document.write("<link rel=\"stylesheet\" " +
                    "href=\"/_layouts/15/1033/styles/themable/corev15.css\" />");
            }
        }
        setStyleSheet();

        $(document).ready(function () {
            //getParameterByName = function (name) {
            //    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
            //    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
            //};
            //try {
            //    var target = parent.postMessage ? parent : (parent.document.postMessage ? parent.document : undefined);
            //    target.postMessage('<message senderId=' + getParameterByName("SenderId") + '>resize(' + ($(document).width()) + ',' + ($(document).height()) + ')</message>', '*');
            //} catch (e) {
            //    alert(e);
            //}
            renderClientWebPart();

        });

        getParameterByName = function (name) {
            var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
            return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
        };

        function renderClientWebPart(){
            try {
                var target = parent.postMessage ? parent : (parent.document.postMessage ? parent.document : undefined);
            target.postMessage('<message senderId=' + getParameterByName("SenderId") + '>resize(' + ($(document).width()) + ',' + ($(document).height()) + ')</message>', '*');
            } catch (e) {
                alert(e);
            }
        }


    </script>
</head>
<body>
    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">All Projects
                </li>
                <li>
                    <a href="#">Project 123</a>
                </li>
                <li class="selected">
                    <a href="#">Project 1300</a>
                </li>
                <li>
                    <a href="#">Project 323</a>
                </li>
                <li>
                    <a href="#">Project 423</a>
                </li>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 no-padding">
                        <a href="#menu-toggle" class="btn btn-default btn-menu-toggle no-border-radius" id="menu-toggle"><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>Show/Hide project list</a>
                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <div class="row margin-top5">
                    <!-- top form -->
                    <div class="col-md-12 no-padding">
                        <div class="col-md-12 no-padding top-form-wrapper">
                            <div class="col-lg-5 col-md-6 col-sm-6 col-xs-12">
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-8 control-label no-padding-md">Estimate project value for Financial Year</label>
                                        <div class="col-sm-4">
                                            <input type="text" value="$5000000" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-8 control-label no-padding-md">Actual project value for Financial Year</label>
                                        <div class="col-sm-4">
                                            <input type="text" value="$750000" class="form-control">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-lg-5 col-md-6 col-sm-6 col-xs-12">
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-8 control-label no-padding-md">Cost Budget for Financial Year</label>
                                        <div class="col-sm-4">
                                            <input type="text" value="$650000" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-8 control-label no-padding-md">Gross Profit for Financial Year</label>
                                        <div class="col-sm-4">
                                            <input type="text" value="$120000" class="form-control">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                                <form class="form-horizontal">
                                    <div class="form-group charts-button-wrap">
                                        <div class="col-sm-12 margin-top10">
                                            <button type="submit" class="btn btn-default pull-right no-border-radius">Charts</button>
                                        </div>
                                    </div>
                                </form>
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <div class="col-sm-12 margin-top10">
                                            <button type="submit" class="btn btn-default pull-right no-border-radius">New Project</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="col-md-12 no-padding margin-top20">
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <div class="col-lg-12 no-padding padding-for-large">
                                    <h4 class="project-label margin-bottom20">Project 1300
                                        <button type="submit" class="btn btn-sm btn-default btn-export">Export</button>
                                        <button id="import" class="btn btn-sm btn-default btn-export" style="cursor: pointer">Import</button>
                                        <input type="file" name="xlfile" id="xlf" style="display: none" /></h4>
                                    <form class="form-horizontal">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-lg-7 col-sm-6 control-label no-padding-md">Total project claim value</label>
                                            <div class="col-lg-5 col-sm-6">
                                                <input type="text" value="$640000" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-lg-7 col-sm-6 control-label no-padding-md">Remaining Claim Value</label>
                                            <div class="col-lg-5 col-sm-6">
                                                <input type="text" value="$110000" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-lg-7 col-sm-6 control-label no-padding-md">Total contract value</label>
                                            <div class="col-lg-5 col-sm-6">
                                                <input type="text" value="$301609" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-lg-7 col-sm-6 control-label no-padding-md">Supervisor allocation</label>
                                            <div class="col-lg-5 col-sm-6">
                                                <input type="text" value="$20000" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-lg-7 col-sm-6 control-label no-padding-md">Supervisor cost incurred</label>
                                            <div class="col-lg-5 col-sm-6">
                                                <input type="text" value="$12000" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-lg-7 col-sm-6 control-label no-padding-md">Building margin</label>
                                            <div class="col-lg-5 col-sm-6">
                                                <input type="text" value="12.5" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-lg-7 col-sm-6 control-label no-padding-md">Insurances</label>
                                            <div class="col-lg-5 col-sm-6">
                                                <input type="text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-lg-7 col-sm-6 control-label no-padding-md">Status</label>
                                            <div class="col-lg-5 col-sm-6">
                                                <div class="dropdown pull-right width-100">
                                                    <button class="btn btn-default dropdown-toggle width-100 no-border-radius" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                                                        Open
                                               
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                                        <li role="presentation" class="text-center"><a role="menuitem" tabindex="-1" href="#">Open</a></li>
                                                        <li role="presentation" class="text-center"><a role="menuitem" tabindex="-1" href="#">Past</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <!-- Milestones -->
                            <div class="col-lg-9 col-md-6 col-sm-12 no-padding">
                                <div class="col-md-12 no-padding">
                                    <h4>Milestones
                                        <button type="submit" class="btn btn-sm btn-default btn-export">Edit</button></h4>
                                </div>

                                <!-- Tables -->
                                <div class="col-lg-12 col-md-6 no-padding sub-tables hidden-sm hidden-xs">
                                    <!-- Table 1 -->
                                    <div class="col-lg-3 col-md-12">
                                        <table class="table table-bordered table-striped table-condensed table-responsive">
                                            <thead>
                                                <tr>
                                                    <th class="table-head-th">Milestone</th>
                                                    <th class="table-head-th">One</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Date for claim</td>
                                                    <td>120/2015</td>
                                                </tr>
                                                <tr>
                                                    <td>Progress claim stage</td>
                                                    <td>$25,000</td>
                                                </tr>
                                                <tr>
                                                    <td>GST incl.</td>
                                                    <td>6%</td>
                                                </tr>
                                                <tr>
                                                    <td>Cost allocated</td>
                                                    <td>$22,000</td>
                                                </tr>
                                                <tr>
                                                    <td>Cost incurred</td>
                                                    <td>$21,000</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.End Table 1 -->

                                    <!-- Table 2 -->
                                    <div class="col-md-3 hidden-md">
                                        <table class="table table-bordered table-striped table-condensed table-responsive">
                                            <thead>
                                                <tr>
                                                    <th class="table-head-th">Milestone</th>
                                                    <th class="table-head-th">Two</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Date for claim</td>
                                                    <td>120/2015</td>
                                                </tr>
                                                <tr>
                                                    <td>Progress claim stage</td>
                                                    <td>$25,000</td>
                                                </tr>
                                                <tr>
                                                    <td>GST incl.</td>
                                                    <td>6%</td>
                                                </tr>
                                                <tr>
                                                    <td>Cost allocated</td>
                                                    <td>$22,000</td>
                                                </tr>
                                                <tr>
                                                    <td>Cost incurred</td>
                                                    <td>$21,000</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.End Table 2 -->

                                    <!-- Table 3 -->
                                    <div class="col-md-3 hidden-md">
                                        <table class="table table-bordered table-striped table-condensed table-responsive">
                                            <thead>
                                                <tr>
                                                    <th class="table-head-th">Milestone</th>
                                                    <th class="table-head-th">Three</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Date for claim</td>
                                                    <td>120/2015</td>
                                                </tr>
                                                <tr>
                                                    <td>Progress claim stage</td>
                                                    <td>$25,000</td>
                                                </tr>
                                                <tr>
                                                    <td>GST incl.</td>
                                                    <td>6%</td>
                                                </tr>
                                                <tr>
                                                    <td>Cost allocated</td>
                                                    <td>$22,000</td>
                                                </tr>
                                                <tr>
                                                    <td>Cost incurred</td>
                                                    <td>$21,000</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.End Table 3 -->

                                    <!-- Table 4 -->
                                    <div class="col-md-3 hidden-md">
                                        <table class="table table-bordered table-striped table-condensed table-responsive">
                                            <thead>
                                                <tr>
                                                    <th class="table-head-th">Milestone</th>
                                                    <th class="table-head-th">Four</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Date for claim</td>
                                                    <td>120/2015</td>
                                                </tr>
                                                <tr>
                                                    <td>Progress claim stage</td>
                                                    <td>$25,000</td>
                                                </tr>
                                                <tr>
                                                    <td>GST incl.</td>
                                                    <td>6%</td>
                                                </tr>
                                                <tr>
                                                    <td>Cost allocated</td>
                                                    <td>$22,000</td>
                                                </tr>
                                                <tr>
                                                    <td>Cost incurred</td>
                                                    <td>$21,000</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.End Table 4 -->
                                </div>
                                <!-- /.End Tables -->

                            </div>
                            <!-- /.End Milestones -->
                        </div>

                        <div class="col-md-12 no-padding margin-top20">
                            <!-- Costings table -->
                            <div class="col-lg-6 col-md-12 col-sm-12 overflow-x-scroll">
                                <h4>Costings</h4>
                                <table id="costings" class="table table-bordered table-striped table-condensed table-responsive">
                                    <thead>
                                        <tr>
                                            <th class="table-head-th">Description</th>
                                            <th class="table-head-th">Costed</th>
                                            <th class="table-head-th">Incurred</th>
                                            <th class="table-head-th">Description</th>
                                            <th class="table-head-th">Costed</th>
                                            <th class="table-head-th">Incurred</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.End Costings table -->

                            <!-- Variations table -->
                            <div class="col-lg-6 col-md-12 col-sm-12 overflow-x-scroll">
                                <h4>Variations</h4>
                                <table class="table table-bordered table-striped table-condensed table-responsive">
                                    <thead>
                                        <tr>
                                            <th class="table-head-th">Variation number</th>
                                            <th class="table-head-th">Status</th>
                                            <th class="table-head-th">Date of issue</th>
                                            <th class="table-head-th">Description</th>
                                            <th class="table-head-th">Amount</th>
                                            <th class="table-head-th">Adjusted Value</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>0162</td>
                                            <td>Approved</td>
                                            <td>05/09/2014</td>
                                            <td>lorem ipsum</td>
                                            <td>$2,100</td>
                                            <td>$103,000</td>
                                        </tr>
                                        <tr>
                                            <td>1935</td>
                                            <td>Pending</td>
                                            <td>NA</td>
                                            <td>lorem ipsum</td>
                                            <td>$1,600</td>
                                            <td>NA</td>
                                        </tr>
                                        <tr>
                                            <td>0162</td>
                                            <td>Approved</td>
                                            <td>05/09/2014</td>
                                            <td>lorem ipsum</td>
                                            <td>$2,100</td>
                                            <td>$103,000</td>
                                        </tr>
                                        <tr>
                                            <td>1935</td>
                                            <td>Pending</td>
                                            <td>NA</td>
                                            <td>lorem ipsum</td>
                                            <td>$1,600</td>
                                            <td>NA</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.End Variations table -->
                        </div>
                    </div>
                    <!-- /.end top form -->


                    <!-- right sidebar -->
                    <div class="col-md-12 no-padding right-sidebar">
                        <div class="col-md-6 col-sm-6">
                            <!-- Yammer -->
                            <table class="table table-bordered table-condensed table-responsive">
                                <thead>
                                    <tr>
                                        <th class="table-head-th">Proj 123 (Yammer)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- /.End Yammer -->
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <!-- Recent docs -->
                            <table class="table table-bordered table-striped table-condensed table-responsive">
                                <thead>
                                    <tr>
                                        <th class="table-head-th">Date</th>
                                        <th class="table-head-th">Document</th>
                                        <th class="table-head-th">Author</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>19/02/2015</td>
                                        <td>dsa-dsads</td>
                                        <td>John</td>
                                    </tr>
                                    <tr>
                                        <td>19/02/2015</td>
                                        <td>dsa-dsads</td>
                                        <td>John</td>
                                    </tr>
                                    <tr>
                                        <td>19/02/2015</td>
                                        <td>dsa-dsads</td>
                                        <td>John</td>
                                    </tr>
                                    <tr>
                                        <td>19/02/2015</td>
                                        <td>dsa-dsads</td>
                                        <td>John</td>
                                    </tr>
                                    <tr>
                                        <td>19/02/2015</td>
                                        <td>dsa-dsads</td>
                                        <td>John</td>
                                    </tr>
                                    <tr>
                                        <td>19/02/2015</td>
                                        <td>dsa-dsads</td>
                                        <td>John</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- /.End recent docs -->
                        </div>
                    </div>
                    <!-- /.end right sidebar -->
                </div>
            </div>

        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

    
</body>
            <!-- Menu Toggle Script -->
    <script type="text/javascript">
        $("#menu-toggle").click(function (e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
            //Communica.Part.adjustSize();
        });


        $("#import").click(function (e) {
            e.preventDefault();
            $("#xlf").trigger('click');
        });

        var X = XLSX;
        var XW = {
            /* worker message */
            msg: 'xlsx',
            /* worker scripts */
            rABS: './xlsxworker2.js',
            norABS: './xlsxworker1.js',
            noxfer: './xlsxworker.js'
        };

        var rABS = typeof FileReader !== "undefined" && typeof FileReader.prototype !== "undefined" && typeof FileReader.prototype.readAsBinaryString !== "undefined";
        var use_worker = typeof Worker !== 'undefined';
        var transferable = use_worker;
        var wtf_mode = false;
        var Accounts = [];

        function fixdata(data) {
            var o = "", l = 0, w = 10240;
            for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w, l * w + w)));
            o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w)));
            return o;
        }

        function ab2str(data) {
            var o = "", l = 0, w = 10240;
            for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint16Array(data.slice(l * w, l * w + w)));
            o += String.fromCharCode.apply(null, new Uint16Array(data.slice(l * w)));
            return o;
        }

        function s2ab(s) {
            var b = new ArrayBuffer(s.length * 2), v = new Uint16Array(b);
            for (var i = 0; i != s.length; ++i) v[i] = s.charCodeAt(i);
            return [v, b];
        }

        function xw_noxfer(data, cb) {
            var worker = new Worker(XW.noxfer);
            worker.onmessage = function (e) {
                switch (e.data.t) {
                    case 'ready': break;
                    case 'e': console.error(e.data.d); break;
                    case XW.msg: cb(JSON.parse(e.data.d)); break;
                }
            };
            var arr = rABS ? data : btoa(fixdata(data));
            worker.postMessage({ d: arr, b: rABS });
        }

        function xw_xfer(data, cb) {
            var worker = new Worker(rABS ? XW.rABS : XW.norABS);
            worker.onmessage = function (e) {
                switch (e.data.t) {
                    case 'ready': break;
                    case 'e': console.error(e.data.d); break;
                    default: xx = ab2str(e.data).replace(/\n/g, "\\n").replace(/\r/g, "\\r"); console.log("done"); cb(JSON.parse(xx)); break;
                }
            };
            if (rABS) {
                var val = s2ab(data);
                worker.postMessage(val[1], [val[1]]);
            } else {
                worker.postMessage(data, [data]);
            }
        }

        function xw(data, cb) {
            transferable = false;
            if (transferable) xw_xfer(data, cb);
            else xw_noxfer(data, cb);
        }

        function to_json(workbook) {
            var result = {};
            workbook.SheetNames.forEach(function (sheetName) {
                var roa = X.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                if (roa.length > 0) {
                    result[sheetName] = roa;
                }
            });
            return result;
        }

        function process_wb(wb) {

            var outputObj = to_json(wb);
            $('#xlf').val("");

            //Set Accounts Array=====================================================
            SetAccounts(outputObj['Accounts']);

            //============================ Costings =================================
            SetCostings(outputObj);

        }

        var xlf = document.getElementById('xlf');
        function handleFile(e) {
            rABS = false;
            use_worker = false;
            var files = e.target.files;
            var f = files[0];
            {
                var reader = new FileReader();
                var name = f.name;

                reader.onload = function (e) {
                    if (typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
                    var data = e.target.result;
                    if (use_worker) {
                        xw(data, process_wb);
                    } else {
                        var wb;
                        if (rABS) {
                            wb = X.read(data, { type: 'binary' });
                        } else {
                            var arr = fixdata(data);
                            wb = X.read(btoa(arr), { type: 'base64' });
                        }
                        process_wb(wb);
                    }
                };

                if (rABS) reader.readAsBinaryString(f);
                else reader.readAsArrayBuffer(f);
            }
        }

        if (xlf.addEventListener) xlf.addEventListener('change', handleFile, false);

        function SetAccounts(xflAccountsTab) {

            Accounts = [];
            var i = 0;

            xflAccountsTab.forEach(function (account) {
                var parent = "YES";
                if (account['Parent Account'] !== undefined)
                    parent = account['Parent Account'];

                Accounts.push({ Id: i, Code: account['Account Code'], Name: account["Account Name"], ParentAccount: parent });
                i++;
            });
        }

        function IsCurrentParent(childAccount, parentAccount) {
            var res = false;

            Accounts.forEach(function (account) {
                if (account.Code == childAccount && account.ParentAccount == parentAccount) {
                    res = true;
                }
            });

            return res;
        }

        function SetCostings(outputObj) {

            $("#costings").find('tbody').empty();
            var rows = [];
            var activeRows = [];
            var disabledRows = [];

            Accounts.forEach(function (account) {

                if (account["ParentAccount"] === "YES") {

                    var Costed = 0;
                    var index = 0;

                    outputObj['Import_1'].forEach(function (imp) {
                        index++;

                        if (index > 17) {
                            if (IsCurrentParent(imp.Field, account["Code"])) {
                                Costed += parseInt(imp.Value);
                            }
                        }

                    });

                    if (Costed == 0) {
                        disabledRows.push({ Description: account["Name"], Costed: 0, Incurred: 0 });

                    } else {
                        activeRows.push({ Description: account["Name"], Costed: Costed, Incurred: parseInt(Costed * (70 / 100)) });

                    }
                }

            });

            var rowid = 0;
            var tbody = '';
            var td = '';

            activeRows.forEach(function (row) {

                if (rowid % 2 == 0 && rowid != 0) {
                    tbody += '<tr>' + td + '</tr>';
                    td = '';
                }

                td += '<td>' + row["Description"] + '</td>' +
                     '<td>$' + row["Costed"] + '</td>' +
                     '<td>$' + row["Incurred"] + '</td>';

                rowid++;

            });

            td = '';
            rowid = 0;

            disabledRows.forEach(function (row) {

                if (rowid % 2 == 0 && rowid != 0) {
                    tbody += '<tr>' + td + '</tr>';
                    td = '';
                }

                td += '<td class="warning">' + row["Description"] + '</td>' +
                     '<td class="warning">$0</td>' +
                     '<td class="warning">$0</td>';
                rowid++;

            });

            $("#costings").find('tbody').append(tbody);
            renderClientWebPart();
        }
    </script>
</html>

