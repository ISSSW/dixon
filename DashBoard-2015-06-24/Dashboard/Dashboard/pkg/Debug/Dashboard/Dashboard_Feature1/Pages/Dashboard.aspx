﻿<%@ Page Language="C#" MasterPageFile="~masterurl/default.master" Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <SharePoint:ScriptLink ID="ScriptLink1" Name="sp.js" runat="server" OnDemand="true" LoadAfterUI="true" Localizable="false" />
    <WebPartPages:AllowFraming ID="AllowFraming1" runat="server" />

    <script type="text/javascript" src="../Scripts/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.runtime.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.js"></script>
    <script type="text/javascript" src="../Scripts/App.js"></script>
    <script type="text/javascript" src="../Scripts/q.js"></script>
    <!--Start Dashboard references -->

    <!-- Bootstrap Core CSS -->
    <link href="../Content/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Sidebar CSS -->
    <link href="../Content/css/sidebar.css" rel="stylesheet" />

    <!-- Custom CSS -->
    <link href="../Content/css/style.css" rel="stylesheet" />

    <!-- Slider CSS -->
    <link rel="stylesheet" href="../Content/css/slider-style.css" type="text/css" media="all" />
    <link rel="stylesheet" href="../Content/css/jquery-ui.css" type="text/css" media="all" />

    <!-- jQuery -->
    <script type="text/javascript" src="../Scripts/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script type="text/javascript" src="../Scripts/bootstrap.min.js"></script>

    <!-- Slider JS files -->
    <script type="text/javascript" src="../Scripts/jquery-ui.js"></script>
    <script type="text/javascript" src="../Scripts/slider.js"></script>
    <!-- End Dashboard references-->


    <!---------------New References ---------------------------->
    <script type="text/javascript" src="../Scripts/dist/cpexcel.js"></script>
    <script type="text/javascript" src="../Scripts/shim.js"></script>
    <script type="text/javascript" src="../Scripts/jszip.js"></script>
    <script type="text/javascript" src="../Scripts/xlsx.js"></script>
    <script type="text/javascript" src="../Scripts/dist/ods.js"></script>
    <script type="text/javascript" src="../Scripts/jquery1.min.js"></script>
    <script type="text/javascript" src="../Scripts/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Scripts/popupWindow.js"></script>

    <!-- Kendo customize css -->
    <link href="../Content/kendo-customize.css" rel="stylesheet" />
    <link href="../Content/kendo.common.min.css" rel="stylesheet" />
    <link href="../Content/kendo.silver.min.css" rel="stylesheet" />
    <link href="../Content/kendo.dataviz.min.css" rel="stylesheet" />
    <link href="../Content/kendo.dataviz.silver.min.css" rel="stylesheet" />
    <!---------------end new references ------------------------>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <WebPartPages:WebPartZone runat="server" FrameType="TitleBarOnly" ID="full" Title="loc:full" />


    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">All Projects
                </li>
                <li class="selected">
                    <a href="#">1300</a>
                </li>
                <li>
                    <a href="#">1900</a>
                </li>
                <li>
                    <a href="#">9006</a>
                </li>
            </ul>

        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 no-padding">
                        <a href="#menu-toggle" class="btn btn-default btn-menu-toggle no-border-radius" id="menu-toggle"><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>Show/Hide project list</a>
                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <div class="row margin-top5">
                    <!-- top form -->
                    <div class="col-md-12 no-padding">
                        <div class="col-md-12 no-padding top-form-wrapper">
                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-8 control-label no-padding-md">Estimate project value for Financial Year</label>
                                        <div class="col-sm-4">
                                            <!--<input type="text" id="estimateprojectvalue" value="$5000000" class="form-control">-->
                                            <input type="text" id="estimateprojectvalue" value="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-8 control-label no-padding-md">Actual project value for Financial Year</label>
                                        <div class="col-sm-4">
                                            <%--<input type="text" id="actualprojectvalue" value="$750000" class="form-control">--%>
                                            <input type="text" id="actualprojectvalue" value="" class="form-control">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-8 control-label no-padding-md">Cost Budget for Financial Year</label>
                                        <div class="col-sm-4">
                                            <%--<input type="text" id="Text1" value="$650000" class="form-control">--%>
                                            <input type="text" id="costbudget" value="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-8 control-label no-padding-md">Gross Profit for Financial Year</label>
                                        <div class="col-sm-4">
                                            <%--<input type="text" id="Text2" value="$120000" class="form-control">--%>
                                            <input type="text" id="grossprofit" value="" class="form-control">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-8 control-label no-padding-md">Total project claim value</label>
                                        <div class="col-sm-4">
                                            <%--<input type="text" id="totalprojectclaim" value="$640000" class="form-control">--%>
                                            <input type="text" id="totalprojectclaim" value="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-8 control-label no-padding-md">Remaining Claim Value</label>
                                        <div class="col-sm-4">
                                            <%--<input type="text" id="remainingclaim" value="$110000" class="form-control">--%>
                                            <input type="text" id="remainingclaim" value="" class="form-control">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group charts-button-wrap">
                                    <div class="col-sm-12 margin-top10">
                                        <button id="chart" class="btn btn-default pull-right no-border-radius" data-toggle="modal" data-target="#charts-modal">Charts</button>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12 margin-top10">
                                        <%--<button type="submit" id="insertData" class="btn btn-default pull-right no-border-radius">New Project</button>--%>
                                        <a href="https://secure.myob.com/oauth2/account/authorize?client_id=2p9jsgqnauqyfscqaj89pexa&redirect_uri=http://dixonapp.azurewebsites.net/MYOB_Auth.html&response_type=code&scope=CompanyFile" id="newProject" class="btn btn-default pull-right no-border-radius">New Project</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 no-padding margin-top20">
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <div class="col-lg-12 no-padding padding-for-large">
                                    <h4 class="project-label margin-bottom20">Project <span id="project-label">1900</span>
                                        <button type="submit" class="btn btn-sm btn-default btn-export">Export</button>
                                        <button id="import" class="btn btn-sm btn-default btn-export" style="cursor: pointer">Import</button>
                                        <input type="file" name="xlfile" id="xlf" value="Import" style="display: none" />

<%--                                        <!-- file upload -->
                                        <input id="getFile" class="form-control" type="file" /><br />
                                        <input id="displayName" class="form-control" type="text" value="" /><br />
                                        <button id="addFileButton" type="button" class="btn btn-sm btn-default btn-export">Add File</button>
                                        <!-- file upload end-->--%>

                                    </h4>
                                    <form class="form-horizontal">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-lg-7 col-sm-6 control-label no-padding-md">Total contract value</label>
                                            <div class="col-lg-5 col-sm-6">
                                                <input type="text" id="totalcontractvalue" value="$301609" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-lg-7 col-sm-6 control-label no-padding-md">Supervisor allocation</label>
                                            <div class="col-lg-5 col-sm-6">
                                                <input type="text" id="supervisorallocation" value="$20000" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-lg-7 col-sm-6 control-label no-padding-md">Supervisor cost incurred</label>
                                            <div class="col-lg-5 col-sm-6">
                                                <input type="text" id="supervisorcostincurred" value="$12000" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-lg-7 col-sm-6 control-label no-padding-md">Building margin</label>
                                            <div class="col-lg-5 col-sm-6">
                                                <input type="text" id="buildingmargin" value="12.5" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-lg-7 col-sm-6 control-label no-padding-md">Insurances</label>
                                            <div class="col-lg-5 col-sm-6">
                                                <input type="text" id="insurances" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-lg-7 col-sm-6 control-label no-padding-md">Status</label>
                                            <div class="col-lg-5 col-sm-6">
                                                <div class="dropdown pull-right width-100">
                                                    <button class="btn btn-default dropdown-toggle width-100 no-border-radius" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                                                        Open
                                               
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                                        <li role="presentation" class="text-center"><a role="menuitem" tabindex="-1" href="#">Open</a></li>
                                                        <li role="presentation" class="text-center"><a role="menuitem" tabindex="-1" href="#">Past</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <!-- Milestones -->
                            <div class="col-lg-9 col-md-6 col-sm-12 no-padding">
                                <div class="col-md-12">
                                    <h4>Milestones
                                        <%--<button type="submit" class="btn btn-sm btn-default btn-export">Edit</button>--%></h4>
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-6 col-sm-12 no-padding">
                                <!-- Tables -->
                                <div class="col-md-12 no-padding">
                                    <!-- Table 1 -->
                                    <div class="col-md-12 milestone-wrapper">
                                        <table class="table milestone-wrapper-table sub-tables">
                                            <tbody>
                                                <tr>
                                                    <td class="wrapper-table-cell">
                                                        <table class="table table-bordered table-striped table-condensed table-responsive">
                                                            <thead>
                                                                <tr>
                                                                    <th class="table-head-th">Milestone</th>
                                                                    <th class="table-head-th">One</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>Date for claim</td>
                                                                    <td>01/12/2015</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Progress claim stage</td>
                                                                    <td>$25,000</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>GST incl.</td>
                                                                    <td>6%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Cost allocated</td>
                                                                    <td>$22,000</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Cost incurred</td>
                                                                    <td>$21,000</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    <td class="wrapper-table-cell">
                                                        <table class="table table-bordered table-striped table-condensed table-responsive">
                                                            <thead>
                                                                <tr>
                                                                    <th class="table-head-th">Milestone</th>
                                                                    <th class="table-head-th">One</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>Date for claim</td>
                                                                    <td>02/12/2015</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Progress claim stage</td>
                                                                    <td>$25,000</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>GST incl.</td>
                                                                    <td>6%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Cost allocated</td>
                                                                    <td>$22,000</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Cost incurred</td>
                                                                    <td>$21,000</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    <td class="wrapper-table-cell">
                                                        <table class="table table-bordered table-striped table-condensed table-responsive">
                                                            <thead>
                                                                <tr>
                                                                    <th class="table-head-th">Milestone</th>
                                                                    <th class="table-head-th">One</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>Date for claim</td>
                                                                    <td>03/12/2015</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Progress claim stage</td>
                                                                    <td>$25,000</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>GST incl.</td>
                                                                    <td>6%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Cost allocated</td>
                                                                    <td>$22,000</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Cost incurred</td>
                                                                    <td>$21,000</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    <td class="wrapper-table-cell">
                                                        <table class="table table-bordered table-striped table-condensed table-responsive">
                                                            <thead>
                                                                <tr>
                                                                    <th class="table-head-th">Milestone</th>
                                                                    <th class="table-head-th">One</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>Date for claim</td>
                                                                    <td>04/12/2015</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Progress claim stage</td>
                                                                    <td>$25,000</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>GST incl.</td>
                                                                    <td>6%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Cost allocated</td>
                                                                    <td>$22,000</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Cost incurred</td>
                                                                    <td>$21,000</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    <td class="wrapper-table-cell">
                                                        <table class="table table-bordered table-striped table-condensed table-responsive">
                                                            <thead>
                                                                <tr>
                                                                    <th class="table-head-th">Milestone</th>
                                                                    <th class="table-head-th">One</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>Date for claim</td>
                                                                    <td>05/12/2015</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Progress claim stage</td>
                                                                    <td>$25,000</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>GST incl.</td>
                                                                    <td>6%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Cost allocated</td>
                                                                    <td>$22,000</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Cost incurred</td>
                                                                    <td>$21,000</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    <td class="wrapper-table-cell">
                                                        <table class="table table-bordered table-striped table-condensed table-responsive">
                                                            <thead>
                                                                <tr>
                                                                    <th class="table-head-th">Milestone</th>
                                                                    <th class="table-head-th">One</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>Date for claim</td>
                                                                    <td>06/12/2015</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Progress claim stage</td>
                                                                    <td>$25,000</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>GST incl.</td>
                                                                    <td>6%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Cost allocated</td>
                                                                    <td>$22,000</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Cost incurred</td>
                                                                    <td>$21,000</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    <td class="wrapper-table-cell">
                                                        <table class="table table-bordered table-striped table-condensed table-responsive">
                                                            <thead>
                                                                <tr>
                                                                    <th class="table-head-th">Milestone</th>
                                                                    <th class="table-head-th">One</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>Date for claim</td>
                                                                    <td>07/12/2015</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Progress claim stage</td>
                                                                    <td>$25,000</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>GST incl.</td>
                                                                    <td>6%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Cost allocated</td>
                                                                    <td>$22,000</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Cost incurred</td>
                                                                    <td>$21,000</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    <td class="wrapper-table-cell">
                                                        <table class="table table-bordered table-striped table-condensed table-responsive">
                                                            <thead>
                                                                <tr>
                                                                    <th class="table-head-th">Milestone</th>
                                                                    <th class="table-head-th">One</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>Date for claim</td>
                                                                    <td>08/12/2015</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Progress claim stage</td>
                                                                    <td>$25,000</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>GST incl.</td>
                                                                    <td>6%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Cost allocated</td>
                                                                    <td>$22,000</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Cost incurred</td>
                                                                    <td>$21,000</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.End Table 1 -->
                                </div>
                                <!-- /.End Tables -->

                            </div>
                            <!-- /.End Milestones -->
                        </div>

                        <div class="col-md-12 no-padding margin-top20">
                            <!-- Costings table -->
                            <div class="col-lg-6 col-md-12 col-sm-12 overflow-x-scroll">
                                <h4>Costings</h4>
                                <table id="costings" class="table table-bordered table-striped table-condensed table-responsive">
                                    <thead>
                                        <tr>
                                            <th class="table-head-th">Description</th>
                                            <th class="table-head-th">Costed</th>
                                            <th class="table-head-th">Incurred</th>
                                            <th class="table-head-th">Description</th>
                                            <th class="table-head-th">Costed</th>
                                            <th class="table-head-th">Incurred</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.End Costings table -->

                            <!-- Variations table -->
                            <div class="col-lg-6 col-md-12 col-sm-12 overflow-x-scroll">
                                <h4>Variations</h4>
                                <div id="grdVariations"></div>
                                <%--<table class="table table-bordered table-striped table-condensed table-responsive">
                                    <thead>
                                        <tr>
                                            <th class="table-head-th">Variation number</th>
                                            <th class="table-head-th">Status</th>
                                            <th class="table-head-th">Date of issue</th>
                                            <th class="table-head-th">Description</th>
                                            <th class="table-head-th">Amount</th>
                                            <th class="table-head-th">Adjusted Value</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>0162</td>
                                            <td>Approved</td>
                                            <td>05/09/2014</td>
                                            <td>lorem ipsum</td>
                                            <td>$2,100</td>
                                            <td>$103,000</td>
                                        </tr>
                                        <tr>
                                            <td>1935</td>
                                            <td>Pending</td>
                                            <td>NA</td>
                                            <td>lorem ipsum</td>
                                            <td>$1,600</td>
                                            <td>NA</td>
                                        </tr>
                                        <tr>
                                            <td>0162</td>
                                            <td>Approved</td>
                                            <td>05/09/2014</td>
                                            <td>lorem ipsum</td>
                                            <td>$2,100</td>
                                            <td>$103,000</td>
                                        </tr>
                                        <tr>
                                            <td>1935</td>
                                            <td>Pending</td>
                                            <td>NA</td>
                                            <td>lorem ipsum</td>
                                            <td>$1,600</td>
                                            <td>NA</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>--%>
                            </div>
                            <!-- /.End Variations table -->
                        </div>
                    </div>
                    <!-- /.end top form -->


                    <!-- right sidebar -->
                    <div class="col-md-12 no-padding right-sidebar">
                        <div class="col-md-6 col-sm-6">
                            <!-- Yammer -->
                            <table class="table table-bordered table-condensed table-responsive">
                                <thead>
                                    <tr>
                                        <th class="table-head-th">Proj 123 (Yammer)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- /.End Yammer -->
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <!-- Recent docs -->
                            <table class="table table-bordered table-striped table-condensed table-responsive">
                                <thead>
                                    <tr>
                                        <th class="table-head-th">Date</th>
                                        <th class="table-head-th">Document</th>
                                        <th class="table-head-th">Author</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>19/02/2015</td>
                                        <td>dsa-dsads</td>
                                        <td>John</td>
                                    </tr>
                                    <tr>
                                        <td>19/02/2015</td>
                                        <td>dsa-dsads</td>
                                        <td>John</td>
                                    </tr>
                                    <tr>
                                        <td>19/02/2015</td>
                                        <td>dsa-dsads</td>
                                        <td>John</td>
                                    </tr>
                                    <tr>
                                        <td>19/02/2015</td>
                                        <td>dsa-dsads</td>
                                        <td>John</td>
                                    </tr>
                                    <tr>
                                        <td>19/02/2015</td>
                                        <td>dsa-dsads</td>
                                        <td>John</td>
                                    </tr>
                                    <tr>
                                        <td>19/02/2015</td>
                                        <td>dsa-dsads</td>
                                        <td>John</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- /.End recent docs -->
                        </div>
                    </div>
                    <!-- /.end right sidebar -->
                </div>
            </div>

        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <div>
        <p id="message">
            <!-- The following content will be replaced with the user name when you run the app - see App.js -->
        </p>
    </div>

    <!-- Menu Toggle Script -->
    <script type="text/javascript">

        //-----------------------------------------------------------------
        var Variations = [];

        $(document).ready(function () {

            //getProjectHeaderData();

            var dataSource = new kendo.data.DataSource({
                pageSize: 8,
                data: Variations,
                autoSync: false,
                schema: {
                    model: {
                        id: "Id",
                        fields: {
                            Id: { editable: false, nullable: true },
                            VariationNumber: { type: "number", validation: { required: true, min: 0 } },
                            Status: { defaultValue: { StatusId: 1, Name: "Approved" } },
                            DateOfIssue: { type: "date", validation: { required: true } },
                            Description: { type: "string", validation: { required: true } },
                            Amount: { type: "number", validation: { required: true, min: 0 } },
                            AdjestedValue: { type: "number", validation: { required: false, min: 0 } }
                        }
                    }
                }
            });

            $("#grdVariations").kendoGrid({
                dataSource: dataSource,
                pageable: true,
                scorable: true,
                height: 300,
                toolbar: ["create"],
                columns: [
                    { field: "VariationNumber", format: "{0:0}", title: "Variation #", width: "90px" },
                    { field: "Status", title: "Status", width: "100px", editor: statusDropDownEditor, template: "#=Status.Name#" },
                    { field: "DateOfIssue", title: "Date of issue", width: "120px", format: "{0:dd-MM-yyyy}" },
                    { field: "Description", title: "Description", width: "160px" },
                    { field: "Amount", title: "Amount", format: "{0:c}", width: "90px" },
                    { field: "AdjestedValue", title: "Adjusted value", format: "{0:c}", width: "110px" },
                    { command: ["edit", "destroy"], title: "&nbsp;", width: 185 }],
                editable: "inline"
            });
        });

        function statusDropDownEditor(container, options) {
            $('<input required data-text-field="Name" data-value-field="StatusId" data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                    autoBind: false,
                    dataSource: [{ StatusId: 1, Name: "Approved" }, { StatusId: 2, Name: "Pending" }]
                });
        }
        //-----------------------------------------------------------------

        $('#newProject').popupWindow({
            centerScreen: 1
        });


        var projectId;

        $(".sidebar-nav li").click(function (e) {
            //e.preventDefault();

            $(this).toggleClass("selected");
            $(this).siblings().removeClass("selected");
            $("#project-label").text($(this).find('a').text());

            projectId = $(this).find('a').text();

            getProjectValues(Q, projectId);
            getCostingsData(projectId);

        });



        //$("#insertData").click(function (e) {
        //    e.preventDefault();

        //    /*var row = [];
        //    var costingData = [];
        //    var projectValueData = [];

        //    for (var i = 0; i < 2; i++) {

        //        //data.push({'Description': 'abc desc', 'Costed});
        //        row.Title = 'abc desc';
        //        row.Costed = '119';
        //        row.Incurred = '991';
        //        costingData.push(row);

        //    }

        //    insertDataToCostingsList(projectId, costingData);*/

        //    var projectValueData = { Title: '301609', SupervisorAllocation: '20000', BuildingMargin: '12.5', Insurances: '111', SupervisorCostIncurred: '12000' };
        //    insertDataToProjectValueList(projectId, projectValueData);

        //    var projectHeaderData = { Title: '5000000', ActualProjectValue: '750000', CostBudget: '650000', GrossProfit: '120000', TotalProjectClaimValue: '640000', RemainingClaimValue: '110000' }
        //    insertDataToProgectHeader(projectHeaderData);
            

        //    //Create folder stracture
        //    //root folder name
        //    var libraryPath = 'RecentDocuments/Residential';
        //    //project number
        //    var projectName = '1300';

        //    //project folder path
        //    var projectPath = libraryPath + '/' + projectName;
        //    var subFolder1 = 'Costings';
        //    var subFolder2 = 'Variations'
        //    //var libraryNames = ['Residential', 'Commercial', 'Other'];

        //    //create project folder
        //    createFolddersInLibrary(Q, libraryPath, projectName).then(function (message) {
        //        //create subfolder1
        //        createFolddersInLibrary(Q, projectPath, subFolder1).then(function (message) {

        //        });

        //        //create subfolder2
        //        createFolddersInLibrary(Q, projectPath, subFolder2).then(function (message) {
        //        });
        //    });

        //    });
            


        $("#addFileButton").click(function (e) {
            e.preventDefault();

            // Define the folder path for this example.
            var serverRelativeUrlToFolder = 'DashboardDocuments';
            var fileInput = jQuery('#getFile');
            var newName = jQuery('#displayName').val();

            uploadFile(fileInput, newName, serverRelativeUrlToFolder);
            
        });

        // Display error messages. 
        /*function onError(error) {
            alert(error.responseText);
        }*/


        $("#menu-toggle").click(function (e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });


        $("#import").click(function (e) {
            e.preventDefault();
            $("#xlf").trigger('click');
        });

        $("#chart").click(function (e) {
            e.preventDefault();
        });

        var X = XLSX;
        var XW = {
            /* worker message */
            msg: 'xlsx',
            /* worker scripts */
            rABS: './xlsxworker2.js',
            norABS: './xlsxworker1.js',
            noxfer: './xlsxworker.js'
        };

        var rABS = typeof FileReader !== "undefined" && typeof FileReader.prototype !== "undefined" && typeof FileReader.prototype.readAsBinaryString !== "undefined";
        var use_worker = typeof Worker !== 'undefined';
        var transferable = use_worker;
        var wtf_mode = false;
        var Accounts = [];

        function fixdata(data) {
            var o = "", l = 0, w = 10240;
            for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w, l * w + w)));
            o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w)));
            return o;
        }

        function ab2str(data) {
            var o = "", l = 0, w = 10240;
            for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint16Array(data.slice(l * w, l * w + w)));
            o += String.fromCharCode.apply(null, new Uint16Array(data.slice(l * w)));
            return o;
        }

        function s2ab(s) {
            var b = new ArrayBuffer(s.length * 2), v = new Uint16Array(b);
            for (var i = 0; i != s.length; ++i) v[i] = s.charCodeAt(i);
            return [v, b];
        }

        function xw_noxfer(data, cb) {
            var worker = new Worker(XW.noxfer);
            worker.onmessage = function (e) {
                switch (e.data.t) {
                    case 'ready': break;
                    case 'e': console.error(e.data.d); break;
                    case XW.msg: cb(JSON.parse(e.data.d)); break;
                }
            };
            var arr = rABS ? data : btoa(fixdata(data));
            worker.postMessage({ d: arr, b: rABS });
        }

        function xw_xfer(data, cb) {
            var worker = new Worker(rABS ? XW.rABS : XW.norABS);
            worker.onmessage = function (e) {
                switch (e.data.t) {
                    case 'ready': break;
                    case 'e': console.error(e.data.d); break;
                    default: xx = ab2str(e.data).replace(/\n/g, "\\n").replace(/\r/g, "\\r"); console.log("done"); cb(JSON.parse(xx)); break;
                }
            };
            if (rABS) {
                var val = s2ab(data);
                worker.postMessage(val[1], [val[1]]);
            } else {
                worker.postMessage(data, [data]);
            }
        }

        function xw(data, cb) {
            transferable = false;
            if (transferable) xw_xfer(data, cb);
            else xw_noxfer(data, cb);
        }

        function to_json(workbook) {
            var result = {};
            workbook.SheetNames.forEach(function (sheetName) {
                var roa = X.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                if (roa.length > 0) {
                    result[sheetName] = roa;
                }
            });
            return result;
        }

        function process_wb(wb) {

            var outputObj = to_json(wb);
            $('#xlf').val("");

            //Set Accounts Array=====================================================
            SetAccounts(outputObj['Accounts']);

            //============================ Costings =================================
            SetCostings(outputObj);

        }

        var xlf = document.getElementById('xlf');
        function handleFile(e) {
            rABS = false;
            use_worker = false;
            var files = e.target.files;
            var f = files[0];
            {
                var reader = new FileReader();
                var name = f.name;

                reader.onload = function (e) {
                    if (typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
                    var data = e.target.result;
                    if (use_worker) {
                        xw(data, process_wb);
                    } else {
                        var wb;
                        if (rABS) {
                            wb = X.read(data, { type: 'binary' });
                        } else {
                            var arr = fixdata(data);
                            wb = X.read(btoa(arr), { type: 'base64' });
                        }
                        process_wb(wb);
                    }
                };

                if (rABS) reader.readAsBinaryString(f);
                else reader.readAsArrayBuffer(f);
            }
        }

        if (xlf.addEventListener) xlf.addEventListener('change', handleFile, false);

        function SetAccounts(xflAccountsTab) {

            Accounts = [];
            var i = 0;

            xflAccountsTab.forEach(function (account) {
                var parent = "YES";
                if (account['Parent Account'] !== undefined)
                    parent = account['Parent Account'];

                Accounts.push({ Id: i, Code: account['Account Code'], Name: account["Account Name"], ParentAccount: parent });
                i++;
            });
        }

        function IsCurrentParent(childAccount, parentAccount) {
            var res = false;

            Accounts.forEach(function (account) {
                if (account.Code == childAccount && account.ParentAccount == parentAccount) {
                    res = true;
                }
            });

            return res;
        }

        function SetCostings(outputObj) {

            $("#costings").find('tbody').empty();
            var rows = [];
            var activeRows = [];
            var disabledRows = [];

            Accounts.forEach(function (account) {

                if (account["ParentAccount"] === "YES") {

                    var Costed = 0;
                    var index = 0;

                    outputObj['Import_1'].forEach(function (imp) {
                        index++;

                        if (index > 17) {
                            if (IsCurrentParent(imp.Field, account["Code"])) {
                                Costed += parseInt(imp.Value);
                            }
                        }

                    });

                    if (Costed == 0) {
                        //disabledRows.push({ Title: account["Name"], Costed: 0, Incurred: 0 });
                        disabledRows.push({ Title: account["Name"], Costed: '0', Incurred: '0' });

                    } else {
                        //activeRows.push({ Title: account["Name"], Costed: Costed, Incurred: parseInt(Costed * (70 / 100)) });
                        activeRows.push({ Title: account["Name"], Costed: Costed.toString(), Incurred: (Costed * (70 / 100)).toString() });

                    }
                }

            });

            /* var rowid = 0;
             var tbody = '';
             var td = '';
 
             activeRows.forEach(function (row) {
 
                 if (rowid % 2 == 0 && rowid != 0) {
                     tbody += '<tr>' + td + '</tr>';
                     td = '';
                 }
 
                 td += '<td>' + row["Description"] + '</td>' +
                      '<td>$' + row["Costed"] + '</td>' +
                      '<td>$' + row["Incurred"] + '</td>';
 
                 rowid++;
 
             });
 
             td = '';
             rowid = 0;
 
             disabledRows.forEach(function (row) {
 
                 if (rowid % 2 == 0 && rowid != 0) {
                     tbody += '<tr>' + td + '</tr>';
                     td = '';
                 }
 
                 td += '<td class="warning">' + row["Description"] + '</td>' +
                      '<td class="warning">$0</td>' +
                      '<td class="warning">$0</td>';
                 rowid++;
 
             });
 
             $("#costings").find('tbody').append(tbody);*/

            //merge two arrays
            var costingData = activeRows.concat(disabledRows);

            insertDataToCostingsList(projectId, costingData);

        }

    </script>

    <!-- Charts modal -->
    <div id="charts-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog chart-modal">
            <div class="modal-content no-border-radius">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-sm-12">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Sample Charts</h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-12">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                                        Project Schedule Monitoring (project specific)
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                        Cost Budget vs Money Received (for business)
                                    </label>
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-12 margin-top20">
                            <div class="col-sm-12 margin-top20">
                                <img alt="chart1" id="chartImg" src="../Images/chart1.jpg" class="img-responsive img-chart" />
                            </div>
                        </div>

                        <script type="text/javascript">

                            $(document).ready(function () {

                                $("#optionsRadios1").click(function () {
                                    $('#chartImg').attr('src', '../Images/chart1.jpg');
                                });

                                $("#optionsRadios2").click(function () {
                                    $('#chartImg').attr('src', '../Images/chart2.jpg');
                                });

                            });

                        </script>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- Charts modal -->


</asp:Content>
