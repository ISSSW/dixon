﻿'use strict';

var context = SP.ClientContext.get_current();
var user = context.get_web().get_currentUser();

// This code runs when the DOM is ready and creates a context object which is needed to use the SharePoint object model

//get current Context
var context = SP.ClientContext.get_current();

//get current user
var user = context.get_web().get_currentUser();

var hostweburl;
var appweburl;
var documentLibraryName = 'Dashboard1';
var scriptbase;

$(document).ready(function () {

    //$("#s4-titlerow").css({ display: "none" });
    //getUserName();

    //remove full screen mode page title and logo 
    $("#s4-titlerow").remove();

    hostweburl = decodeURIComponent(getQueryStringParameter("SPHostUrl"));
    appweburl = decodeURIComponent(getQueryStringParameter("SPAppWebUrl"));
    scriptbase = hostweburl + "/_layouts/15/";

    //$.getScript(hostweburl + "/_Layouts/15/SP.RequestExecutor.js");

    //$.getScript(scriptbase + "SP.RequestExecutor.js", execCrossDomainRequest);

    //$.getScript(scriptbase + "SP.RequestExecutor.js", getCostingsData);

    $.getScript(scriptbase + "SP.RequestExecutor.js", initialDataLoad);

});

// Function to retrieve a query string value.
function getQueryStringParameter(paramToRetrieve) {
    var params =
        document.URL.split("?")[1].split("&");
    var strParams = "";
    for (var i = 0; i < params.length; i = i + 1) {
        var singleParam = params[i].split("=");
        if (singleParam[0] == paramToRetrieve)
            return singleParam[1];
    }
}

function execCrossDomainRequest() {
    //var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/lists?@target='" + hostweburl + "'";
    //var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Dashboard1')/items?@target='" + hostweburl + "'&$select=Title";
    //var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/title?@target='" +hostweburl + "'";
    var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/GetFolderByServerRelativeUrl('" + documentLibraryName + "')/Files?@target='" + hostweburl + "'&$select=Name,TimeLastModified,ServerRelativeUrl&$orderby=TimeLastModified desc&$expand=Author";


    var executor = new SP.RequestExecutor(appweburl);
    executor.executeAsync({
        url: requestUrl,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: successHandler,
        error: errorHandler
    });
}


// Prints the data to the page.
function successHandler(data) {
    var jsonObject = JSON.parse(data.body);
    var blogsHTML = "";
    var recentDocuments = [];
    var tr;
    var results = jsonObject.d.results;
    //console.log(results);

    for (var i = 0; i < results.length; i++) {
        var documentDateIso = results[i].TimeLastModified;
        var documentDate = new Date(documentDateIso).toLocaleDateString();
        var fullDocumentName = results[i].Name;
        var splitDocumentName = fullDocumentName.split('.');
        var documentName = splitDocumentName[0];
        //var documentUrl = results[i].ServerRelativeUrl;      
        var documentAuthor = results[i].Author.Title;
        var documentUrl = hostweburl + "/" + documentLibraryName + "/" + fullDocumentName;

        var recentDocument = { DocumentDate: documentDate, DocumentName: documentName, DocumentUrl: documentUrl, DocumentAuthor: documentAuthor };
        recentDocuments.push(recentDocument);

        //blogsHTML = "<p>" + documentDate + "    " + results[i].Name +  " " + results[i].Author.Title + "</p></br>";
        //$('#message').append(blogsHTML);

        var tr;
        tr = $('<tr/>');
        tr.append("<td>" + documentDate + "</td>");
        tr.append("<td> <a href=" + documentUrl + ">" + documentName + "</a></td>");
        tr.append("<td>" + documentAuthor + "</td>");
        $('#recentDocument').children('tbody').append(tr);

    }
}

// Prints the error message to the page.
function errorHandler(data, errorCode, errorMessage) {
    $('#message').text("Could not complete cross-domain call: " + errorMessage);
    document.getElementById("listData").innerText = "Could not complete cross-domain call: " + errorMessage;
}

// This function prepares, loads, and then executes a SharePoint query to get the current users information
function getUserName() {
    context.load(user);
    context.executeQueryAsync(onGetUserNameSuccess, onGetUserNameFail);
}

// This function is executed if the above call is successful
// It replaces the contents of the 'message' element with the user name
function onGetUserNameSuccess() {

    $('#message').text('Hello ' + user.get_title());
}

// This function is executed if the above call fails
function onGetUserNameFail(sender, args) {
    alert('Failed to get user name. Error:' + args.get_message());
}

function initialDataLoad() {
    //var projectID = '1300';

    getProjectHeaderData();

    //getCostingsData(projectID);

    //getProjectValues(Q, projectID);

}


function getItemIdByKey(Q, listName, columnName, key, ID) {

    var outPut = [];
    var results = [];
    var result;
    var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('" + listName + "')/items?@target='" + hostweburl + "'&$select=" + ID + "&$filter=" + columnName + " eq '" + key + "' ";

    var deffered = Q.defer();

    var executor = new SP.RequestExecutor(appweburl);
    executor.executeAsync({
        url: requestUrl,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;

            if (results) {
                var result = results[0].ID;
            }


            deffered.resolve(result);
        },
        error: function (data) {
            result = "inside error function";
            deffered.reject = data;
        }
    });

    return deffered.promise;
}


//Get Costing Data
function getCostingsData(projectID) {
    //function getCostingsData() {
    //var projectID = '1300';
    var outPut = [];
    var results = [];
    var result;
    var rowid = 0;
    var tbody = '';
    var td = '';

    var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Costing')/items?@target='" + hostweburl + "'&$select=Title,Costed,Incurred&$filter=ProjectNumber/ProjectNumber eq '" + projectID + "' ";

    var executor = new SP.RequestExecutor(appweburl);
    executor.executeAsync({
        url: requestUrl,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;
            for (var i = 0; i < results.length; i++) {

                var description = results[i].Title;
                var Costed = parseFloat(results[i].Costed).toFixed(2);
                var Incurred = parseFloat(results[i].Incurred).toFixed(2);

                outPut.push({ Description: description, Costed: Costed, Incurred: Incurred });
            }


            if (outPut !== undefined && outPut.length > 0) {

                $("#costings").find('tbody').empty();

                outPut.forEach(function (row) {

                    if (rowid % 2 == 0 && rowid != 0) {
                        tbody += '<tr>' + td + '</tr>';
                        td = '';
                    }

                    td += '<td>' + row["Description"] + '</td>' +
                         '<td>$' + row["Costed"] + '</td>' +
                         '<td>$' + row["Incurred"] + '</td>';

                    rowid++;

                });

                $("#costings").find('tbody').append(tbody);
            }

        },
        error: function (data) {
            result = "inside error function";
        }
    });
}

//Get Project header data
function getProjectHeaderData() {

    var outPut = [];
    var results = [];
    var result;
    var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Project Header')/items?@target='" + hostweburl + "'&$select=Title,ActualProjectValue,CostBudget,GrossProfit,TotalProjectClaimValue,RemainingClaimValue&$top=1";

    var executor = new SP.RequestExecutor(appweburl);
    executor.executeAsync({
        url: requestUrl,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;

            var estimateProjectValue = results[0].Title;
            var actualProjectValue = results[0].ActualProjectValue;
            var costBudget = results[0].CostBudget;
            var grossProfit = results[0].GrossProfit;
            var totalProjectClaimValue = results[0].TotalProjectClaimValue;
            var remainingClaimValue = results[0].RemainingClaimValue

            if (results !== undefined && results.length > 0) {

                if (estimateProjectValue !== undefined)
                    $("#estimateprojectvalue").val("$" + parseFloat(estimateProjectValue).toFixed(2));
                if (actualProjectValue !== undefined)
                    $("#actualprojectvalue").val("$" + parseFloat(actualProjectValue).toFixed(2));
                if (costBudget !== undefined)
                    $("#costbudget").val("$" + parseFloat(costBudget).toFixed(2));
                if (grossProfit !== undefined)
                    $("#grossprofit").val("$" + parseFloat(grossProfit).toFixed(2));
                if (totalProjectClaimValue !== undefined)
                    $("#totalprojectclaim").val("$" + parseFloat(totalProjectClaimValue).toFixed(2));
                if (remainingClaimValue !== undefined)
                    $("#remainingclaim").val("$" + parseFloat(remainingClaimValue).toFixed(2));

            }

        },
        error: function (data) {
            result = "inside error function";
        }
    });
}


//Get Project Values - 
function getProjectValues(Q, projectID) {

    var deferred = Q.defer();
    var outPut = [];
    var results = [];
    var result;
    var requestUrl = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Project Value')/items?@target='" + hostweburl + "'&$select=Title,SupervisorAllocation,BuildingMargin,Insurances,SupervisorCostIncurred&$filter=ProjectNumber/ProjectNumber eq '" + projectID + "'&$top=1 ";

    var executor = new SP.RequestExecutor(appweburl);
    executor.executeAsync({
        url: requestUrl,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            results = jsonObject.d.results;

            var totalContractValue = results[0].Title;
            var supervisorAllocation = results[0].SupervisorAllocation;
            var buildingMargin = results[0].BuildingMargin;
            var insurances = results[0].Insurances;
            var supervisorCostIncurred = results[0].SupervisorCostIncurred;


            if (results !== undefined && results.length > 0) {

                if (totalContractValue !== undefined)
                    $("#totalcontractvalue").val("$" + parseFloat(totalContractValue).toFixed(2));
                if (supervisorAllocation !== undefined)
                    $("#supervisorallocation").val("$" + parseFloat(supervisorAllocation).toFixed(2));
                if (supervisorCostIncurred !== undefined)
                    $("#supervisorcostincurred").val("$" + parseFloat(supervisorCostIncurred).toFixed(2));
                if (buildingMargin !== undefined)
                    $("#buildingmargin").val(parseFloat(buildingMargin).toFixed(2));
                if (insurances !== undefined)
                    $("#insurances").val("$" + parseFloat(insurances).toFixed(2));
            }

            deferred.resolve(true);
        },
        error: function (data) {
            deferred.reject(false);
            result = "inside error function";
        }

    });

    return deferred.promise;
}


//Insert data to sharepoint list
function createItem(metadata, listName) {

    var formattedListItemName = listName.replace(/\s+/g, "");

    var item = $.extend({
        "__metadata": { "type": "SP.Data." + formattedListItemName[0].toUpperCase() + formattedListItemName.substring(1) + "ListItem" }
    }, metadata);



    var url = appweburl + "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('" + listName + "')/items?@target='" + hostweburl + "'";
    var bodyItem = JSON.stringify(item);

    var executor = new SP.RequestExecutor(appweburl);

    executor.executeAsync({
        url: url,
        method: "POST",  // instead of "type" from $.ajax has to use "method"!
        body: bodyItem,  // instead of "data" from $.ajax has to use "body"!
        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
            "content-type": "application/json;odata=verbose", // set content-type here
            //"content-length": bodyItem.length,
            "X-RequestDigest": $("#__REQUESTDIGEST").val()
        },
        success: function (data) {
            var body = JSON.parse(data.body);
            $("#message").html("Item created.");
        },
        error: function (data) {
            $("#message").html("Failed to create item.");
        }
    });
}


//insert data to Costings list
function insertDataToCostingsList(projectId, costingData) {

    var mainListName = 'Project'
    var listName = 'Costing';
    var columnName = 'ProjectNumber';
    var ID = 'ID';

    console.log(projectId);

    //get relavent lookup ID, before insert data
    getItemIdByKey(Q, mainListName, columnName, projectId, ID).then(function (lookupId) {

        if (costingData.length > 0) {
            for (var j = 0; j < costingData.length; j++) {

                //append lookup ID to array
                costingData[j]['ProjectNumberId'] = lookupId;

                //insert costing data 
                createItem(costingData[j], listName);

            }
        }
    });

    /* for (var i = 0; i < 2; i++) {
         row.Title = 'Tiytle 5';
         row.Name = 'Name 5';
         row.School = 'school 5';
         row.SubjectId = 2;
 
         metadata.push(row);
     }
 
     console.log(metadata.length);
     //var metadata = {
     //    'Title': 'Tiytle 3',
     //    'Name': 'Name 3',
     //    'School': 'school 3',
     //    'SubjectId': 2
     //};
 
     //var metadata = $.extend({},
     var listName = 'List1';
     for (var i = 0; i < metadata.length; i++) {
         createItem(metadata[i], listName);
     }*/
    //createItem(metadata, listName);
}

//Insert data to project value list
function insertDataToProjectValueList(projectId, projectValueData) {
    var mainListName = 'Project'
    var listName = 'Project Value';
    var columnName = 'ProjectNumber';
    var ID = 'ID';
    var lookupId = [];

    console.log(projectId);

    getItemIdByKey(Q, mainListName, columnName, projectId, ID).then(function (lookupId) {

        if (projectValueData) {
            projectValueData['ProjectNumberId'] = lookupId;

            //insert costing data 
            createItem(projectValueData, listName);

        }
    });
}

function createFolddersInLibrary(Q, libraryPath, folderName) {

    var deferred = Q.defer();

    var url = appweburl + "/_api/SP.AppContextSite(@target)/web/getfolderbyserverrelativeurl('" + libraryPath + "')/folders?@target= '" + hostweburl + "' ";

    var metadata = "{ '__metadata': { 'type': 'SP.Folder' }, 'ServerRelativeUrl': '" + folderName + "'}"

    var executor = new SP.RequestExecutor(appweburl);

    executor.executeAsync({
        url: url,
        method: "POST",
        body: metadata,
        headers: {
            "accept": "application/json; odata=verbose",
            "content-type": "application/json; odata=verbose"
        },
        success: function (data) {          
            var message = JSON.parse(data.body);
            deferred.resolve(message.Name);
            //console.log(message);
        },
        error: function (data) {
            var message = JSON.parse(data.body);         
            //console.log(message);
            deferred.reject(message);
        }
    });

    return deferred.promise;
}

//insert data to Project Header List
function insertDataToProgectHeader(headerValue) {
    var listName = 'Project Header';

    if (headerValue) {
        createItem(headerValue, listName);
    }
}

 
function getData() {
    var listName = 'List2';
    var columnName = 'Subject';
    var key = 'English';
    var ID = 'ID';

    var result = getItemIdByKey(listName, columnName, key, ID);

    console.log(result);
}







